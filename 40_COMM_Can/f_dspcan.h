/*************** (C) COPYRIGHT 2010  Inovance Technology Co., Ltd****************
* File Name          : f_dspcan.h
* Author             : Yanyi	
* Version            : V0.0.1
* Date               : 08/09/2010
* Description        : DSP CAN总线底层驱动库，包含文件
* Modify             : 1970 20150819 V3.10 统一驱动层接口，发送接收缓存结构体为环形缓存方式
********************************************************************************/
#ifndef		__f_dspcan__
#define		__f_dspcan__	  

//芯片类型
#define     SERVO_DRIVER                            1

#if defined STM32F407
    #include "stm32f4xx.h"
    #include "stm32f4xx_can.h"
#else
    #include "stm32f2xx.h"
    #include "stm32f2xx_can.h"
#endif

#include "PUB_GlobalPrototypes.h"  
#define CAN_ST


// 自动应答邮箱数据定义

#define USE_CAN1
/* #define USE_CAN2 */

#ifdef  USE_CAN1
  #define E_CAN                     CAN1
  #define CAN_CLK                   RCC_APB1Periph_CAN1
  #define CAN_RX_PIN                GPIO_Pin_11
  #define CAN_TX_PIN                GPIO_Pin_12
  #define CAN_GPIO_PORT             GPIOA
  #define CAN_GPIO_CLK              RCC_AHB1Periph_GPIOA
  #define CAN_AF_PORT               GPIO_AF_CAN1
  #define CAN_RX_SOURCE             GPIO_PinSource11
  #define CAN_TX_SOURCE             GPIO_PinSource12

  #define CAN_RX0_IRQ_CHANNEL       CAN1_RX0_IRQn
  #define CAN_RX1_IRQ_CHANNEL       CAN1_RX1_IRQn
  #define CAN_TX_IRQ_CHANNEL        CAN1_TX_IRQn
#else
  #define E_CAN                     CAN2
  
  #define CAN_CLK                   RCC_APB1Periph_CAN2
  #define CAN_RX_PIN                GPIO_Pin_5
  #define CAN_TX_PIN                GPIO_Pin_6
  #define CAN_GPIO_PORT             GPIOB
  #define CAN_GPIO_CLK              RCC_AHB1Periph_GPIOB
  #define CAN_AF_PORT               GPIO_AF_CAN2
  #define CAN_RX_SOURCE             GPIO_PinSource5
  #define CAN_TX_SOURCE             GPIO_PinSource6

  #define CAN_RX0_IRQ_CHANNEL       CAN2_RX0_IRQn
  #define CAN_RX1_IRQ_CHANNEL       CAN2_RX1_IRQn
  #define CAN_TX_IRQ_CHANNEL        CAN2_TX_IRQn  
#endif 

// 中断优先级定义
#define     CAN_RX0_IRQ_Priority                    2
#define     CAN_RX0_IRQ_SubPriority                 1

#define     CAN_RX1_IRQ_Priority                    2
#define     CAN_RX1_IRQ_SubPriority                 1

#define     CAN_TX_IRQ_Priority                     2
#define     CAN_TX_IRQ_SubPriority                  2

// CAN波特率宏定义
#define		CAN_BAUD_SUM							8		// CAN波特率总数

#define		CAN_INIT_TIME							1		// 初始化进行中
#define		CAN_INIT_SUCC							2		// 初始化成功
#define		CAN_INIT_TIMEOUT						3		// 初始化超时，失败
#define		CAN_INIT_BAUD_ERR						4		// 波特率选择出错

// 收发状态标志
#define		CAN_MBOX_TRAN_SUCC						0		// CAN邮箱操作成功，不表示发送成功
#define		CAN_MBOX_NUM_ERROR						1		// CAN邮箱号出错
#define		CAN_MBOX_BUSY							2		// CAN邮箱忙
#define		CAN_MBOX_EMPTY							3		// CAN接收邮箱空，
#define		CAN_MBOX_REC_SUCC						4		// CAN邮箱接收数据成功
#define		CAN_MBOX_REC_OVER						5		// CAN邮箱接收有数据溢出(读取不及时被覆盖)

#define		IINIT_CAN_TIME							4

#define		REC_ERR_INDEX							0
#define		TEC_ERR_INDEX							1

// 邮箱数定义
//#define		TRAN_MBOX_NUM			                3		// 发送邮箱数
//#define		REC_MBOX_NUM			                10		// 伺服接收缓存数
#define 	MAX_TX_MSG_COUNT   		                8 
#define 	MAX_RX_MSG_COUNT                        10

#define     CANBUF_OVER                             0xcccc

typedef struct 
{
	Uint16	PRE;
	Uint16	BS1;
	Uint16 	BS2;
}CAN_BAUD_ST;


// DSP  CAN帧完整 数据结构
typedef struct DSP_CAN_TYPE
{
	Uint32  msgid;                                          // ID 29位  11位使用低11位
	Uint16  rtr;
	Uint8   data[8];                                        // CAN帧数据
	Uint16  len;                                            // 缓存数据长度
}DspCanDataStru;

// CAN中断发送结构
typedef struct
{
    Uint8 		read;										// 读长度
	Uint8 		write;										// 写长度
	Uint8 		len;                                 		// 实际长度
    CanTxMsg 	TxMessage[MAX_TX_MSG_COUNT];				// 发送缓存
}CAN_TX_STR;

// CAN中断发送结构
typedef struct
{
    Uint8 		    read;								    // 读长度
	Uint8 		    write;								    // 写长度
	Uint8 		    len;                                    // 实际长度
    DspCanDataStru  RxMessage[MAX_RX_MSG_COUNT];		    // 接收缓存
    Uint16          bufOver; 
}CAN_RX_STR;


// 外部函数声明
extern void ClrCanSendBuf(void);                            // 取消未发送完成的帧

extern void DisableDspCAN(void);                            // 禁止CAN控制器

extern Uint16 InitdspECan(Uint16 baud);						// DSP CAN模块初始化

extern void InitTranMbox(Uint16 mbox, Uint32 msgid);
															// 初始化发送邮箱
extern void InitRecMbox(Uint16 mbox, Uint32 msgid, Uint32 lam, Uint16 fifoNo);
															// 初始化邮箱为接收邮箱
extern Uint16 eCanDataTran(Uint16 mbox, DspCanDataStru *pdata);
															// 指定发送邮箱发送数据															
extern Uint16 CanDataRead(DspCanDataStru *dataPi);		    // 邮箱数据读取
															
extern void CANErrCount(Uint16 *CANRecvErr, Uint16 *CANTranErr, Uint16 *CANBOffNo);

extern Uint16 CanMailBoxEmp(void);


//extern __ASM void set_PRIMASK(uint32_t priMask);

/*******************************************************************************
* 函数名称          : Uint16 CANIntHook(DspCanDataStru *pdata)
* 入口参数			: pdata 接收到数据帧
* 出口				：0-------未处理接收数据，1---------已经处理接收数据
* 创建	            : 1970	
* 版本		        : V0.1
* 时间              : 07/10/2015
* 说明				: 接收中断调用，供应用层处理接收函数
********************************************************************************/
extern Uint16 CANIntHook(DspCanDataStru *pdata);

#endif


