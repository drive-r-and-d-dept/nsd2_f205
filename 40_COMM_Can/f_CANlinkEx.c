
#include "f_dspcan.h"
#if SERVO_DRIVER
    #include "PUB_Main.h"
    #include "COMM_System.h"
    #include "func_funcode.h"
    #include "FUNC_COMMInterface.h"
	#include "FUNC_GlobalVariable.h"
    #include "FUNC_FunCodeDefault.h"
    #include "FUNC_OperEeprom.h" 
    #include "FUNC_ManageFunCode.h"
    #include "FUNC_ErrorCode.h"
    #include "f_canlink.h"

#else
    #include "f_funcCode.h"
    #include "f_comm.h"

#endif


Uint16 CanNodeRunStat;                                      // 节点运行状态
Uint16 CanStopServoFlag=0;

/*******************************************************************************
* 函数名称          :   Uint16 WriteRegFunTest(Uint16 addr, Uint16 data, Uint16 daTest)
* 入口参数			: 	addr   地址
						data   数据
						daTest "0"仅测试地址    "1" 地址数据都测试
* 出口				:   CANLINK_NO_ERROR		0						// 无错误
*                       CANLINK_ADDR_ERROR		2						// 地址错误
*                       CANLINK_DATA_ERROR		3						// 数据错误
*                       CANLINK_FUN_DIS_ERROR	4						// 功能禁止错误
* 创建	            : 	
* 版本		        :   V0.0.1
* 时间              :   06/15/2012
* 说明				:   写寄存器测试函数，用于测试地址，某些配置中判断地址是否有效等
*                                         数据测试后可调用"WriteMultiReg"组成完整写操作
********************************************************************************/
Uint16 WriteMultiRegTest(Uint16 addr, Uint16 *data, Uint16 len, Uint16 daTest)
{
    //Uint16 *pFunCodeAddr = 0;                   //功能码地址
    //Uint16 *pWrRdAddr = 0;                      //写入数据地址或读数据来源地址
    Uint16  IndexTemp = 0;                     //临时序号
    //Uint16  CycCnt = 0;                     //for循环计数器
    Uint32  FunCodeTemp = 0;                   //功能码
    Uint8   LmtCheck = 0;                     //限值比较结果
    Uint8   Group = 0;
	Uint8   Offset = 0;
    //Uint8   PostEr941_Flag = 0;
	Uint16  Index;

	if((daTest !=0) && (daTest !=1))  return 0x02;

    //Group_Input最高位等于1时,写入数据时不存储到Eeprom
	#if 0
    Group = Group_Input & 0x3F;
	#else
	Group=(addr & 0xff00)>>8;
	Offset=addr & 0x00ff;
	#endif

    //--------------------地址范围检查--------------------
    //1. 读写数据量是否为零或组号有没有超出范围
    //if(Total == 0) return 0x02;

    if((Group > FUNGROUP_ENDINDEX) &&(Group < AUXFUNGROUP_STARTINDEX)) return CANLINK_ADDR_ERROR;
    else if(Group > AUXFUNGROUP_ENDINDEX) return CANLINK_ADDR_ERROR;

    //2. 末尾地址有没有超出本组功能码的范围
    if((Offset + len) > FunCode_PanelDispLen[Group])   return CANLINK_ADDR_ERROR;
   
    //H0241厂家密码认证检查
    if(FunCodeUnion.code.OEM_OEMPass != OEMPASSWORD)
    { 
        // if((Group == 1) && ((Offset + len) > H01_PANELDISPLEN_OEM)) return CANLINK_ADDR_ERROR;
        if((Group == 1) && ((Offset + len) > DRIVER_GROUP_PANELDISPLEN_OEM)) return CANLINK_ADDR_ERROR;

        //H0240电机参数可见认证检查
        if( (FunCodeUnion.code.MT_EnVisable <= 10000) &&
            // (Group == 0) && ((Offset + len) > H00_PANELDISPLEN_OEM))return CANLINK_ADDR_ERROR;
            (Group == 0) && ((Offset + len) > MOTOR_GROUP_PANELDISPLEN_OEM))return CANLINK_ADDR_ERROR;
    }

    //如果H0000!=65535或14XXX,00组除了H0000外都不可以改写
    if( (Group == 0) && (FunCodeUnion.code.MT_MotorModel != 65535) &&
        ((FunCodeUnion.code.MT_MotorModel / 1000) != 14) && 
        // ((Offset + len) > H00_PANELDISPLEN_OEM) )
        ((Offset + len) > MOTOR_GROUP_PANELDISPLEN_OEM) )
    {
        return CANLINK_ADDR_ERROR;
    }

	for(Index=0;Index<len;Index++)
	{
		Offset=(addr+Index) & 0x00ff;
		IndexTemp = GetGroupCodeDftIndex(Group ,Offset);
        if(FunCodeDeft[IndexTemp].Attrib.bit.DataBits == ATTRIB_TWO_WORD) //
		{
			 if((Index+1)>=len)
			 {
				 return CANLINK_DATA32_ERROR;               // 32位数据没写完整
			 }
             else if(FunCodeDeft[IndexTemp].Attrib.bit.DataIndex == ATTRIB_HIGH_WORD)
             {
                 return CANLINK_DATA32_ERROR;               // 32位数据地址以低16位为准
             }
		}
		if(daTest ==1)
		{
		    if(STR_FUNC_Gvar.ManageFunCodeOutput.ResetFunCode == 1) return CANLINK_FUN_DIS_ERROR;
		    if(FunCodeDeft[IndexTemp].Attrib.bit.Writable > 1) return CANLINK_FUN_DIS_ERROR;
		    if((FunCodeDeft[IndexTemp].Attrib.bit.Writable == 1) &&
               (STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus == RUN))
                return CANLINK_FUN_DIS_ERROR;
		    //----------H0241设定检查----------
            if((Group == 0x02) && (Offset == 41) && (*(data+Index) != OEMPASSWORD)) return CANLINK_DATA_ERROR;

            //----------H0240设定检查----------
            if((Group == 0x02) && (Offset == 40) && (*(data+Index) < 10000)) return CANLINK_DATA_ERROR;

		    if(FunCodeDeft[IndexTemp].Attrib.bit.DataBits == ATTRIB_TWO_WORD)
            {
//		        if(FunCodeUnion.code.CM_Modbus32BitsSeq == 0)
//                {
//                    //高十六位在前,低十六位在后
//                    FunCodeTemp = A_SHIFT16_PLUS_B(*(data+Index),*(data+Index+1));
//                }
//                else
//                {
                    //高十六位在后,低十六位在前
                    FunCodeTemp = A_SHIFT16_PLUS_B(*(data+Index+1),*(data+Index));
//                }

                LmtCheck = LimitCheck_TwoWords(IndexTemp, FunCodeTemp);
                if(LmtCheck != 0) return CANLINK_DATA_ERROR;
	        }
		    else
		    {
                //调用内联比较函数
                LmtCheck = LimitCheck_0neWord(IndexTemp,*(data+Index));
                if(LmtCheck != 0) return CANLINK_DATA_ERROR;
		    }
		}
		if(FunCodeDeft[IndexTemp].Attrib.bit.DataBits == ATTRIB_TWO_WORD)
		{
		    Index++;
		}
	}
	return CANLINK_NO_ERROR;
}

/*******************************************************************************
* 函数名称          :   void WriteMultiRegFun(Uint16 addr, Uint16 *data, Uint16 len, Uint16 eeprom)
* 入口参数			: 	addr   地址
						*data  数据缓存
						eeprom "1" 掉电保存
* 出口				:   无
* 创建	            : 	
* 版本		        :   V0.0.1
* 时间              :   06/15/2012
* 说明				:   写寄存器函数，直接写不进行任何判断
*                       特性判断由 WriteRegFunTest 完成，两函数合成完整写操作
********************************************************************************/
void WriteMultiRegFun(Uint16 addr, Uint16 *data, Uint16 len, Uint16 eeprom)
{
    Uint16 *pFunCodeAddr = 0;                   //功能码地址
    Uint16 *pWrRdAddr = 0;                      //写入数据地址或读数据来源地址
    Uint16  IndexTemp = 0;                     //临时序号
    //Uint16  CycCnt = 0;                     //for循环计数器
    //Uint32  FunCodeTemp = 0;                   //功能码
    //Uint8   LmtCheck = 0;                     //限值比较结果
    Uint8   Group = 0;
	Uint8   Offset = 0;
    Uint8   PostEr941_Flag = 0;
	Uint16  Index;

    //Group_Input最高位等于1时,写入数据时不存储到Eeprom
	#if 0
    Group = Group_Input & 0x3F;
	#else
	Group=(addr & 0xff00)>>8;
	Offset=addr & 0x00ff;
	#endif

    pFunCodeAddr = (Uint16 *)FunCode_GroupStartAddr[Group] + Offset;
	for(Index=0;Index<len;Index++)
	{
		Offset=(addr+Index) & 0x00ff;
		IndexTemp = GetGroupCodeDftIndex(Group ,Offset);
		pFunCodeAddr = ((Uint16 *)FunCode_GroupStartAddr[Group] + Offset);
        pWrRdAddr = data + Index;
        if(FunCodeDeft[IndexTemp].Attrib.bit.DataBits == ATTRIB_TWO_WORD) //
		{
//            if(FunCodeUnion.code.CM_Modbus32BitsSeq == 0)
//            {
//                if((*pFunCodeAddr == *(pWrRdAddr + 1)) && ( *(pFunCodeAddr + 1) == *pWrRdAddr)) 
//				{
//
//				}
//				else
//				{
//                    //如果更改下次上电生效属性功能码，报941标志位置1
//                    if(FunCodeDeft[IndexTemp].Attrib.bit.Active == 1) PostEr941_Flag = 1;
//
//                    //高十六位在前,低十六位在后
//                    *pFunCodeAddr = *(pWrRdAddr + 1);
//                    *(pFunCodeAddr + 1) = *pWrRdAddr;
//
//					//如果更改H03 H04 H17组参数 STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate置1
//                    if((Group == 0x03) || (Group == 0x04) || (Group == 0x17))
//                    {
//                        STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate = 1;
//                    }
//
//                    //-------------------- Eeprom存储数据 --------------------
//                    if((FunCodeUnion.code.CM_WriteEepromEnable == 1) &&
//			         (FunCodeDeft[IndexTemp].Attrib.bit.CommSaveEn == ATTRIB_COMM_SAVE_EEPROM) &&
//                     (Group != 0x0D) && (Group != 0x0B) && (eeprom == 1))
//                    {                
//                        IndexTemp = GetGroupCodeIndex(Group, Offset);
//                        SaveToEepromSeri(IndexTemp,(IndexTemp + 1));
//
//                        // 再次上电生效的警告
//                        if(PostEr941_Flag == 1)
//                        {
//                            PostErrMsg(PCHGDWARN);
//                        }
//                    }
//				}
//            }
//            else
            {
                if((*pFunCodeAddr == *pWrRdAddr) && ( *(pFunCodeAddr + 1) == *(pWrRdAddr + 1) ))
				{

				}
				else
				{
                    //如果更改下次上电生效属性功能码，报941标志位置1
                    if(FunCodeDeft[IndexTemp].Attrib.bit.Active == 1) PostEr941_Flag = 1;

                    //高十六位在后,低十六位在前
                    *pFunCodeAddr = *pWrRdAddr;
                    *(pFunCodeAddr + 1) = *(pWrRdAddr + 1);

					//如果更改H03 H04 H17组参数 STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate置1
                    if((Group == 0x03) || (Group == 0x04) || (Group == 0x17))
                    {
                        STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate = 1;
                    }

                    //-------------------- Eeprom存储数据 --------------------
                    if((FunCodeUnion.code.CM_CanLinkWrEepromEn == 1) &&
			         (FunCodeDeft[IndexTemp].Attrib.bit.CommSaveEn == ATTRIB_COMM_SAVE_EEPROM) &&
                     (Group != 0x0D) && (Group != 0x0B))
                    {                
                        IndexTemp = GetGroupCodeIndex(Group, Offset);
                        SaveToEepromSeri(IndexTemp,(IndexTemp + 1));

                        // 再次上电生效的警告
                        if(PostEr941_Flag == 1)
                        {
                            PostErrMsg(PCHGDWARN);
                        }
                    }
				}
            }
			Index++;
        }
        //---------------------------- 写16位功能码 ----------------------------
        else
        {
            if(*pFunCodeAddr == *pWrRdAddr) 
			{

			}
            if(*pFunCodeAddr != *pWrRdAddr)
            {
                *pFunCodeAddr = *pWrRdAddr;
                if(FunCodeDeft[IndexTemp].Attrib.bit.Active == 1) PostEr941_Flag = 1;
                //如果更改H03 H04 H17组参数 STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate置1
                if((Group == 0x03) || (Group == 0x04) || (Group == 0x17))
                {
                    STR_FUNC_Gvar.ManageFunCodeOutput.AiAoDiDoUpdate = 1;
                }

                //-------------------- Eeprom存储数据 --------------------
                if((FunCodeUnion.code.CM_CanLinkWrEepromEn == 1) &&
                    (FunCodeDeft[IndexTemp].Attrib.bit.CommSaveEn == ATTRIB_COMM_SAVE_EEPROM) &&
                    (Group != 0x0D) && (Group != 0x0B))
                {
                    SaveToEepromOne(GetGroupCodeIndex(Group ,Offset));
                    // 再次上电生效的警告
                    if(PostEr941_Flag == 1)
                    {
                        PostErrMsg(PCHGDWARN);
                    }
                }
            }
        }			 		
	}
}


/*******************************************************************************
* 函数名称          : Uint16 WriteMultiReg(Uint16 addr, Uint16 *data, Uint16 len, Uint16 eeprom)
* 入口参数			: addr  写操作起始地址
*                     data  数据缓存
*                     len   写长度
*                     eeprom 掉电保存       "1" 掉电保存
* 出口				：CANLINK_NO_ERROR		    0						// 无错误
*                     CANLINK_ADDR_ERROR		2						// 地址错误
*                     CANLINK_DATA_ERROR		3						// 数据错误
*                     CANLINK_FUN_DIS_ERROR	    4						// 功能禁止错误
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: 写多个连续寄存器操作
********************************************************************************/
Uint16 WriteMultiReg(Uint16 addr, Uint16 *data, Uint16 len, Uint16 eeprom)
{
    Uint16 err;
    
    err = WriteMultiRegTest(addr, data, len, 1);            // 属性判断
    if (err)
        return err;
    else
        WriteMultiRegFun(addr, data, len, eeprom);          // 直接写入操作

    return err;        
}

/*******************************************************************************
* 函数名称          :   Uint16 ReadSingleReg(Uint16 addr, Uint16 *data)
* 入口参数			: 	addr   地址
						*data  数据缓存
* 出口				:   CANLINK_ADDR_ERROR		2						// 地址错误
*                       CANLINK_DATA_ERROR		3						// 数据错误
*                       CANLINK_FUN_DIS_ERROR	4						// 功能禁止错误
* 创建	            : 	
* 版本		        :   V0.0.1
* 时间              :   06/15/2012
* 说明				:   读单个寄存器操作函数
********************************************************************************/
//Uint16 ReadSingleReg(Uint16 addr, Uint16 *data)
//{
//// 实现代码
//    return CANLINK_NO_ERROR;
//}

/*******************************************************************************
* 函数名称          : Uint16 ReadMultiReg(Uint16 addr, Uint16 *data, Uint16 len)
* 入口参数			: addr  写操作起始地址
*                     data  数据缓存
*                     len   写长度
* 出口				：CANLINK_NO_ERROR		    0						// 无错误
*                     CANLINK_ADDR_ERROR		2						// 地址错误
*                     CANLINK_DATA_ERROR		3						// 数据错误
*                     CANLINK_FUN_DIS_ERROR 	4						// 功能禁止错误
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink数据帧发送多寄存器读操作
********************************************************************************/
Uint16 ReadMultiReg(Uint16 addr, Uint16 *dataPi, Uint16 len)
{
    Uint16 Group,Offset;
	Group=(addr & 0xff00)>>8;
	Offset=addr & 0x00ff;
    return(COMMWrRdFuncode(Group,Offset,len, dataPi,READSERIES,COMM_TYPE_CANLINK));
}


/*******************************************************************************
* 函数名称          : Uint16 ReadEleLabel(Uint8 Offset, Uint16 Total, Uint16 *dataPi)
* 入口参数		: Offset  电子标签偏移量
*                    Total 读取长度
*                   dataPi 读取数据地址
* 出口		* 0x00  操作成功        Sucess
				 * 0x10  操作模式错误    OperationModeErr

				 * 0x01  命令字错误码
				 * 0x02  地址不存在，超出地址范围
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink电子标签读取
********************************************************************************/
Uint16 ReadEleLabel(Uint8 Offset, Uint16 Total, Uint16 *dataPi)
{
    return(COMMWrRdEleLabel(Offset, Total, dataPi, READSERIES));
}


/*******************************************************************************
* 函数名称          :   Uint16 Write32BitRegFun(Uint16 addr, Uint32 data, Uint16 eeprom)
* 入口参数			: 	addr   地址
						*data  数据缓存
						eeprom "1" 掉电保存
* 出口				:   CANLINK_NO_ERROR		0						// 无错误
*                       CANLINK_ADDR_ERROR		2						// 地址错误
*                       CANLINK_DATA_ERROR		3						// 数据错误
*                       CANLINK_FUN_DIS_ERROR	4						// 功能禁止错误
* 创建	            : 	
* 版本		        :   V0.0.1
* 时间              :   06/15/2012
* 说明				:   写32位寄存器函数
********************************************************************************/
//Uint16 Write32BitRegFun(Uint16 addr, Uint32 data, Uint16 eeprom)
//{
//// 实现代码
//    return CANLINK_NO_ERROR;    
//}

/*******************************************************************************
* 函数名称          :   Uint16 Read32BitRegFun(Uint16 addr, Uint32 *data)
* 入口参数			: 	addr   地址
						*data  数据缓存
* 出口				:   CANLINK_ADDR_ERROR		2						// 地址错误
*                       CANLINK_DATA_ERROR		3						// 数据错误
*                       CANLINK_FUN_DIS_ERROR	4						// 功能禁止错误
* 创建	            : 	
* 版本		        :   V0.0.1
* 时间              :   06/15/2012
* 说明				:   读32位寄存器操作函数
********************************************************************************/
//Uint16 Read32BitRegFun(Uint16 addr, Uint32 *data)
//{
//	Uint16 reStat;
//// 实现代码
//	return reStat;
//}



/*******************************************************************************
* 函数名称          : void CanlinkOnline(void)
* 入口参数			: 
* 出口				:
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink总线在线
*                     节点收到监测器信号，总线连接触发执行该函数
********************************************************************************/
void CanlinkOnline(void)
{
	AuxFunCodeUnion.code.OS_CommDicnctStop=0;//具体处理应模仿H0D05=1的操作。    
// 实现代码
/*  112 参考
	if(WarnReg.bit.CANDISCONNECT)                           // 清除警告标志位
	{
		WarnReg.bit.CANDISCONNECT = 0;
		gstr_Gvar.SVSTFlag.bit.ZEROSPDSTOPFLG = 0;
	}
*/
}

/*******************************************************************************
* 函数名称          : void CanlinkMoniTimeout(void)
* 入口参数			: 
* 出口				:
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink监测器超时处理，掉线
*                     脱离总线，设备对提线的标识，如报故障
********************************************************************************/
void CanlinkMoniTimeout(void)
{
	PostErrMsg(COMMDISCONNECT);
/*	WarnReg.bit.CANDISCONNECT = 1;
	gstr_Gvar.SVSTFlag.bit.ZEROSPDSTOPFLG = 1;
*/
}

/*******************************************************************************
* 函数名称          : void CanlinkSafeFun(void)
* 入口参数			: 
* 出口				:
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink设备安全处理，当CANlink设备进入安全模式触发该函数
*                     设备处理用于使设备置于安全状态，如停机等
********************************************************************************/
void CanlinkSafeFun(void)
{
    // 实现代码
    AuxFunCodeUnion.code.OS_CommDicnctStop=1;//具体处理应模仿H0D05=1的操作。

    //清除虚拟DI son
    if(1 == FunCodeUnion.code.VI_VDIFuncSel1)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFFE;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel2)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFFD;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel3)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFFB;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel4)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFF7;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel5)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFEF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel6)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFDF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel7)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFFBF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel8)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFF7F;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel9)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFEFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel10)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFDFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel1)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xFBFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel12)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xEFFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel13)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xDFFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel14)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0xBFFF;
    }
    else if(1 == FunCodeUnion.code.VI_VDIFuncSel15)
    {
        AuxFunCodeUnion.code.CC_VDILevel &= 0x7FFF;
    }
}

/*******************************************************************************
* 函数名称          : void CanlinkAddrErrro(void)
* 入口参数			: 
* 出口				:
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink地址冲突处理，当发生地址错误时，
*                     设备处理 如标志对应故障
********************************************************************************/
void CanlinkAddrErrro(void)
{
// 实现代码
	PostErrMsg(COMMADDRCONFLICT);	//警告
}

/*******************************************************************************
* 函数名称          : void CANlinkDeviceStat(void)
* 入口参数			: 无
* 出口				: 无
* 创建	            : 	
* 版本		        : V0.0.1
* 时间              : 05/10/2012
* 说明				: CANlink设备状态栏处理，用于监测器获取设备当前状态
*                     当前定义bit0  "1" 设备故障 "0" 反之
*                             bit1  "1" 运行
********************************************************************************/
void CANlinkDeviceStat(void)
{
	CanNodeRunStat = 0;
// 以下112代码，请实现对应产品    
						 
	if(STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus == ERR)       //故障
	{
		CanNodeRunStat |= 0x00000001;	
	}
	else if(STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus == RUN) //运行
	{
		CanNodeRunStat |= 0x00000002;	
	}
	else if(STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus == RDY) //Servo Off,Rdy
	{
		CanNodeRunStat |= 0x00000004;
	}
	else if(STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus == NRD) //伺服未准备好状态,此时SVRDY无效
	{
		CanNodeRunStat = 0x00000000;
	}
	else
	{
		CanNodeRunStat = 0x00000000;
	}

}

