
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */ 
#include "FUNC_Filter.h" 
#include "FUNC_GlobalVariable.h"


/* Private_Constants ---------------------------------------------------------*/
/* 不带参数的宏定义 */

/* Private_Macros ------------------------------------------------------------*/
/* 带参数的宏定义 */

/* Private_TypesDefinitions --------------------------------------------------*/ 
/* 结构体变量定义 枚举变量定义 */

/* Private_Variables ---------------------------------------------------------*/
/* 变量定义 */


/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */
void LowPassFilter(STR_BILINEAR_LOWPASS_FILTER *p);
void InitLowPassFilter(STR_BILINEAR_LOWPASS_FILTER *p);

void MidFilter(STR_MidFilter *p);
void AvgFilter(STR_MidFilter *p);

/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部调用的函数的声明 */ 



/*******************************************************************************
  函数名: 
  输入:   无 
  输出:   无 
  子函数: 无
    1.  
    2.
********************************************************************************/
void InitLowPassFilter(STR_BILINEAR_LOWPASS_FILTER *p)
{
    if(p->Tc > p->Ts)
    {
        //ka=Ts/(2*Tc+Ts)
        p->Ka_Q20 = ((int32)p->Ts << 20) / ((p->Tc << 1) + p->Ts);
        if(p->Ka_Q20 < 400)      // Tc < 1600 * Ts
        {
            p->Ka_Q20 = 400;
        }
        //kb=1-2*ka
        p->Kb_Q20 = (1L<<20) - (p->Ka_Q20 << 1) ;
    }
    else
    {
        p->Ka_Q20 = 0;
        p->Kb_Q20 = 0;
    }
}

/*******************************************************************************
  函数名: void LowPassFilter(STR_BILINEAR_LOWPASS_FILTER *p)
  输入:   无 
  输出:   无 
  描述:   采用双线性变换的低通的滤波器
    1.  Y/X=1/(1+Tc*s)      s=2*(z-1)/(Ts*(z+1))
        Y/X=Ts*(z+1)/(Ts*(z+1)+2*Tc*(z-1))
        Y/X=Ts*(z+1)/(z*(2*Tc+Ts)+(-2*Tc+Ts))
        Y*z*(2*Tc+Ts)+Y*(-2*Tc+Ts)=Ts*(z+1)*X
        Y*z*(2*Tc+Ts)=Y*(2*Tc-Ts)+X*z*Ts+X*Ts
        Y*z=Y*(2*Tc-Ts)/(2*Tc+Ts)+(X*z*Ts+X*Ts)/(2*Tc+Ts)
        Y*z=Y-Y*2*Ts/(2*Tc+Ts)+(X*z*Ts+X*Ts)/(2*Tc+Ts)
        y(n)=y(n-1)*kb+(x(n)+x(n-1))*ka
        ka=Ts/(2*Tc+Ts)
        kb=1-2*ka
********************************************************************************/
void LowPassFilter(STR_BILINEAR_LOWPASS_FILTER *p)
{
    int64  Input_Q9 = 0;
    int64  Output_Q9 = 0;

    if((p->Ka_Q20 == 0) && (p->Kb_Q20 == 0))
    {
        p->Output = p->Input;
        return;
    }
    //y(n)=y(n-1)*kb+(x(n)+x(n-1))*ka
    Input_Q9 = (int64)p->Input << 9;

    Output_Q9 = ((int64)p->Kb_Q20 * p->OutputLatch_Q9 + (int64)p->Ka_Q20 * (p->InputLatch_Q9 + Input_Q9)) >> 20;

    p->InputLatch_Q9  = Input_Q9;
    p->OutputLatch_Q9 = Output_Q9;
    p->Output  = p->OutputLatch_Q9 >> 9;  
}


//==========================================================================
// 函数名：  MidFilter()
// 输  入：  InPut;
// 输  出:   OutPut;
// 功能描述: 中值滤波
//==========================================================================
void MidFilter(STR_MidFilter *p)
{              
    static int8 count = 1;
    int32  BubbleSortArray[3] = {0,0,0};
    int32  Temp = 0;

    //取依次输入的三个数
    p->SeriData[0]= p->SeriData[1];
    p->SeriData[1]= p->SeriData[2];
    p->SeriData[2]= p->InPut;

    if(count<3)   //前两个数以输入为输出
    {
        p->OutPut = p->InPut;
        count++;
    }
    else          //从第三个数开始取中值
    {
        BubbleSortArray[0] = p->SeriData[0];
        BubbleSortArray[1] = p->SeriData[1];
        BubbleSortArray[2] = p->SeriData[2];

        if(BubbleSortArray[0] < BubbleSortArray[1])
        {
            Temp = BubbleSortArray[0];
            BubbleSortArray[0] = BubbleSortArray[1];
            BubbleSortArray[1] = Temp;
        }

        if(BubbleSortArray[0] < BubbleSortArray[2])
        {
            Temp = BubbleSortArray[0];
            BubbleSortArray[0] = BubbleSortArray[2];
            BubbleSortArray[2] = Temp;
        }

        if(BubbleSortArray[1] > BubbleSortArray[2])
        {
            p->OutPut = BubbleSortArray[1];
        }
        else
        {
            p->OutPut = BubbleSortArray[2];
        }
    }
}

//==========================================================================
// 函数名：  AvgFilter()
// 输  入：  InPut;
// 输  出:   OutPut;
// 功能描述: 中值滤波
//==========================================================================
void AvgFilter(STR_MidFilter *p)
{
	static 	int32 inputlast = 0;
	static int8 count = 1;
	int64 temp;
	if (count < 2)
	{
		p->OutPut = p->InPut;
		count ++;
	}
	else
	{
		temp = (int64)(p->InPut + inputlast);
		p->OutPut = (int32)(temp / 2);
	}
	inputlast = p->InPut;
}

/********************************* END OF FILE *********************************/
