
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "PUB_RccDriver.h" 
#include "PUB_Main.h"
#include "FUNC_System.h"
#include "MTR_System.h" 
 
#if defined STM32F407
    #include "stm32f4xx.h"
    #include "stm32f4xx_tim.h"
    #include "stm32f4xx_dbgmcu.h"
    #include "stm32f4xx_syscfg.h"
#else
    #include "stm32f2xx.h"
    #include "stm32f2xx_tim.h"
    #include "stm32f2xx_dbgmcu.h"
    #include "stm32f2xx_syscfg.h"
#endif 

/* Private_Constants ---------------------------------------------------------*/
/* 不带带参数的宏定义 */
//#define PUB_SYSCLK_FREQ_72MHz     72000000
#define PUB_ENABLE                1
#define PUB_DISABLE               0

/* Private_Macros ------------------------------------------------------------*/
/* 带参数的宏定义 */

/* Private_TypesDefinitions --------------------------------------------------*/ 
/* 结构体变量定义 枚举变量定义 */
STR_PUB_GLOBALVARIABLE  STR_PUB_Gvar = STR_PUB_GLOBALVARIABLE_DEFAULT;   //PUB模块全局变量结构体

/* Private_Variables ---------------------------------------------------------*/
/* 文件内变量定义 */

/* Private_Variables ---------------------------------------------------------*/
/* 可供其它文件调用的变量定义 */
Uint16 * TIM4_CNT = (Uint16 *)(0x40000000 + 0x0800 + 0x24);

#if CODE_RUN_TIME_TEST
    Uint16 * TIM11_CNT = (Uint16 *)(0x40010000 + 0x4800 + 0x24);
#endif
/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */


/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部调用的函数的声明 */ 
Static_Inline void Pub_PeripheralConfig_RST(void);
Static_Inline void Init_TIM2_UnRealTimer(void);
Static_Inline void Init_TIM4AndTIM11_ProgramTimeTest(void);
Static_Inline void Init_PeripheralClocks(void);
Static_Inline void PUB_Interrupt_RST(void);
Static_Inline void ZFalling_Handler(void);


static void SetSysClock(void);
/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  
********************************************************************************/
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// @ 进main()之前的函数 ，在start.s中调用 tongwenzou20100727
//功能:1 初始化系统时钟，包含HSE的启动PLL FLASH 调用SystemInit (void)
// @brief  Setup the microcontroller system
//         Initialize the Embedded Flash Interface, the PLL and update the
//         SystemCoreClock variable.
//2.读取省线编码器UVW信息
//++++++++++原来的清零的功能在这里不必要了++++++++++++++++++++++++++++++

void startB4main(void)
{
    volatile int32 * DataAddr = (volatile int32 *) 0x20000000;
    int32 i = 0;

    //关闭所有的外设            
    RCC->AHB1RSTR |= 0xFFFFFFFF;  //RCC AHB1 peripheral reset register (RCC_AHB1RSTR)
    RCC->AHB2RSTR |= 0xFFFFFFFF;  //RCC AHB2 peripheral reset register (RCC_AHB2RSTR)
    RCC->AHB3RSTR |= 0xFFFFFFFF;  //RCC AHB3 peripheral reset register (RCC_AHB3RSTR)
    RCC->APB1RSTR |= 0xFFFFFFFF;  //RCC APB1 peripheral reset register (RCC_APB1RSTR)
    RCC->APB2RSTR |= 0xFFFFFFFF;  //RCC APB2 peripheral reset register (RCC_APB2RSTR)
    RCC->AHB1RSTR &= 0;  //RCC AHB1 peripheral reset register (RCC_AHB1RSTR)
    RCC->AHB2RSTR &= 0;  //RCC AHB2 peripheral reset register (RCC_AHB2RSTR)
    RCC->AHB3RSTR &= 0;  //RCC AHB3 peripheral reset register (RCC_AHB3RSTR)
    RCC->APB1RSTR &= 0;  //RCC APB1 peripheral reset register (RCC_APB1RSTR)
    RCC->APB2RSTR &= 0;  //RCC APB2 peripheral reset register (RCC_APB2RSTR)

    SysTick->CTRL = 0;

    EXTI->IMR = 0;
    EXTI->EMR = 0;

    NVIC->ICER[0] = 0xFFFFFFFF;
    NVIC->ICER[1] = 0xFFFFFFFF;
    NVIC->ICER[2] = 0xFFFFFFFF;
    NVIC->ICER[3] = 0xFFFFFFFF;
    NVIC->ICER[4] = 0xFFFFFFFF;
    NVIC->ICER[5] = 0xFFFFFFFF;
    NVIC->ICER[6] = 0xFFFFFFFF;
    NVIC->ICER[7] = 0xFFFFFFFF;  

    //Boot区使用Ram清零
    for(i=0;i<0x5000;i+=4)
    {
        *DataAddr = 0;
        DataAddr += 4;
    }

    PUB_SystemInit();

    DBGMCU_APB1PeriphConfig(DBGMCU_TIM2_STOP | DBGMCU_TIM3_STOP | DBGMCU_TIM4_STOP, ENABLE);
    DBGMCU_APB1PeriphConfig(DBGMCU_WWDG_STOP | DBGMCU_I2C1_SMBUS_TIMEOUT , ENABLE);
    DBGMCU_APB2PeriphConfig(DBGMCU_TIM11_STOP, ENABLE);
    DBGMCU_Config(DBGMCU_STOP, ENABLE);
}

//============================================================================================
#define VECT_TAB_OFFSET  0 ;0xC200 /*!< Vector Table base offset field. 
                                   This value must be a multiple of 0x200. */
// PLL_VCO = (HSE_VALUE or HSI_VALUE / PLL_M) * PLL_N 
/* Caution:
 * The software has to set PLL_M bits correctly to ensure that the VCO input frequency
 * ranges from 1 to 2 MHz. It is recommended to select a frequency of 2 MHz to limit
 * PLL jitter.
 * The software has to set PLL_N bits correctly to ensure that the VCO output
 * frequency is between 192 and 432 MHz.
*/
#define PLL_M      8

#if defined STM32F407
    #define PLL_N      336
#else
    #define PLL_N      240
#endif 



// SYSCLK = PLL_VCO / PLL_P 
//Caution: The software has to set PLL_P bits correctly not to exceed 120 MHz on this domain.
#define PLL_P      2

// USB OTG FS, SDIO and RNG Clock =  PLL_VCO / PLLQ
/* Caution: 
 * The USB OTG FS requires a 48 MHz clock to work correctly. The SDIO and the
 * random number generator need a frequency lower than or equal to 48 MHz to work
 * correctly.
 */
#define PLL_Q      5

static void SetSysClock(void)
{
/******************************************************************************/
/*            PLL (clocked by HSE) used as System clock source                */
/******************************************************************************/
  __IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  /* Enable HSE */
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
  /* Wait till HSE is ready and if Time out is reached exit */
  do
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;
  } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET)
  {
    HSEStatus = (uint32_t)0x01;
  }
  else
  {
    HSEStatus = (uint32_t)0x00;
  }

  if (HSEStatus == (uint32_t)0x01)
  {
    /* HCLK = SYSCLK / 1*/
    RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
      
    /* PCLK2 = HCLK / 2*/
    RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;
    
    /* PCLK1 = HCLK / 4*/
    RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;

    /* Configure the main PLL */
    RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) -1) << 16) |
                   (RCC_PLLCFGR_PLLSRC_HSE) | (PLL_Q << 24);

    /* Enable the main PLL */
    RCC->CR |= RCC_CR_PLLON;

    /* Wait till the main PLL is ready */
    while((RCC->CR & RCC_CR_PLLRDY) == 0)
    {
    }
   
    /* Configure Flash prefetch, Instruction cache, Data cache and wait state */
    FLASH->ACR = FLASH_ACR_PRFTEN | FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_LATENCY_3WS;

    /* Select the main PLL as system clock source */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    /* Wait till the main PLL is used as system clock source */
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL)
    {
    }
  }
  else
  { /* If HSE fails to start-up, the application will have wrong clock
         configuration. User can add here some code to deal with this error */
  }

}

void SystemInit(void)
{
  /* Reset the RCC clock configuration to the default reset state ------------*/
  /* Set HSION bit */
  RCC->CR |= (uint32_t)0x00000001;

  /* Reset CFGR register */
  RCC->CFGR = 0x00000000;

  /* Reset HSEON, CSSON and PLLON bits */
  RCC->CR &= (uint32_t)0xFEF6FFFF;

  /* Reset PLLCFGR register */
  RCC->PLLCFGR = 0x24003010;

  /* Reset HSEBYP bit */
  RCC->CR &= (uint32_t)0xFFFBFFFF;

  /* Disable all interrupts */
  RCC->CIR = 0x00000000;

#ifdef DATA_IN_ExtSRAM
  SystemInit_ExtMemCtl(); 
#endif /* DATA_IN_ExtSRAM */
         
  /* Configure the System clock source, PLL Multiplier and Divider factors, 
     AHB/APBx prescalers and Flash settings ----------------------------------*/
  SetSysClock();

  /* Configure the Vector Table location add offset address ------------------*/
#ifdef VECT_TAB_SRAM
  SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM */
#else
  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */
#endif

}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  
********************************************************************************/
int32 main(void)
{
    static Uint16 Schedule_EnFlag = 0;
    static Uint16 Schedule_OckTime = 0;
    static Uint16 TimeLatch = 0;
    Uint16 SystemTime = 0;

    STR_PUB_Gvar.AllInitDone = 0;

    //PUB模块外设初始化
    Pub_PeripheralConfig_RST();

    //FUNC模块外设初始化
    FUNC_PeripheralConfig_RST();

    //MTR模块外设初始化
    MTR_PeripheralConfig_RST();

    //FUNC模块参数初始化1 主要是功能码部分
    FUNC_Parameter_Frist_RST();

    //MTR模块参数初始化1 
    MTR_Parameter_Frist_RST();

    //FUNC模块参数初始化2
    FUNC_Parameter_Second_RST();

    //MTR模块参数初始化2
    MTR_Parameter_Second_RST();

    //FUNC模块中断函数初始化
    FUNC_Interrupt_RST();

    //MTR模块中断函数初始化
    MTR_Interrupt_RST();

    //PUB模块中断函数初始化
    PUB_Interrupt_RST();
                                    
    //看门狗初始化
    FUNC_InitAndEnableWatchDog();

    STR_PUB_Gvar.AllInitDone = 1;

    SystemTime = GetSysTime_1MHzClk();
    Schedule_OckTime = SystemTime - 1001;

    while(1)
    {
        //主循环调度处理  
        SystemTime = GetSysTime_1MHzClk();
        if( ((Uint16)(SystemTime - Schedule_OckTime) > 1000) && 
            ((Uint16)(SystemTime - Schedule_OckTime) < 60000) )
        {
            Schedule_OckTime += 1000;
            Schedule_EnFlag = 1;
        }
        else if((Uint16)(SystemTime - Schedule_OckTime) > 60000)  //实际调度过慢 复位
        {
            Schedule_OckTime = SystemTime;
            Schedule_EnFlag = 1;
        }

        //主循环函数处理
        if(Schedule_EnFlag == 1)
        {
            Schedule_EnFlag = 0;

            // 主循环程序调度时间测试(每隔多长时间调度一次)
            SystemTime = GetSysTime_1MHzClk();
            STR_PUB_Gvar.MainLoop_PSTime = SystemTime - TimeLatch;
            STR_PUB_Gvar.MainLoop_PSTime = STR_PUB_Gvar.MainLoop_PSTime & 0xFFFF;
            TimeLatch = SystemTime;

            MTR_MainLoop();                 //-----------主循环MTR执行程序 

            FUNC_MainLoop();                //-----------主循环FUNC执行程序 

            /* 主循环程序执行时间测试 */
            STR_PUB_Gvar.MainLoop_PRTime = GetSysTime_1MHzClk() - TimeLatch;
            STR_PUB_Gvar.MainLoop_PRTime = STR_PUB_Gvar.MainLoop_PRTime & 0xFFFF;
        }
    }
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  //用于三环调度的中断
********************************************************************************/
void EXTI0_IRQHandler()   // FPGA中断  EXTI0_IRQHandler
{
    static Uint16 ToqIntScheduleTime = 0;         //转矩中断调度时间测试变量

    //中断开始时刻测试
    STR_PUB_Gvar.ToqIntStartTime = GetSysTime_1MHzClk();  

    //转矩(FPGA)中断执行程序
    //if(EXTI_GetITStatus(EXTI_Line0) != RESET)     //确定是来自0管脚的中断
    if (((EXTI->PR & EXTI_Line0) != 0) && ((EXTI->IMR & EXTI_Line0) != 0))
    {
        //清中断标志位
        //EXTI_ClearITPendingBit(EXTI_Line0);
        EXTI->PR = EXTI_Line0;

        if(STR_PUB_Gvar.AllInitDone == 0) return;

        FUNC_AdcStart_ToqInterrupt();

        //中断开始到MTR_GetPara_ToqInterrupt()函数运行完共用时间 720/120 us
        MTR_GetPara_ToqInterrupt(); 

        //速度模式：启用斜坡函数时1228/120us， 没启用斜坡函数时1156/120us
        //转矩模式：996/120us
        FUNC_CmdProcess_ToqInterrupt(); 

        //速度模式：运行到FPGA转矩指令赋值时间2228/120us
        //转矩模式：运行到FPGA转矩指令赋值时间726/120us
        //速度模式：MTR_ReguControl_ToqInterrupt()运行时间2770/120us
        //转矩模式：MTR_ReguControl_ToqInterrupt()运行时间1246/120us 
        MTR_ReguControl_ToqInterrupt();

        //示波器采样、自适应滤波器启动时，运行时间234/120us
        FUNC_AuxFunc_ToqInterrupt();

        ZFalling_Handler();

        if(STR_PUB_Gvar.SoftInterruptEn == 1)
        {
            STR_PUB_Gvar.SoftInterruptEn = 0;
            EXTI->SWIER |= EXTI_Line4;
        }
    }

    /*转矩(FPGA)中断时间测试*/
    STR_PUB_Gvar.ToqInterrupt_PSTime = STR_PUB_Gvar.ToqIntStartTime - ToqIntScheduleTime;   //转矩中断调度时间测试
    STR_PUB_Gvar.ToqInterrupt_PSTime = STR_PUB_Gvar.ToqInterrupt_PSTime & 0xFFFF;
    ToqIntScheduleTime = STR_PUB_Gvar.ToqIntStartTime;

    /*转矩(FPGA)中断时间测试*/
    STR_PUB_Gvar.ToqInterrupt_PRTime = GetSysTime_1MHzClk() - STR_PUB_Gvar.ToqIntStartTime;  //转矩中断运行时间测试
    STR_PUB_Gvar.ToqInterrupt_PRTime = STR_PUB_Gvar.ToqInterrupt_PRTime & 0xFFFF;
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  
********************************************************************************/
void EXTI4_IRQHandler(void)
{
    static Uint16 PosSoftIntScheduleTime = 0;     //位置环软中断调度时间测试变量
    Uint16 PosIntTimeTest = 0;                    //位置环软中断运行时间测试变量
    
    /*位置环软中断时间测试*/
    PosIntTimeTest = GetSysTime_1MHzClk();        //位置环软中断运行时间测试
    STR_PUB_Gvar.PosInterrupt_PSTime = PosIntTimeTest - PosSoftIntScheduleTime;
    STR_PUB_Gvar.PosInterrupt_PSTime = STR_PUB_Gvar.PosInterrupt_PSTime & 0xFFFF;
    PosSoftIntScheduleTime = PosIntTimeTest;

    /*位置环软中断执行程序*/    
    if(EXTI_GetITStatus(EXTI_Line4) != RESET)
    {
        EXTI_ClearITPendingBit(EXTI_Line4);
        FUNC_PostionControl_PosInterrupt();
        MTR_PostionControl_PosInterrupt();
    }

    /*位置环软中断时间测试*/
    STR_PUB_Gvar.PosInterrupt_PRTime = GetSysTime_1MHzClk() - PosIntTimeTest;   //位置环软中断运行时间测试
    STR_PUB_Gvar.PosInterrupt_PRTime = STR_PUB_Gvar.PosInterrupt_PRTime & 0xFFFF;
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  //非实时处理部分计数中断，和三环中断分离是为了保证FPGA死机情况下DSP继续运行
********************************************************************************/
void TIM2_IRQHandler(void)
{

    /*辅助中断执行程序*/
    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);       //Clear TIM1 update interrupt

//        //电机模块的辅助中断处理主要软件启动ST芯片的ADC和读取ADC采样值
//        MTR_System_AuxInterrupt();

        //功能模块的辅助中断处理主要处理芯片上电复位时显示rESEt, 复位完成后监控FPGA中断是否正常
        FUNC_System_AuxInterrupt();

        if( STR_PUB_Gvar.AllInitDone == 1 )  //初始化完成
        {
            TIM_Cmd(TIM2, DISABLE);                         //不使能定时器
            TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);     //不使能定时器更新中断
        } 
    }
}
/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  Pub 外设初始化(包括系统时钟初始化，程序测试时间定时器初始化，以及TIM1定时器初始化等) 
********************************************************************************/ 
Static_Inline void Pub_PeripheralConfig_RST()
{
    Init_PeripheralClocks();      //初始化系统以及外设相关时钟

    Init_TIM2_UnRealTimer();      //定时器1辅助中断的初始化,用以配置非实时调度周期以及用于显示RESET //给非实时调用部分面板，通信等提供时基, 中断频率和电流环中断一致！                                 

    Init_TIM4AndTIM11_ProgramTimeTest();  //用于时间测试用的TIM4 TIM11
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  初始化CPU，系统，外设时钟
********************************************************************************/
Static_Inline void Init_PeripheralClocks()
{
    PUB_PeripheralClockConfig();
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  TIM1 辅助中断定时器初始化
********************************************************************************/ 
Static_Inline void Init_TIM2_UnRealTimer()
{
    NVIC_InitTypeDef           NVIC_InitStructure_main;    //定义TIM1中断的中断控制器的结构体变量
    TIM_TimeBaseInitTypeDef    TIM2_TimeBaseStructure;     //定义TIM1中断的中断定时器的结构体变量

    DINT;                                              //关总中断

    TIM_DeInit(TIM2);
    TIM_TimeBaseStructInit(&TIM2_TimeBaseStructure);

    // 确定定时器2的时基
    #if defined STM32F407
        TIM2_TimeBaseStructure.TIM_Prescaler         = 0x1;
        TIM2_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
        TIM2_TimeBaseStructure.TIM_Period            = (Uint16)((42000000L  / 1000) - 1);
        TIM2_TimeBaseStructure.TIM_ClockDivision     = 0;
    #else
        TIM2_TimeBaseStructure.TIM_Prescaler         = 0x0;
        TIM2_TimeBaseStructure.TIM_CounterMode       = TIM_CounterMode_Up;
        TIM2_TimeBaseStructure.TIM_Period            = (Uint16)((60000L / 1) - 1);
        TIM2_TimeBaseStructure.TIM_ClockDivision     = 0;             
    #endif
    TIM_TimeBaseInit(TIM2, &TIM2_TimeBaseStructure);

    TIM_Cmd(TIM2, ENABLE);     //使能定时器 

    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);        // 使能定时器更新中断 

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);   //优先组配置为1位主，可抢占性;3位亚优先级，非强占性

    //配置TIM2的中断优先级
    NVIC_InitStructure_main.NVIC_IRQChannel = TIM2_IRQn;       //更新事件中断
    NVIC_InitStructure_main.NVIC_IRQChannelPreemptionPriority = TIM2_UP_IRQ_PreemptionPriority;
    NVIC_InitStructure_main.NVIC_IRQChannelSubPriority = TIM2_UP_IRQ_SubPriority;       //0~7
    NVIC_InitStructure_main.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure_main);

    EINT;                                              //开总中断
}

/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述:  用于程序时间测试的计时器初始化
********************************************************************************/
Static_Inline void Init_TIM4AndTIM11_ProgramTimeTest()
{
    TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;   //定义TIM6程序时间计时器的结构体变量

    TIM_DeInit(TIM4);    //复位定时器4
    //系统时钟  1MHz
    #if defined STM32F407
        TIM_TimeBaseStructure.TIM_Prescaler = 84 - 1; 
    #else
        TIM_TimeBaseStructure.TIM_Prescaler = 60 - 1;          
    #endif
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 65535;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

    TIM_Cmd(TIM4, ENABLE);   // TIM4 counter enable

#if CODE_RUN_TIME_TEST
    TIM_DeInit(TIM11);   //复位定时器11
    //函数运算时间计算用计数器
    TIM_TimeBaseStructure.TIM_Prescaler =  0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 65535;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseInit(TIM11, &TIM_TimeBaseStructure);

    TIM_Cmd(TIM11, ENABLE);   // TIM11 counter enable
#endif
}
/*******************************************************************************
  函数名:  
  输入:    
  输出:    
  子函数:         
  描述: 
********************************************************************************/
void PUB_Interrupt_RST(void)
{
    GPIO_InitTypeDef    GPIO_InitStruct;            //定义FPGA中断的GPIO管脚的初始化结构体变量
    NVIC_InitTypeDef    NVIC_InitStructure_EXTI;    //定义FPGA中断的中断控制器的结构体变量
    EXTI_InitTypeDef    EXTI_InitStructureFPGA;     //定义FPGA中断的中断定时器的结构体变量

    //FPGA管脚中断配置   
    GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_0;	        //对中断管脚PB0的配置，FPGA发出的16K中断
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStruct); 

    //用于配置FPGA送过来的中断的中断定时器将PB0管脚作为 EXTI Line0 的中断来源
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, GPIO_PinSource0);   //中断对应的管脚

    EXTI_InitStructureFPGA.EXTI_Mode    = EXTI_Mode_Interrupt;
    EXTI_InitStructureFPGA.EXTI_LineCmd = ENABLE;
    EXTI_InitStructureFPGA.EXTI_Line    = EXTI_Line0;             //PB0 ->中断
    EXTI_InitStructureFPGA.EXTI_Trigger = EXTI_Trigger_Falling;   //下降沿中断  
    EXTI_Init(&EXTI_InitStructureFPGA);                           //初始化配置

    //先清除已有的中断标志先
    EXTI_ClearITPendingBit(EXTI_Line0);                           //PB0 由FPGA发送过来的中断 

    //软件中断 用于位置调节的中断
    EXTI_InitStructureFPGA.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructureFPGA.EXTI_LineCmd = ENABLE;
    EXTI_InitStructureFPGA.EXTI_Line = EXTI_Line4;                  //PB0 ->中断
    EXTI_InitStructureFPGA.EXTI_Trigger= EXTI_Trigger_Falling;      //下降沿中断
    EXTI_Init(&EXTI_InitStructureFPGA);   //初始化配置

    EXTI_ClearITPendingBit(EXTI_Line4);       //清除软中断

    //由FPGA送来的中断配置其优先级
    NVIC_InitStructure_EXTI.NVIC_IRQChannel = EXTI0_IRQn;         //用于速度转矩调节的中断
    NVIC_InitStructure_EXTI.NVIC_IRQChannelPreemptionPriority = EXTI0_UP_IRQ_PreemptionPriority;
    NVIC_InitStructure_EXTI.NVIC_IRQChannelSubPriority = EXTI0_UP_IRQ_SubPriority;
    NVIC_InitStructure_EXTI.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure_EXTI);

    NVIC_InitStructure_EXTI.NVIC_IRQChannel = EXTI4_IRQn;       //用于位置调节的中断
    NVIC_InitStructure_EXTI.NVIC_IRQChannelPreemptionPriority = EXTI4_IRQ_PreemptionPriority;
    NVIC_InitStructure_EXTI.NVIC_IRQChannelSubPriority = EXTI4_IRQ_SubPriority;
    NVIC_InitStructure_EXTI.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure_EXTI);

    GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_7;         //对中断管脚PB7的配置, Z信号接入端
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStruct);

    EXTI_InitStructureFPGA.EXTI_Mode    = EXTI_Mode_Interrupt;
    EXTI_InitStructureFPGA.EXTI_LineCmd = ENABLE;
    EXTI_InitStructureFPGA.EXTI_Line    = EXTI_Line7;             //PB7 ->Z中断
    EXTI_InitStructureFPGA.EXTI_Trigger = EXTI_Trigger_Falling;   //下降沿中断

    // 配置中断对应的管脚
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, GPIO_PinSource7); 
    // 初始化Z信号中断配置
    EXTI_Init(&EXTI_InitStructureFPGA);
    // 清除已有的Z信号中断标志
    EXTI_ClearITPendingBit(EXTI_Line7);
}

/*******************************************************************************
  函数名: void EXTI9_5_IRQHandler() 
  输  入:           
  输  出:   
  子函数:                                       
  描  述: 处理Z脉冲中断
********************************************************************************/
Static_Inline void ZFalling_Handler(void)
{
    if(EXTI_GetITStatus(EXTI_Line7) != RESET)  //确定是来自PB7管脚的中断
	{
	    EXTI_ClearITPendingBit(EXTI_Line7); 

	    ZeroIndexISR();       //执行Z中断处理函数

        AngInt_ZPosLatch();

        ZPosErrDeal();
	}
}

/*******************************************************************************
  函数名:  
  输  入:           
  输  出:   
  子函数:                                       
  描  述: 
********************************************************************************/
void SysTick_Handler(void)
{
}
/********************************* END OF FILE *********************************/
