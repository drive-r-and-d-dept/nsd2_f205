
#ifndef __PUB_SERVOCONFIGURATION_H
#define __PUB_SERVOCONFIGURATION_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "PUB_GlobalPrototypes.h"

/* Exported_Constants --------------------------------------------------------*/
/* 不带参数的宏定义 */
// 驱动器系列号 0-64

// 驱动器系列号 0-64
#define     SERVO_620P          0


/* 伺服软件版本号命名规则(10进制)
 * XY.A 
 * X  驱动器型号0-64
 * Y  归档版本号0-99
 * A  市场临时版本号0-9
*/
//驱动器型号X
#define DRIVER_TYPE             SERVO_620P

//标准软件通用版本号Y.A
#define VERSION_STANDARD        98

//H0100 DSP软件通用版本号
#define VERSION_H0100         ((DRIVER_TYPE * 1000) + VERSION_STANDARD)

/* 软件内部版本号命名规则(16进制)
 * B.D 
 * B  提交测试部版本号0-99
 * D  研发内部版本号0-99
*/
//提交测试版本号
#define VERSION_B               6

//内部版本号
#define VERSION_D               1

//H0150 DSP软件内部版本号
#define VERSION_H0150         ((VERSION_B * 100) + VERSION_D)

//H0151 FPGA软件内部版本号
#define VERSION_H0151         ((VERSION_B * 100) + 0)

/* 非标版本号命名规则(16进制)
 * FFF.AB 
 * FFF  非标号
 * A   非标通用版本
 * B   非标临时版本
*/
//非标类型
//标准版本
#define STANDARD    0x000
//直线电机非标         
#define LINEARMOT   0x001
//600P非标
#define IS600P      0x216  

//汇川绝对值编码器非标
#define HCABSENC    0x683
//尼康绝对值编码器非标
#define NOKINABSENC    0x674
//多摩川绝对值编码器非标
#define TAMAGAWAABSENC    0x673 
       
//非标号
#define NONSTANDARD_PROJECT    HCABSENC
//注  IS620P非标软件 需要在函数void DealDriverPara(void)后面再次对H0000赋值

//非标软件版本号
#if NONSTANDARD_PROJECT != 0
    #define VERSION_NONSTANDARD     0x50 
    #define VERSION_H0002      ((Uint16)((NONSTANDARD_PROJECT << 8) + VERSION_NONSTANDARD)) 
    #define VERSION_H0003      (NONSTANDARD_PROJECT >> 8) 
#else
    #define VERSION_NONSTANDARD     0 
    #define VERSION_H0002      0 
    #define VERSION_H0003      0 
#endif 


//CAN功能使能开关
//0--代表不使能CAN功能；1--代表使能CAN功能
#if defined STM32F407
    #define  CAN_ENABLE_SWITCH      0 
#else
    #define  CAN_ENABLE_SWITCH      1
#endif    

//汇川编码器开关
#define  HC_ENC_SW              1 

//尼康编码器开关
#define  NOKIN_ENC_SW           1 

//多摩川编码器开关
#define  TAMAGAWA_ENC_SW        1  


//电子标签宏定义
//厂商
#define EL_OEM               1
//产品线
#define EL_PRODUCT_LINE      13
//产品型号
#define EL_MODEL             0x107
//产品版本
#define EL_PRODUCT_VER       VERSION_H0100
//CAN通信版本
#define EL_CAN_VER           310
//BOOT版本
#define EL_BOOT_VER          0
//芯片型号
#if defined STM32F407
    #define EL_CHIP_MODEL        0x130
#else
    #define EL_CHIP_MODEL        0x120
#endif 
//非标号
#define EL_NONSTANDARD_VER   NONSTANDARD_PROJECT


#ifdef __cplusplus
}
#endif

#endif /* __PUB_SERVOCONFIGURATION_H */

/********************************* END OF FILE *********************************/
