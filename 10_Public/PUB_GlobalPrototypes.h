 
#ifndef PUB_GLOBALPROTOTYPES_H
#define PUB_GLOBALPROTOTYPES_H 

#ifdef __cplusplus
extern "C" {
#endif   

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */  

/* Exported_Constants --------------------------------------------------------*/
/* 不带参数的宏定义 */
/*exact-width signed integer types */
typedef   signed          char int8;
typedef   signed short     int int16;
typedef   signed           int int32;
typedef   signed       __int64 int64;

/* exact-width unsigned integer types */
typedef unsigned           char Uint8;
typedef unsigned short     int Uint16;
typedef unsigned           int Uint32;
typedef unsigned       __int64 Uint64; 

//DSP分配给FPGA的存储器起始映像地址
#define FPGA_BASE  0x60000000

    /* 内联函数关键字宏定义 */
    #define Static_Inline   static __inline

    /* 内嵌汇编指令函数宏定义,使用时需要包含stm32f10x.h文件 */
    //开中断
    #define  EINT    __set_PRIMASK(0)
    //关中断
    #define  DINT    __set_PRIMASK(1)
    //空操作
    #define  NOP     __NOP()
/* The table below gives the allowed values of the pre-emption priority and subpriority according
 * to the Priority Grouping configuration performed by NVIC_PriorityGroupConfig function
 * ============================================================================================================================
 *   NVIC_PriorityGroup   | NVIC_IRQChannelPreemptionPriority | NVIC_IRQChannelSubPriority  | Description
 * ============================================================================================================================
 *  NVIC_PriorityGroup_0  |                0                  |            0-15             |   0 bits for pre-emption priority
 *                        |                                   |                             |   4 bits for subpriority
 * ----------------------------------------------------------------------------------------------------------------------------
 *  NVIC_PriorityGroup_1  |                0-1                |            0-7              |   1 bits for pre-emption priority
 *                        |                                   |                             |   3 bits for subpriority
 * ----------------------------------------------------------------------------------------------------------------------------
 *  NVIC_PriorityGroup_2  |                0-3                |            0-3              |   2 bits for pre-emption priority
 *                        |                                   |                             |   2 bits for subpriority
 * ----------------------------------------------------------------------------------------------------------------------------
 *  NVIC_PriorityGroup_3  |                0-7                |            0-1              |   3 bits for pre-emption priority
 *                        |                                   |                             |   1 bits for subpriority
 * ----------------------------------------------------------------------------------------------------------------------------
 *  NVIC_PriorityGroup_4  |                0-15               |            0                |   4 bits for pre-emption priority
 *                        |                                   |                             |   0 bits for subpriority  
 * ============================================================================================================================
 */
    //EXTI线0中断优先级(辅助FPGA发送的外部中断 转矩中断)
    #define  EXTI0_UP_IRQ_PreemptionPriority        0
    #define  EXTI0_UP_IRQ_SubPriority               0

    //定时器TIM2更新中断优先级(辅助中断)
    #define  TIM2_UP_IRQ_PreemptionPriority         2
    #define  TIM2_UP_IRQ_SubPriority                2

    //串口UART1全局中断优先级(MODBUAS中断)
    #define  USART1_IRQ_PreemptionPriority          2
    #define  USART1_IRQ_SubPriority                 0

    //EXTI线4中断优先级(软件触发中断 位置中断)
    #define  EXTI4_IRQ_PreemptionPriority           1
    #define  EXTI4_IRQ_SubPriority                  2 

//    //EXTI线2中断优先级(DI9中断 中断定长)
//    //标准程序不使用该中断
//    #define  EXTI2_IRQ_PreemptionPriority           1
//    #define  EXTI2_IRQ_SubPriority                  1 

//    //EXTI线7中断优先级(PB7中断 Z中断) 
//    //标准程序采用在电流环里面查询的方式处理Z中断相关功能
//    #define  EXTI9_5_IRQ_PreemptionPriority         2
//    #define  EXTI9_5_IRQ_SubPriority                0

     
    #define     PUB_SYSCLK_FREQ_120MHz     120000000
    #define     PUB_APB1_FREQ_30MHz        30000000     
    #define     PUB_APB1_TIM_FREQ_60MHz    60000000    
    #define     PUB_APB2_FREQ_60MHz        60000000    
    #define     PUB_APB2_TIM_FREQ_120MHz   120000000

    #define     PUB_SYSCLK_FREQ_168MHz      168000000L
    #define     PUB_APB1_FREQ_42MHz         42000000L
    #define     PUB_APB1_TIM_FREQ_84MHz     84000000L
    #define     PUB_APB2_FREQ_84MHz         84000000L  
    #define     PUB_APB2_TIM_FREQ_168MHz    168000000L


#define     SQRT2_Q10       1448L
#define     PI_Q12          12868L

//-----------使能PWM----------------
#define   ENPWM          1
//-----------关PWM----------------
#define   DISPWM         0

// 运行模式宏定义：
#define TOQMOD      0x01
#define SPDMOD      0x02
#define POSMOD      0x04

// 运行状态宏定义：
//0 伺服未准备好状态 此时SVRDY无效
#define NRD         0x00
//1 准备好状态  SERVO_OFF
#define RDY         0x01
//2 伺服正常运行态  SERVO_ON
#define RUN         0x02
//3 伺服故障态
#define ERR         0x03

/* Exported_Macros -----------------------------------------------------------*/
/* 带参数的宏定义 */
//求取绝对值
#define ABS(A)      (((A)>=0) ? (A) : (-(A)))

//取参数A、B的最小值
#define MIN(A,B)    (((A)<(B))? (A) : (B))

//取参数A、B的最大值
#define MAX(A,B)    (((A)>(B))? (A) : (B))

//下限幅
#define MINLMT(A,Min) (((A)<(Min))? (Min) : (A))

//上限幅
#define MAXLMT(A,Max) (((A)>(Max))? (Max) : (A))

//上下限幅
#define MAX_MIN_LMT(A,Pos,Neg)  MAX(MIN(A,Pos),Neg)

//求(A<<16 + B)的值，常用来求32位功能码的值
//如：A_SHIFT16_PLUS_B(FunCodeUnion.code.PL_PosSecCmxHigh,FunCodeUnion.code.PL_PosSecCmxLow)
#define A_SHIFT16_PLUS_B(A,B)   (((Uint32)(A) << 16) + (Uint32)(B))

//仅对除以非2的除法四舍五入使用
#define Sign_NP(A)   (((A)>=0) ? 1 : -1)



/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 

/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */

/* Inline Function --------------------------------------------------------*/
/* 内联函数定义 */
/*******************************************************************************
  函数名: Static_Inline void DELAY_US(Uint32 Nus)
  描述：延时函数
  测试函数：
            ...
            DINT;
            DelayCnt1 = GetSysTime_120MHzClk();
            DELAY_US(i);
            DelayCnt2 = GetSysTime_120MHzClk();
            EINT;
            DelayCnt3[i] = DelayCnt2 - DelayCnt1;
            ...
  测试结果：    输入        时间us
                0           24/120
                1           138/120
                2           252/120
                3           366/120
                4           484/120
                5           596/120
                6           712/120
                7           826/120
                8           942/120
                10          1174/120
                20          2322/120
                30          3474/120
                40          4624/120

  测试函数：
            ...
            DINT;
            DelayCnt1 = GetSysTime_168MHzClk();
            DELAY_US(i);
            DelayCnt2 = GetSysTime_168MHzClk();
            EINT;
            DelayCnt3[i] = DelayCnt2 - DelayCnt1;
            ...
  测试结果：    输入        时间us
                0           14/168=0.08
                1           184/168=1.09
                2           346/168=2.05
                3           510/168=3.03
                4           672/168=4.00
                5           836/168=4.97
                6           998/168=5.94
                7           836/168=6.91
                8           836/168=7.88
                9           1488/168=8.85
                10          1650/168=9.82
                11          1814/168=10.79
                15          2466/168=14.67
                20          3280/168=19.52
                25          4096/168=24.38
                30          4910/168=29.22
                35          5726/168=34.08
                40          6540/168=38.92
                50          8170/168=48.63
                60          9800/168=58.33
                70          11430/168=68.03
                80          13060/168=77.73
                90          14690/168=87.44
                1000        15506/168=92.29 
********************************************************************************/

Static_Inline void DELAY_US(Uint32 Nus)
{    
   Uint16 i=0;
   while(Nus--)
   {
      #if defined STM32F407
          i = 31;    //测试结果如上
      #else
          i = 21;    //测试结果如上
      #endif
      while(i--);
   }
}

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 




#ifdef __cplusplus
}
#endif 

#endif /* end of PUB_GlobalPrototypes */

/********************************* END OF FILE *********************************/




