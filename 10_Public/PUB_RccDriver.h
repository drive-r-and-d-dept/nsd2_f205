 
#ifndef __PUB_RCCDRIVER_H
#define __PUB_RCCDRIVER_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "PUB_GlobalPrototypes.h"

/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */




/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
extern void PUB_SystemInit(void);
extern void PUB_PeripheralClockConfig(void);

#ifdef __cplusplus
}
#endif

#endif /* __PUB_RCCDRIVER_H */

/********************************* END OF FILE *********************************/


