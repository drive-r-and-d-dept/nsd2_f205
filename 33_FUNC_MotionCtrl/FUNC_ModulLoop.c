/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "FUNC_GlobalVariable.h"
#include "FUNC_InterfaceProcess.h"
#include "FUNC_FunCode.h"
#include "FUNC_ModuCtrl.h"
#include "FUNC_Filter.h" 
#include "MTR_PDFFCtrl.h"

#define PI2_Q10  6434  //3.1415928*2*1024
#define SPDFREQ 1
#define POSFREQ 0

MODUCTRL ModuCtrl={0};
 
STR_PDFFCONTROLLER  ModuSpd_PdffCtrl = {0} ;  

static int64 AbsSpd=0;
static int64 Kb_ScalQ38=0;
static int64 AbsPos=0;
static int64 OldAbsPos=0;
static int64 IntgRef=0;
//static int64 IntgSpd=0;
static int64 IntgTrq= 0;

 
//static STR_BILINEAR_LOWPASS_FILTER      SpdFeedForwardFilter = BILINEAR_LOWPASS_FILTER_Defaults ;     //定义位置环速度前馈滤波器结构体变量并初始化
static STR_BILINEAR_LOWPASS_FILTER      ToqFeedForwardFilter = BILINEAR_LOWPASS_FILTER_Defaults ;     //定义位置环速度前馈滤波器结构体变量并初始化

int32 ModCtrl(int32 ref);
void ModCtrlParaStpSet(void);
int32 ModuPDFFCtrl(STR_PDFFCONTROLLER *p, int32 Ref, int32 Fdb_P, int32 Fdb_I);    //PDFF控制器
void ModCtlParaRlUpdat(void);
void ModSpdCtrl(void);

void ModCtlParaRlUpdat(void)
{
	//	int64 temp;
	
	//增益更新
	ModuCtrl.poskp = FunCodeUnion.code.AT_ModuKp; //0.1Hz
	//ModuSpd_PdffCtrl.Kp = ((Uint32)FunCodeUnion.code.AT_ModuKp * (Uint16)FunCodeUnion.code.AT_ModuGainRatio)>>10;
	ModuSpd_PdffCtrl.Kp = (Uint32)FunCodeUnion.code.GN_Spd_Kp2;
	if((FunCodeUnion.code.AT_ModuGainRatioT == 51200)||(FunCodeUnion.code.AT_ModuGainRatioT==0))
	{
		ModuSpd_PdffCtrl.Ki_Q10 = 0;
	}
	else
	{
		//ModuSpd_PdffCtrl.Ki_Q10 = ((int64) FunCodeUnion.code.AT_ModuGainRatio *STR_FUNC_Gvar.System.SpdPrd_Q10)/((Uint32)FunCodeUnion.code.AT_ModuGainRatioT * 10);
	
		ModuSpd_PdffCtrl.Ki_Q10 = ((int64) FunCodeUnion.code.GN_Spd_Kp2 *STR_FUNC_Gvar.System.SpdPrd_Q10)/((Uint32)FunCodeUnion.code.GN_Spd_Ti2* 10);
	}
	 //速度指令改为四位小数0.0001rpm
	 ModuSpd_PdffCtrl.Kb_Scal_Q38 = Kb_ScalQ38 * ((int16)FunCodeUnion.code.GN_InertiaRatio + 100L);
	
}

//======================
//ModCtrlParaUpdat()
//系数更新

//======================
void ModCtrlParaStpSet(void)
{
	int64 temp;
     
	ModuCtrl.posfd=0;
	ModuCtrl.spdfd=0;
	ModuCtrl.posintg=0;
	ModuCtrl.posref =0;
	ModuCtrl.spdff =0;
	ModuCtrl.toqff =0;
	ModuCtrl.spdref =0;
	ModuCtrl.posAmp =0;
	ModuCtrl.posDir = 0;
	 
	IntgTrq=0;
	AbsSpd=0;
	AbsPos=0;
	OldAbsPos=0;
	//IntgSpd=0;
	IntgTrq= 0;
	IntgRef = 0;

	ModuSpd_PdffCtrl.KiSum = 0;
	ModuSpd_PdffCtrl.SaturaFlag = 0;
	//增益更新

	ModuCtrl.posRegCoff_Q15 = ((int64)60000L << 15) / UNI_FUNC_MTRToFUNC_InitList.List.EncRev; 	

    temp = (int32)FunCodeUnion.code.MT_RateToq * 1000000L/ (FunCodeUnion.code.MT_Inertia* (100 +FunCodeUnion.code.GN_InertiaRatio )) ;

	ModuCtrl.toq2spdCoff_Q12 = (int64)(temp * 60000000)/ (628 * STR_FUNC_Gvar.System.SpdFreq);//ToqFreq);//PosFreq); //rad/s to rpm

	//spd*encoder/60*10000*Fsamp  Q24=16777216
	ModuCtrl.spd2PosCoff_Q24 = ((int64)16777216 * UNI_FUNC_MTRToFUNC_InitList.List.EncRev) / ((Uint64)600000L * STR_FUNC_Gvar.System.SpdFreq); 


	//速度指令改为四位小数0.0001rpm
	temp = (((Uint64)2 * PI_Q12 * 4096) << 30) /100000000L;
	temp = temp * FunCodeUnion.code.MT_Inertia*4096;
	temp = ((temp * 2 * PI_Q12) >> 16) / (600);	//H0800重新带一个小数点
	Kb_ScalQ38 = temp / ((Uint32)FunCodeUnion.code.MT_RateToq * 100L);
	ModuSpd_PdffCtrl.Kb_Scal_Q38 = Kb_ScalQ38* (FunCodeUnion.code.GN_InertiaRatio + 100L);

	 
	ToqFeedForwardFilter.Ts = STR_FUNC_Gvar.System.SpdPrd_Q10 >> 10;       //单位us，在位置环调用周期

	ToqFeedForwardFilter.Tc = 500L;    //H08_18;速度前馈滤波时间参数
	ToqFeedForwardFilter.InitLowPassFilter(&ToqFeedForwardFilter);           //速度前馈滤波初始化

	ModuSpd_PdffCtrl.PosLmt = FunCodeUnion.code.MT_MaxToqOrCur/FunCodeUnion.code.MT_RateToq*4096*4096;   //转矩Q24
	ModuSpd_PdffCtrl.NegLmt = -FunCodeUnion.code.MT_MaxToqOrCur/FunCodeUnion.code.MT_RateToq*4096*4096;//转矩Q24

}

 
//==================
 

//=====================
//=======================
int32 ModCtrl(int32 ref)
{
	int64 regtemp;
	static int64 remnant=0;
	//int32 num=0;
	
	//增益更新
 	ModuCtrl.poskp = FunCodeUnion.code.AT_ModuKp; //0.1Hz

	//位置方向判断
	if(ref > 0)
	{
		ModuCtrl.posDir = 0;
	}
	else if(ref < 0)
	{
		ModuCtrl.posDir = 1;
	}
	
 	ModuCtrl.posref = ref;//Temp;
 	IntgRef+=((int64)ref<<24);
	
	ModuCtrl.posintg = IntgRef - AbsPos;
    ModuCtrl.posAmp += (ref - STR_FUNC_Gvar.PosCtrl.PosFdb);  //实际指令和实际反馈的随动偏差
	
	   
    //计算模型位置环的速度指令输出
	regtemp = (int64)ModuCtrl.poskp * ModuCtrl.posintg;
	regtemp = (regtemp * ModuCtrl.posRegCoff_Q15)>>15;	
	ModuCtrl.spdref =  (int32)(regtemp>>24);   
	
	//计算速度前馈
	regtemp = (int64)(((int64)ModuCtrl.spdref * (int16)FunCodeUnion.code.AT_ModuKffCoef) + ((int64)ModuCtrl.spdfd * ((int16)128 -(int16)FunCodeUnion.code.AT_ModuKffCoef)))>>7;
	ModuCtrl.spdff = (int32)((regtemp * FunCodeUnion.code.AT_ModuKffValu)>>10);
	
	//限制速度指令输出
	if(ModuCtrl.spdref>(FunCodeUnion.code.MT_MaxSpd*11000))
		ModuCtrl.spdref = FunCodeUnion.code.MT_MaxSpd*11000;
	else if(ModuCtrl.spdref<-(FunCodeUnion.code.MT_MaxSpd*11000))
		ModuCtrl.spdref = -FunCodeUnion.code.MT_MaxSpd*11000;
	
	
	//计算模型环的位置增量，返回给实际环路
		
	ModuCtrl.posfd =(int32)((AbsPos  - OldAbsPos)>>24);
	
	//以下计算余数，单独处理，不能叠加到模型位置上
	regtemp = AbsPos  - OldAbsPos-(((AbsPos  - OldAbsPos)>>24)<<24);
	remnant +=regtemp;

	OldAbsPos = AbsPos ;
	if((remnant>>24)>=1)		
	{			
		ModuCtrl.posfd+=1;
		remnant=remnant-16777216;
	}
	if((remnant>>24)<=-1)		
	{			
		ModuCtrl.posfd+=-1;
		remnant=remnant+16777216;
	}
	
	return ModuCtrl.posfd; 
}

int32 ModuPDFFCtrl(STR_PDFFCONTROLLER *p, int32 Ref, int32 Fdb_P, int32 Fdb_I)
{

    int32  ErrKf = 0;
    int64  OutputTemp = 0;
    int64  Err = 0;              //误差

	ErrKf = Ref - Fdb_P;
    OutputTemp = (int64)p->Kp * ErrKf;

 
    //积分部分的计算
    Err =  Ref - Fdb_I;            //误差计算
   
    //积分运算
    if(p->Ki_Q10 == 0)
    {
        p->KiSum = 0;     //PI切换到P时,之前累加和KiSum清零
    }
    else if(p->SaturaFlag == 0)
    {
        p->KiSum += ((int64)p->Ki_Q10 * (int64)Err);
    }
    else if((p->SaturaFlag == 1) && (Err > 0))   //负向遇限处理
    {
        p->KiSum += ((int64)p->Ki_Q10 * (int64)Err); 
    }
    else if((p->SaturaFlag == 2) && (Err < 0))   //正向遇限处理
    {
        p->KiSum += ((int64)p->Ki_Q10 * (int64)Err);
    } 


    OutputTemp += (p->KiSum >> 10);

    //输出定标计算
    OutputTemp = OutputTemp * (int64)p->Kb_Scal_Q38;
    OutputTemp = OutputTemp >> 38;   //Q24

	 //输出限幅处理
    if((int32)OutputTemp > p->PosLmt) 
    {
        p->SaturaFlag = 2;
    }
    else if ((int32)OutputTemp < p->NegLmt) 
    {
        p->SaturaFlag = 1;
    }
    else
    {
        p->SaturaFlag = 0;
    }
	   //输出限幅处理
    if((int32)OutputTemp > (32767*4096)) 
    {
        OutputTemp = 32767*4096;
    }
    else if ((int32)OutputTemp < -(32767*4096)) 
    {
        OutputTemp = -32767*4096;
    }

    return (int32)OutputTemp;
}

void ModSpdCtrl(void)
{
	int32 spdregtemp=0;
	static int64 IncPos =0;
	
  	spdregtemp = ModuPDFFCtrl(&ModuSpd_PdffCtrl, ModuCtrl.spdref ,ModuCtrl.spdfd, ModuCtrl.spdfd); //输出电机额定转矩百分比4096对应100%
	ModuCtrl.toqRef = spdregtemp; //转矩指令，以100%对应4096*4096表示
	
	ToqFeedForwardFilter.Input = (int32)ModuCtrl.toqRef;   //速度前馈输入
    ToqFeedForwardFilter.LowPassFilter(&ToqFeedForwardFilter);  //滤波

	//滤波结果作为前馈
	if(ModuCtrl.posDir == 0)
	{
		ModuCtrl.toqff = ((ToqFeedForwardFilter.Output>>12) * (int16)FunCodeUnion.code.AT_ModuTffPosCoff)>>10;
	}
	else  //negtive dir
	{
		ModuCtrl.toqff = ((ToqFeedForwardFilter.Output>>12) * (int16)FunCodeUnion.code.AT_ModuTffNegCoff)>>10;
	}
  	
    IntgTrq += ModuCtrl.toqRef;//spdregtemp; //积分额定100%对应4096*4096

	AbsSpd = IntgTrq * ModuCtrl.toq2spdCoff_Q12 ; //Q12 rpm 0.0001
	
	ModuCtrl.spdfd = AbsSpd >>24;
	IncPos = ((int64)AbsSpd* ModuCtrl.spd2PosCoff_Q24)>>24;
	AbsPos+=IncPos;

	STR_FUNC_Gvar.OscTarget.CH1_Test = ModuCtrl.toqRef;
	STR_FUNC_Gvar.OscTarget.CH2_Test = ModuCtrl.spdref; //model feedback
	STR_FUNC_Gvar.OscTarget.CH3_Test = ModuCtrl.spdff;
 	STR_FUNC_Gvar.OscTarget.CH4_Test = ModuCtrl.posfd;
 
	//=========================
}
