 
#ifndef FUNC_PULSE_H
#define FUNC_PULSE_H


#ifdef  __cplusplus                     //C++和C语言可兼容要求
extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */ 
#include "PUB_GlobalPrototypes.h"






/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/






/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */	




/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 

/*定义FUNC_PosCtrl.c文件内调用的变量的结构体类型*/



/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */






/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 

/*FUNC_PosCtrl.c模块共享全局函数的声明*/

extern int32 PosPulseCalc(void);
void  MaxPulsFreqSet(void);



#ifdef __cplusplus
}
#endif /* extern "C" */ 

#endif /* end of FUNC_PosCtrl.h */

/********************************* END OF FILE *********************************/

