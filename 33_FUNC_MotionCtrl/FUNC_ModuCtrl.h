#ifndef FUNC_MODUCTRL_H
#define FUNC_MODUCTRL_H 

#include "PUB_GlobalPrototypes.h" 

#include "MTR_PDFFCtrl.h"


#ifdef __cplusplus
extern "C" {
#endif   
typedef struct 
{
	int64 posintg; 			//位置粂分，随动偏差值
	int32 posfd;   			//位置反馈
	int64 posfd_Q12;
	int64 posref;
	
	int32 spdfd;   			//反馈速度
	int32 spdref;  			//参考速度
	int64 spdintg; 			//速度积分，随动偏差
	int32 toqRef;
	int8 posDir;			//运行位置方向0:正向，1负向
	//int32 toqkt;   		//转矩系数
	//int32 loadj;  		//惯量

	int32 spdkp;
	int32 spdki;
	int32 poskp;

	int32 spdff;  			//速度前馈输出
	int32 toqff;  			//转矩前馈输出
	int64 posRegCoff_Q15; 	//速度调节器系数	
	int32 toq2spdCoff_Q12;
	int32 spd2PosCoff_Q24;
	int64 PosObserve_Q30;

	int32 posAmp; 
	
}MODUCTRL;

extern MODUCTRL ModuCtrl;
//extern STR_PDFFCONTROLLER  ModuSpd_PdffCtrl;

extern int32 ModCtrl(int32 ref);
extern void ModCtrlParaStpSet(void);
extern void ModCtlParaRlUpdat(void);
extern void ModSpdCtrl(void);


#ifdef __cplusplus
}
#endif 

#endif /* end of FUNC_MODESELECT_H */


