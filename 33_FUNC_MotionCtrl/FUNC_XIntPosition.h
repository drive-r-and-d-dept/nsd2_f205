 
#ifndef FUNC_XINTPOSITION_H
#define FUNC_XINTPOSITION_H


#ifdef  __cplusplus                     //C++和C语言可兼容要求
extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */ 
#include "PUB_GlobalPrototypes.h"

/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/
#define XintPosAttribDflts  {0}


/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */	


/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */
typedef struct _STR_XINTPOS_ATTRIB
{
    int32 PosXintLatch;  // 发生中断时锁存位置
    //int32 SpdXintLatch;  // 发生中断时锁存速度

    /*union {
        Uint16 all;
        struct {
            Uint16  XINT_DIR:1; // 中断定长方向
            Uint16  Rsvd:15;    // 备用
        } bit;
    } CtrlBit;
    */
} STR_XINTPOS_ATTRIB;

// 这个结构体只是针对DI9下降沿不能触发中断而用, 后期硬件升级之后将删除
typedef struct{

    Uint16  XintPosIntGene:1;  // 中断定长的中断产生标志位
    Uint16  DI9TrgEdge:2;      // 沿标志 (1: 上升沿;  2: 下降沿; 3: 上升下降沿都可以)
    Uint16  DI9LevelState:1;   // 当前Di9端口的电平状态
    Uint16  DI9LevelLast:1;    // 上次Di9端口的电平状态
    Uint16  Rsvd:11;

}STR_XINTPOS_DRV_BIT;

typedef union{

    volatile Uint16                  all;
    volatile STR_XINTPOS_DRV_BIT     bit; 
}UNI_XINTPOS_DRV_REG;




/*FUNC_XintPosition.c文件内调用的变量的结构体类型*/


/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */
 extern UNI_XINTPOS_DRV_REG   UNI_XintPosDrive;

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
extern int32 XintPosCmd(void);


extern void XintPosDrvConfig(void);    // 底层中断配置, 与硬件相关

extern void XintPosEnJudgment(void);

extern void XintPosReset(void);

#ifdef __cplusplus
}
#endif /* extern "C" */ 

#endif /* end of FUNC_XintPosition.h */

