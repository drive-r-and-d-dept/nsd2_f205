
#ifndef __MTR_ABSENCANGLEINIT_H
#define __MTR_ABSENCANGLEINIT_H 

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "PUB_GlobalPrototypes.h"

/* Exported_Constants --------------------------------------------------------*/
/* 不带参数的宏定义 */


/* Exported_Macros -----------------------------------------------------------*/
/* 带参数的宏定义 */

/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 



/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */


/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
extern void SerialCommEncAngleInit(void);         //H0D03绝对式编码器电角度初始化,在转矩中断函数调用

#ifdef __cplusplus
}
#endif

#endif /* __MTR_ABSENCANGLEINIT_H */

/********************************* END OF FILE *********************************/
