#ifndef FUNC_MOTOROVERLOAD_H
#define FUNC_MOTOROVERLOAD_H

#ifdef __cplusplus
 extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */  
#include "PUB_GlobalPrototypes.h" 


/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/
//暂无



/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */
//暂无



/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 
//暂无



/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */
extern void InitMotorOverLoadProc(void);
extern void MotorHeatCalc(int32 current);



/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
//暂无



#ifdef __cplusplus
}
#endif

#endif



