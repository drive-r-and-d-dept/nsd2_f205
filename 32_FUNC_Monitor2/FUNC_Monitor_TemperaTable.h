 
#ifndef FUNC_MONITOR_TEMPERATABLE_H
#define FUNC_MONITOR_TEMPERATABLE_H

#ifdef __cplusplus
 extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */  
#include "PUB_GlobalPrototypes.h" 


/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/
//暂无



/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */
//暂无



/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 
//暂无



/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */
extern const Uint16 TemperaTable_AluBaseBoard[];   //0  -0.4kw     SizeA   铝基板曲线 
extern const Uint16 TemperaTable_AluBaseBoard400w[]; //0.4kw
extern const Uint16 TemperaTable_ExternalModule[]; //0.5kw-1.5kw   SizeB、SizeC  外置模块曲线
extern const Uint16 TemperaTable_BSMModule[];      //2.0kw-7.5kw   SizeD、SizeE  BSM,Infinon两种为同样的曲线



/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
//暂无



#ifdef __cplusplus
}
#endif

#endif /*FUNC_Monitor_TemperaTable.h */    

/********************************* END OF FILE *********************************/
