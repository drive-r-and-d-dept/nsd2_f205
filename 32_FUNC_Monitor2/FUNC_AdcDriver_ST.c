
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */ 
#include "FUNC_GlobalVariable.h"
#include "FUNC_FunCode.h"
#include "FUNC_ErrorCode.h" 
#include "FUNC_AdcDriver_ST.h"
#include "FUNC_Filter.h" 
#include "FUNC_GPIODriver.h" 

#if defined STM32F407
    #include "stm32f4xx_adc.h"
    #include "stm32f4xx_dma.h"
    #include "stm32f4xx_gpio.h"		
#else
    #include "stm32f2xx_adc.h"
    #include "stm32f2xx_dma.h"
	 #include "stm32f2xx_gpio.h"
#endif 

/* Private_Constants ---------------------------------------------------------*/
/* 宏定义 常数类*/
#define    ADCREFL    1365
#define    ADCREFH    3413

// DSP的AD采样后得到数字量，对于AI1和AI2，AI输入电压为0V 时，AD采样数字量为 2048, 
// 即 0<==>2048，而 -12v<==>4096, +12V <==>0 ，是反向线性映射的关系
#define  ONCHIP_AD_ZERO     2048L


//母线电压的范围：对应不同的驱动器电压等级
//#define DC220MAX 4640L  //0.1V  MD300S2.2  500V,
#define DC220MAX           5000L
//IS500  3.3V对应1000V  AD采样时数字量4096对应低压3V即母线电压1000V 
#define DC380MAX_IS500     10000L  
/* Private_Macros ------------------------------------------------------------*/
/* 宏定义 函数类 */
//暂无


/* Private_TypesDefinitions --------------------------------------------------*/ 
/* 结构体变量定义 枚举变量定义 */  




/* Private_Variables ---------------------------------------------------------*/
/* 变量定义 */     
volatile Uint32 ADC_RegularConvertedValueTab[5]; //存储AD转换的初始值,是DMA存储的目标

static int32  g_CalGain;
static int16  g_CalOffset;
static Uint16 Avg_AdcRefH = 0 ; //实际采样值
static Uint16 Avg_AdcRefL = 0 ; //实际采样值

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */   
void InitAdc(void);                     
void Get_ADC_ChannelValueAIUdc_16k(void);   //AI和母线电压Udc采样16k获取
void Get_ADC_ChannelValueIPM_1k(void);      //IPM温度采样获取

/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部调用的函数的声明 */ 
Static_Inline void ADSampConfig(void);
Static_Inline void ADCOfftrimInit(void);
Static_Inline void ADCCalibration(void);
Static_Inline void GetADCInitValue(void);

//Static_Inline void DMA1_Channel1_IRQHandler(void);
//Static_Inline Uint16 GetOnchipAdValue(Uint16 AdChannel);
//Static_Inline void GetADCInitValue(Uint16 AdChannel, Uint16 *AdcSeqValue);

/*******************************************************************************
函数名: void InitAdc(void)   初始化ADC配置
输入  : 无  
输出  : 无  
子函数: void ADCOfftrimInit(void);
        void ADSampConfig(void);
        void ADC_SoftwareStartConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState);     
描述  : 对AD的初始化操作
        1.对AD采样值自动纠偏；
        2.AD底层配置：通道分配、转换模式、中断、DMA配置等；
        3.对AD采样缓存赋初值：2048 
********************************************************************************/ 
void InitAdc(void)
{    
    //--------------AD采样外设配置----------------------------------------//       
    FUNC_GPIOC->MODER.bit.MODER0 = FUNC_GPIO_Mode_AN;      //VREF1.0 PC0  模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR0 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOC->MODER.bit.MODER1 = FUNC_GPIO_Mode_AN;      //VREF2.5 PC1 模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR1 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOC->MODER.bit.MODER2 = FUNC_GPIO_Mode_AN;      //AD-DC PC2   模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR2 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOC->MODER.bit.MODER3 = FUNC_GPIO_Mode_AN;      //AD-AI1 PC3  模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR3 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOC->MODER.bit.MODER4 = FUNC_GPIO_Mode_AN;      //AD-TEMP PC4 模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR4 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOC->MODER.bit.MODER5 = FUNC_GPIO_Mode_AN;      //AD-PWR PC5  模拟输入
    FUNC_GPIOC->PUPDR.bit.PUPDR5 = FUNC_GPIO_PuPd_NOPULL;
    FUNC_GPIOA->MODER.bit.MODER2 = FUNC_GPIO_Mode_AN;      //AD-AI2 PA2  模拟输入
    FUNC_GPIOA->PUPDR.bit.PUPDR2 = FUNC_GPIO_PuPd_NOPULL;

    //调用纠偏函数
    ADCOfftrimInit(); 


    //调用工作模式配置模式
    ADSampConfig();
   
    ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
    /*AD上电*/
    ADC_Cmd(ADC1, ENABLE);
    ADC_Cmd(ADC2, ENABLE);  

    //初始化之后启动一次转换  调用stm32f20x_adc.c函数
    ADC_SoftwareStartConv(ADC1);
   
   //赋初值
    STR_FUNC_Gvar.ADC_Samp.AI1  = 2048;     //AI1  通道13
    STR_FUNC_Gvar.ADC_Samp.AI2  = 2048;     //AI2  通道2
    STR_FUNC_Gvar.ADC_Samp.Udc_Live  = 0;   //DC   通道12
    STR_FUNC_Gvar.ADC_Samp.IPMT = 2048;     //Temp 通道14
    STR_FUNC_Gvar.ADC_Samp.Pwr  = 0;     	//PWR  通道15
}


/*******************************************************************************
函数名: void ADCOfftrimInit(void)   AD自动纠偏，ST的DSP自带此功能 在函数InitAdc()中被调用
输入  : 无    
输出  : 无  
子函数: void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState); 
        void ADC_ResetCalibration(ADC_TypeDef* ADCx); 
        FlagStatus ADC_GetResetCalibrationStatus(ADC_TypeDef* ADCx);        
描述  : AD自动纠偏，ST的DSP自带此功能 
********************************************************************************/ 
Static_Inline void ADCOfftrimInit(void) 
{
    //这里面按照TI平台的处理进行零漂和线性度的校正
    //ADCCalibration(/*_iq12 * */&g_CalGain, /*int16 **/ &g_CalOffset);
    ADCCalibration();

    ADC_Cmd(ADC1, DISABLE);
    ADC_Cmd(ADC2, DISABLE);

    DELAY_US(50); 
}


/*******************************************************************************
函数名: ADCCalibration(void)   获取AD纠偏基准源的采样值
输入  : 无    
输出  : CalGain     Q12
        CalOffset   Q0 
子函数:  
         
               
描述  : 获取CalGain, CalOffset; 
********************************************************************************/ 
Static_Inline void ADCCalibration()
{
    Uint16 AdcRefLReaL = ADCREFL;     //理想值1V
    Uint16 AdcRefHReaL = ADCREFH;     //理想值2.5V

    //获取基准源采样数据
    GetADCInitValue();

    g_CalGain   = (((int32)AdcRefHReaL  - AdcRefLReaL)<<12) / (int32)(Avg_AdcRefH - Avg_AdcRefL);
   
    g_CalOffset = (int16)AdcRefLReaL  - (int16)((Avg_AdcRefL* (g_CalGain) + (1<<11))>>12);
}


/*******************************************************************************
函数名: GetADCInitValue(void)   获取AD纠偏基准源的采样值
输入  : 无    
输出  : 无  
子函数:  
         
               
描述  :  
********************************************************************************/ 
Static_Inline void GetADCInitValue()                   
{
    static Uint32 ADCChannelValue[8]; 
    Uint16 i = 0;
    Uint16 j = 0;
    Uint16 ADCInitA = 0;
    Uint16 ADCInitB = 0;
    Uint32 SumADCInitA = 0;
    Uint32 SumADCInitB = 0;

    //工作模式的配置

    DMA_InitTypeDef   DMA_InitStructure;
    ADC_InitTypeDef   ADC_InitStructure;
    ADC_CommonInitTypeDef ADC_CommonInitStructure;

    /*********************************DMA2 Channel0 配置**********************************************************/

    DMA_DeInit(DMA2_Stream0);
    DMA_InitStructure.DMA_Channel = DMA_Channel_0; 
    DMA_InitStructure.DMA_Memory0BaseAddr = (Uint32)&ADCChannelValue;
    DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_BASE + 0x308;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = 8;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;        
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA2_Stream0, &DMA_InitStructure);

    /* DMA2_Stream0 enable */
    DMA_Cmd(DMA2_Stream0, ENABLE);

    /*********************************ADC1 配置**********************************************************/
    ADC_DeInit();

     /* ADC Common Init **********************************************************/
    ADC_CommonInitStructure.ADC_Mode = ADC_DualMode_RegSimult;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_2;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
    ADC_CommonInit(&ADC_CommonInitStructure);

    /* ADC1 Init ****************************************************************/
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;  
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 8;
    ADC_Init(ADC1, &ADC_InitStructure);

    ADC_DMACmd(ADC1,ENABLE);
    ADC_DMARequestAfterLastTransferCmd(ADC1,ENABLE);
    ADC_EOCOnEachRegularChannelCmd(ADC1,ENABLE);

  /* ADC1 regular channel7 configuration *************************************/
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 1, ADC_SampleTime_3Cycles);      //设channel 11( 2.5V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 2, ADC_SampleTime_3Cycles);      //设channel 10( 1.0V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 3, ADC_SampleTime_3Cycles);      //设channel 11( 2.5V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 4, ADC_SampleTime_3Cycles);      //设channel 10( 1.0V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 5, ADC_SampleTime_3Cycles);      //设channel 11( 2.5V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 6, ADC_SampleTime_3Cycles);      //设channel 10( 1.0V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 7, ADC_SampleTime_3Cycles);      //设channel 11( 2.5V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 8, ADC_SampleTime_3Cycles);      //设channel 10( 1.0V )为第2个转换，采样时间1.5个AD周期
    /* ADC2 Init ****************************************************************/
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;  
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 8;
    ADC_Init(ADC2, &ADC_InitStructure);

  /* ADC2 regular channel7 configuration *************************************/
    ADC_RegularChannelConfig(ADC2, ADC_Channel_10, 1, ADC_SampleTime_3Cycles);    //设channel 10 ( 1.0V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_11,2, ADC_SampleTime_3Cycles);     //设channel 11 ( 2.5V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_10, 3, ADC_SampleTime_3Cycles);    //设channel 10 ( 1.0V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_11,4, ADC_SampleTime_3Cycles);     //设channel 11 ( 2.5V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_10, 5, ADC_SampleTime_3Cycles);    //设channel 10 ( 1.0V )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_11,6, ADC_SampleTime_3Cycles);     //设channel 11 ( 2.5V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_10, 7, ADC_SampleTime_3Cycles);    //设channel 10 ( 1.0V )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_11,8, ADC_SampleTime_3Cycles);     //设channel 11 ( 2.5V )为第2个转换，采样时间1.5个AD周期
  
    /*AD上电*/
    ADC_Cmd(ADC1, ENABLE);
    ADC_Cmd(ADC2, ENABLE); 

    DELAY_US(50);
    
    for (i = 0; i < 16; i++)
    {
        ADCChannelValue[i] = 0;
    }
    
    //前50次采样值误差大，不用
    for (i = 0; i < 50; i++)
    {
        ADC_SoftwareStartConv(ADC1);
        //等待AD转换完成    
        while(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));  //ADC转换完成

        ADC_ClearFlag(ADC1,ADC_FLAG_EOC);            //清楚转换完成标志   
    }
    
    for (i = 0; i < INIT_AD_SAMPLETIMES; i++)
    {
        //重新赋值
        ADCInitA = 0; 
        ADCInitB = 0; 

        ADC_SoftwareStartConv(ADC1);
        //等待AD转换完成    
        while(ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));  //ADC转换完成

        ADC_ClearFlag(ADC1,ADC_FLAG_EOC);            //清楚转换完成标志   
    
        for (j = 0; j < 8; )
        {    
            ADCInitA += (ADCChannelValue[j]   & 0x0000ffff);  //2.5 通道;
            ADCInitB += (ADCChannelValue[j] >>16)          ;  //1V  
            j++;
            ADCInitA += (ADCChannelValue[j] >>16);  //2.5 通道;
            ADCInitB += (ADCChannelValue[j]   & 0x0000ffff);  //1V
            j++;  
        } 
        
        //此步求取可用于存储分析AD采样值
        ADCInitA = ADCInitA >> 3;   
        ADCInitB = ADCInitB >> 3;
        
        //获取采样值总和
        SumADCInitA += ADCInitA;
        SumADCInitB += ADCInitB;              
    }
    
    //获取AD多次采样的平均值   
    Avg_AdcRefH = (Uint16)(SumADCInitA >> INIT_AD_SAMPLETIMES_BIT);
    Avg_AdcRefL = (Uint16)(SumADCInitB >> INIT_AD_SAMPLETIMES_BIT);    
}


/*******************************************************************************
函数名:  void ADSampConfig(void)       在函数InitAdc()中被调用
输入  :  无  
输出  :  无  
子函数:  void DMA_DeInit(DMA_Channel_TypeDef* DMAy_Channelx)；
         void DMA_Init(DMA_Channel_TypeDef* DMAy_Channelx, DMA_InitTypeDef* DMA_InitStruct)；
         void DMA_ITConfig(DMA_Channel_TypeDef* DMAy_Channelx, uint32_t DMA_IT, FunctionalState NewState)；
         void DMA_Cmd(DMA_Channel_TypeDef* DMAy_Channelx, FunctionalState NewState)；
         void ADC_Init(ADC_TypeDef* ADCx, ADC_InitTypeDef* ADC_InitStruct)；
         void ADC_RegularChannelConfig(ADC_TypeDef* ADCx, uint8_t ADC_Channel, uint8_t Rank, uint8_t ADC_SampleTime)；
         void ADC_DMACmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         void ADC_ExternalTrigConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState)；       
描述  :  1.配置DMA：外设AD地址、DMA存储器地址，从AD读取7组32位数据到DMA1_channel1存储区，循环模式，非存储器对存储器模式；
         2.配置AD ：双AD模式，同步规则通道采样，AD1与AD2同步采样，但转换顺序不一样，使能DMA请求，采样窗宽度，外部触发（TIM1CC1事件）；
********************************************************************************/
Static_Inline void ADSampConfig(void)
{
    DMA_InitTypeDef   DMA_InitStructure;
    ADC_InitTypeDef   ADC_InitStructure;
    ADC_CommonInitTypeDef ADC_CommonInitStructure;

    /*********************************DMA2 Channel0 配置**********************************************************/
    DMA_DeInit(DMA2_Stream0);
    DMA_InitStructure.DMA_Channel = DMA_Channel_0; 
    DMA_InitStructure.DMA_Memory0BaseAddr = (Uint32)&ADC_RegularConvertedValueTab;
    DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_BASE + 0x308;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = 5;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA2_Stream0, &DMA_InitStructure);

    /* DMA2_Stream0 enable */
    DMA_Cmd(DMA2_Stream0, ENABLE);

 
    /*********************************ADC1 配置**********************************************************/
    /*复位ADC1 & ADC2*/ 
    ADC_DeInit();

     /* ADC Common Init **********************************************************/
    ADC_CommonInitStructure.ADC_Mode = ADC_DualMode_RegSimult;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_2;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
    ADC_CommonInit(&ADC_CommonInitStructure);

    /* ADC1 Init ****************************************************************/
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;  
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 5;
    ADC_Init(ADC1, &ADC_InitStructure);
    
//    ADC_DMACmd(ADC1,ENABLE);
//    ADC_DMARequestAfterLastTransferCmd(ADC1,ENABLE);

  /* ADC1 regular channel7 configuration *************************************/
    ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 1, ADC_SampleTime_3Cycles);      //设channel 13(  AI1  )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 2, ADC_SampleTime_3Cycles);      //设channel 14(  Temp )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_2 , 3, ADC_SampleTime_3Cycles);      //设channel  2(  AI2  )为第3个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 4, ADC_SampleTime_3Cycles);      //设channel 12(  DC   )为第4个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 5, ADC_SampleTime_3Cycles);      //设channel 15(  Pwr  )为第4个转换，采样时间1.5个AD周期
    /* ADC2 Init ****************************************************************/
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = ENABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;  
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 5;
    ADC_Init(ADC2, &ADC_InitStructure);

  /* ADC2 regular channel7 configuration *************************************/
    ADC_RegularChannelConfig(ADC2, ADC_Channel_2, 1, ADC_SampleTime_3Cycles);     //设channel  2(  AI2  )为第1个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_12,2, ADC_SampleTime_3Cycles);     //设channel 12(  DC )为第2个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_15,3, ADC_SampleTime_3Cycles);     //设channel 15(  Pwr  )为第4个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_13,4, ADC_SampleTime_3Cycles);     //设channel 13(  AI1  )为第3个转换，采样时间1.5个AD周期
    ADC_RegularChannelConfig(ADC2, ADC_Channel_14,5, ADC_SampleTime_3Cycles);     //设channel 14(  Temp )为第4个转换，采样时间1.5个AD周期
 
    /*AD上电*/
//    ADC_Cmd(ADC1, ENABLE);
//    ADC_Cmd(ADC2, ENABLE); 
}


/*******************************************************************************
函数名: void Get_ADC_ChannelValueAIUdc_16k(void); 
输入  : 无   
输出  : 无   
子函数: void PostErrMsg(Uint16 PostErrCode)         
描述  : DMA操作，从AD读取AI和母线电压值采样数据到DMA存储区
        Channel_AI1    13   //AI1                  Channel_AI2    2    //AI2
        Channel_AI3    9    //AI3                  Channel_Udc    12   //母线电压AD通道
        Channel_IPMT   14   //模块温度AD通道       Channel_Pwr    15   //控制电掉电监测
        Channel_Mot    13   //电机温度监测，目前没有，实际接的是REF25 
********************************************************************************/ 
void Get_ADC_ChannelValueAIUdc_16k(void)
{
    static STR_MidFilter     AI1ChanelFilter = MidFilter_Defaults ;
    static STR_MidFilter     AI2ChanelFilter = MidFilter_Defaults ;
    static STR_MidFilter     UdcChanelFilter = MidFilter_Defaults ;
    int32 AI1_Temp = 0;
    int32 AI2_Temp = 0;
    int32 Udc_Temp = 0;
    int32 UdcCoef = 0;              //母线调节器系数更新值

    // 将DMA读取的数据按实际变量的数据宽度进行分离，存储区位32位，实际变量为16位
    AI1_Temp  = (ADC_RegularConvertedValueTab[0] & 0x0000ffff) + (ADC_RegularConvertedValueTab[3] >>16);  //AI1 通道
    //AI1中值滤波 取最近三次采样中间值
    AI1_Temp  =    (((int32)AI1_Temp * g_CalGain) >>13) + g_CalOffset;
    if(AI1_Temp < 0)
    {
        AI1_Temp = 0 ;
    }
    
    AI1ChanelFilter.InPut = AI1_Temp;
    MidFilter(&AI1ChanelFilter);
    
    if(FunCodeUnion.code.AI1_MidFltrEn == 1)
    {
        AI1_Temp = AI1ChanelFilter.OutPut;
    }
    
    //校正最后使得-12v => -32767;  12v => 32767
    STR_FUNC_Gvar.ADC_Samp.AI1 = (ONCHIP_AD_ZERO - AI1_Temp) << 4;


    if(0 == STR_FUNC_Gvar.AI.HighPrecisionAIEn)
    {
        // 将DMA读取的数据按实际变量的数据宽度进行分离，存储区位32位，实际变量为16位 
        AI2_Temp  = (ADC_RegularConvertedValueTab[2] & 0x0000ffff) + (ADC_RegularConvertedValueTab[0] >>16); //AI2 通道
        //AI2中值滤波 取最近三次采样中间值
        AI2_Temp  =    (((int32)AI2_Temp * g_CalGain) >>13) + g_CalOffset;
        if(AI1_Temp < 0)
        {
            AI2_Temp = 0 ;
        }
        
        AI2ChanelFilter.InPut = AI2_Temp;
        MidFilter(&AI2ChanelFilter);
        
        if(FunCodeUnion.code.AI2_MidFltrEn == 1)
        {
            AI2_Temp = AI2ChanelFilter.OutPut;
        }
        
        //校正最后使得-12v => -32767;  12v => 32767
        STR_FUNC_Gvar.ADC_Samp.AI2 = (ONCHIP_AD_ZERO - AI2_Temp) << 4;
    }

    // 将DMA读取的数据按实际变量的数据宽度进行分离，存储区位32位，实际变量为16位
    Udc_Temp  = (ADC_RegularConvertedValueTab[3] & 0x0000ffff) + (ADC_RegularConvertedValueTab[1] >>16); //DC  通道
    //Udc中值滤波 取最近三次采样中间值
    Udc_Temp  = (((int32)Udc_Temp * g_CalGain) >>13) + g_CalOffset;
    if(Udc_Temp < 0)
    {
        Udc_Temp = 0 ;
    }
    UdcChanelFilter.InPut = Udc_Temp;

	AvgFilter(&UdcChanelFilter); 

    //母线电压采样系数220V
    if(FunCodeUnion.code.OEM_VoltClass == 220)      //H01_04  OEM驱动器电压级
    {   
        //母线电压增益，3V对应母线电压的最高电压500v,单位0.1v  
        //#define DC220MAX  5000L   即ADC数字量4096对应实际母线电压5000（0.1V）
        UdcCoef = DC220MAX * FunCodeUnion.code.OEM_UdcGain / 1000;
    }
    else if(FunCodeUnion.code.OEM_VoltClass == 380)
    {   
        //IS500 3V对应1000V即ADC数字量4096对应实际母线电压10000（0.1V） #define DC380MAX_IS500 10000L    
        UdcCoef = DC380MAX_IS500 * FunCodeUnion.code.OEM_UdcGain / 1000;
    }

    //母线电压采样结果进行运算Udc_Live为的实时母线电压值，单位对应0.1v
    //IS500 220V时3V对应母线电压的最高电压500v，也对应ADC数字量4096
    //IS500_380V时3V对应母线电压的最高电压1000v，也对应ADC数字量4096
    //IS550_380V借用变频器结构3.3V对应母线电压的最高电压1000v，AD采样数字量4096对应低压3V即母线电压909V 
    STR_FUNC_Gvar.ADC_Samp.Udc_Live = (int32)((UdcCoef * (UdcChanelFilter.OutPut)) >> 12); 
}


/*******************************************************************************
函数名: void Get_ADC_ChannelValueIPM_1k(void) 
输入  : 无   
输出  : 无   
子函数: void PostErrMsg(Uint16 PostErrCode)         
描述  : DMA操作，从AD读取模块温度采样数据到DMA存储区
        Channel_AI1    13   //AI1                  Channel_AI2    2    //AI2
        Channel_AI3    9    //AI3                  Channel_Udc    12   //母线电压AD通道
        Channel_IPMT   14   //模块温度AD通道       Channel_Pwr    15   //控制电掉电监测
        Channel_Mot    13   //电机温度监测，目前没有，实际接的是REF25 
********************************************************************************/ 
void Get_ADC_ChannelValueIPM_1k(void)
{
    int32 IPMT_Temp = 0;

     /* 将DMA读取的数据按实际变量的数据宽度进行分离，存储区位32位，实际变量为16位 */
    IPMT_Temp = (ADC_RegularConvertedValueTab[1] & 0x0000ffff) + (ADC_RegularConvertedValueTab[4] >>16);//Temp 通道
    IPMT_Temp = (((int32)IPMT_Temp * g_CalGain) >>13) + g_CalOffset;
    if(IPMT_Temp < 0)
    {
        IPMT_Temp = 0;
    }
    STR_FUNC_Gvar.ADC_Samp.IPMT = IPMT_Temp;
}


/*******************************************************************************
函数名:
输入  : 无
输出  : 无
描述  : 
********************************************************************************/ 
void Get_ADC_ChannelValuePwr_1k(void)
{
    int32 Pwr_Temp = 0;

    Pwr_Temp  = (ADC_RegularConvertedValueTab[4] & 0x0000ffff) + (ADC_RegularConvertedValueTab[2] >>16); //Pwr  通道

    Pwr_Temp  = (((int32)Pwr_Temp * g_CalGain) >>13) + g_CalOffset;
    if(Pwr_Temp < 0)
    {
        Pwr_Temp = 0 ;
    }

    STR_FUNC_Gvar.ADC_Samp.Pwr = Pwr_Temp;
}


/*******************************************************************************
函数名: Uint16 GetOnchipAdValue(Uint16 AdChannel) 
输入  : 采样通道序号         Uint16 AdChannel   
输出  : 限幅之后得到的采样值 Uint16 SampleValue  
子函数: 无        
描述  : 提取指定AD通道的采样值，该数据由DMA读取来，并做限幅处理 
********************************************************************************/ 
/*
Static_Inline Uint16 GetOnchipAdValue(Uint16 AdChannel)
{
    int16 SampleValue = 2048;

    if(AdChannel > 15)
    {
        return 0; 
    }
    SampleValue = STR_MTR_Gvar.DriveADC.ChannelValue[AdChannel];

    if(SampleValue < 0)   
    {
        SampleValue = 0;
    }
    else if(SampleValue >4095) 
    {
        SampleValue = 4095; 
    }
    return SampleValue;
}
*/

/*******************************************************************************
函数名:  void GetADCInitValue(Uint16 AdChannel, Uint16 *AdcSeqValue)   
输入  :  要采样的通道序号AdChannel,   采样平均值存储地址&AdcSeqValue
输出  :  无   
子函数:  void ADC_InjectedSequencerLengthConfig(ADC_TypeDef* ADCx, uint8_t Length)；
         void ADC_InjectedChannelConfig(ADC_TypeDef* ADCx, uint8_t ADC_Channel, uint8_t Rank, uint8_t ADC_SampleTime)；
         void ADC_ExternalTrigInjectedConvConfig(ADC_TypeDef* ADCx, uint32_t ADC_ExternalTrigInjecConv)；
         void ADC_ExternalTrigInjectedConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         void ADC_InjectedDiscModeCmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         void ADC_SoftwareStartInjectedConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState)；
         FlagStatus ADC_GetFlagStatus(ADC_TypeDef* ADCx, uint8_t ADC_FLAG)；
         uint16_t ADC_GetInjectedConversionValue(ADC_TypeDef* ADCx, uint8_t ADC_InjectedChannel)           
描述  :  单独对某通道进行采样，先丢掉该采样的前50次（因为误差大），然后对接下来的64*4次采样结果取平均值，传递到采样平均值存储地址&AdcSeqValue中
********************************************************************************/ 
/*
Static_Inline void GetADCInitValue(Uint16 AdChannel, Uint16 *AdcSeqValue)                 
{    
    Uint16 i,j;     
    Uint16 ADCInit = 0;       
    Uint32 SumADCInit = 0;
    ADC_InitTypeDef ADC_InitStructure_AD;

    //复位ADC1 
    ADC_DeInit(ADC1);
    //ADC1配置  
    ADC_InitStructure_AD.ADC_Mode = ADC_Mode_Independent;                        //独立工作模式    
    ADC_InitStructure_AD.ADC_ScanConvMode = ENABLE;                              //扫描模式，每次触发会连续完成所有通道的转换
    ADC_InitStructure_AD.ADC_ContinuousConvMode = DISABLE;                       //非连续模式
    ADC_ExternalTrigInjectedConvConfig(ADC1, ADC_ExternalTrigInjecConv_None); //使用软件触发,需针对注入通道来设置
    ADC_InitStructure_AD.ADC_DataAlign = ADC_DataAlign_Right;                    //数据右对齐
    ADC_InjectedSequencerLengthConfig(ADC1, 4);                               //转换序列长度4，需针对注入通道来设置
    ADC_Init(ADC1, &ADC_InitStructure_AD);                                    //将以上设置值写入AD1的配置寄存器
    // 设置ADC1注入通道的通道分配以及采样时间  
    ADC_InjectedChannelConfig(ADC1, AdChannel, 1, ADC_SampleTime_13Cycles5);  //设AdChannel为第1个转换，采样时间13.5个AD周期,
    ADC_InjectedChannelConfig(ADC1, AdChannel, 2, ADC_SampleTime_13Cycles5);  //接下来的3个转换都针对AdChannel
    ADC_InjectedChannelConfig(ADC1, AdChannel, 3, ADC_SampleTime_13Cycles5);
    ADC_InjectedChannelConfig(ADC1, AdChannel, 4, ADC_SampleTime_13Cycles5);
    // 设置ADC1注入通道的触发方式 
    ADC_ExternalTrigInjectedConvCmd(ADC1, ENABLE);                            //使能外部触发，需针对注入通道来设置
    //ADC1上电 
    ADC_Cmd(ADC1, ENABLE);              

    //前50次采样值误差大，丢弃
    for (i = 0; i < 50; i++) 
    {   
        ADC_SoftwareStartInjectedConvCmd(ADC1, ENABLE);                       //启动注入通道转换
    
        //等待AD转换完成  
        while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_JEOC));                       //等待注入通道转换结束
    }
    
    for (i = 0; i < INIT_AD_SAMPLETIMES; i++)
    {        
        //重新赋值
        ADCInit = 0; 
        
        //读取AD通道采样值
        for (j = 0; j < 4; j++)
        { 
            ADC_SoftwareStartInjectedConvCmd(ADC1, ENABLE);                    //启动注入通道转换
            //等待AD转换完成  
            while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_JEOC ));                    //等待注入通道转换结束   
            ADCInit += ADC_GetInjectedConversionValue(ADC1, (j<<2) );          //获取注入通道转换结果                            
        } 
        //求4次转换的平均值
        ADCInit = ADCInit >> 2;         
        
        //获取采样值总和
        SumADCInit += ADCInit;                 
    }
    
    //获取AD多次采样的平均值   
    *AdcSeqValue = (Uint16)(SumADCInit >> INIT_AD_SAMPLETIMES_BIT);           
}
*/

//原中断读取AD值程序，暂留
/*******************************************************************************
函数名: void DMA1_Channel1_IRQHandler(void)  
输入  : 无   
输出  : 无   
子函数: void PostErrMsg(Uint16 PostErrCode)         
描述  : DMA操作，从AD读取采样数据到DMA存储区 
********************************************************************************/ 
/*
Static_Inline void DMA1_Channel1_IRQHandler(void)
{
    static Uint16 start = 0;      //中断正常与否的状态标志
    if(DMA1->ISR & DMA_ISR_TCIF1) //检查DMA“传输完成中断标志位”，是否已传输完成
    {
        //清除中断标志位 
        DMA1->IFCR = DMA1_FLAG_TC1;  
    
        //将DMA读取的数据按实际变量的数据宽度进行分离，存储区位32位，实际变量为16位 
        STR_MTR_Gvar.ADC_Samp.ChannelValue[0] = ADC_RegularConvertedValueTab[0] & 0x0000ffff;        //AI1
        STR_MTR_Gvar.ADC_Samp.ChannelValue[1] = (ADC_RegularConvertedValueTab[0] & 0xffff0000) >>16; //AI2
        STR_MTR_Gvar.ADC_Samp.ChannelValue[2] = ADC_RegularConvertedValueTab[1] & 0x0000ffff;        //AI3
        STR_MTR_Gvar.ADC_Samp.ChannelValue[3] = (ADC_RegularConvertedValueTab[1] & 0xffff0000) >>16; //DC
        STR_MTR_Gvar.ADC_Samp.ChannelValue[4] = ADC_RegularConvertedValueTab[2] & 0x0000ffff;        //Temp
        STR_MTR_Gvar.ADC_Samp.ChannelValue[5] = (ADC_RegularConvertedValueTab[2] & 0xffff0000) >>16; //PWR
        STR_MTR_Gvar.ADC_Samp.ChannelValue[6] = ADC_RegularConvertedValueTab[3] & 0x0000ffff;        //REF25
        STR_MTR_Gvar.ADC_Samp.ChannelValue[7] = (ADC_RegularConvertedValueTab[3] & 0xffff0000) >>16; //AI1

        STR_MTR_Gvar.ADC_Samp.ChannelValue[8] = ADC_RegularConvertedValueTab[4] & 0x0000ffff;        //AI2
        STR_MTR_Gvar.ADC_Samp.ChannelValue[9] = (ADC_RegularConvertedValueTab[4] & 0xffff0000) >>16; //AI3
        STR_MTR_Gvar.ADC_Samp.ChannelValue[10] = ADC_RegularConvertedValueTab[5] & 0x0000ffff;       //DC
        STR_MTR_Gvar.ADC_Samp.ChannelValue[11] = (ADC_RegularConvertedValueTab[5] & 0xffff0000) >>16;//Temp
        STR_MTR_Gvar.ADC_Samp.ChannelValue[12] = ADC_RegularConvertedValueTab[6] & 0x0000ffff;       //PWR
        STR_MTR_Gvar.ADC_Samp.ChannelValue[13] = 0;//给电机温度留出空位，硬件上接的实际是REF25
//      STR_MTR_Gvar.ADC_Samp.ChannelValue[13] = (ADC_RegularConvertedValueTab[6] & 0xffff0000) >>16;//REF25                                                 

        start = 1; 
    }
    else if(1 == start)  //转换没完成 
    {
        //AD转换未完成报错处理
        PostErrMsg(AD_SAMPLE_ERR);
    }    
}
*/
/********************************* END OF FILE *********************************/
