 
#ifndef FUNC_MONITOR_TEMPERATURE_H
#define FUNC_MONITOR_TEMPERATURE_H

#ifdef __cplusplus
 extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */  
#include "PUB_GlobalPrototypes.h" 


/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/




/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */
//暂无



/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 
//----------------------------------------------------------------------------
//AD采样函数
//片内AD采样数据结构体,此结构体内的变量直接可以被算法调用，其量纲符合算法调度
typedef struct{
     Uint16  ADCValue;
     Uint16  ErrValue;    
	 Uint16  FanSt;	    
}STR_MONITOR_IPMTEMPERATURE;



/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */







/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
extern void InitTemperatureProcess(void);     //上电初始化IPM温度测量参数配置
extern void Tempera_1k_Monitor(void);         //母线电压监控相关报错
extern void GetIPM_4Hz_Temperature(void);    //获取IPM温度交由H0b组显示



#ifdef __cplusplus
}
#endif

#endif /*FUNC_Monitor_Temperature.h*/    

/********************************* END OF FILE *********************************/
