 
#ifndef __FUNC_AODRIVER_ST_H
#define __FUNC_AODRIVER_ST_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "PUB_GlobalPrototypes.h"

/* Exported_Constants --------------------------------------------------------*/
/* 不带参数的宏定义 */

/* Exported_Macros -----------------------------------------------------------*/
/* 带参数的宏定义 */

/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */ 

/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */


/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 
extern void InitTim3(void);
extern void AoConst_Update(void);
extern void AoProcess(void);


#ifdef __cplusplus
}
#endif

#endif /* __FUNC_AODRIVER_ST_H */

/********************************* END OF FILE *********************************/
