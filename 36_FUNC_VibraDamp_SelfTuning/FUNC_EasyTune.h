 
#ifndef FUNC_EASYTUNE_H
#define FUNC_EASYTUNE_H


#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */ 
#include "PUB_GlobalPrototypes.h"

/* Exported_Constants --------------------------------------------------------*/
/* 宏定义 常数类*/

// 定义2f69的几个状态
#define ActiveClose 0  //点x关闭界面
#define PageOne 1     //在第一页
#define PageTwo 2    //在第二页
#define PageThree 3   //在第三页
#define AutoClose 5   //自调整结束后自动关闭界面

#define GAINTOOLOW  0x0982    //刚性等级太低
/* 文件内变量定义 */
//0x0900 伺服系统其他故障（需要读取h0b组功能码）
//0x0980 摩擦过大或负载过重
#define COINERR 0x0981//0x0981 定位完成没有效输出
//0x0983 预设增益不合理
#define SPDCMDTOOLOW 0x0984 
#define COMMBREAKOFF 0x985//0x0985 通讯中断，自动退出。超10S没有读取状态信息
#define WRONGTUNMODE 0x986   //转矩模式下不能增益调整 只能惯量辨识
#define TUNFAILED 0x0987     //自调整最终无法满足要求



/* Exported_Macros -----------------------------------------------------------*/
/* 宏定义 函数类 */	


/* Exported_Types ------------------------------------------------------------*/ 
/* 结构体变量类型定义 枚举变量类型定义 */
		/*
		Bit0-bit3。辨识前状态与辨识中状态
		0: 准备态
		1：rdy
		2：son
		3：err
		4：设置准备OK
		5：son辨识中
		6：son辨识完成
		
		*/

	typedef enum
	{
		SERVO_NRD=0,
		SERVO_RDY,
		SERVO_SON,
		SERVO_ERR,
		SERVO_PARA_OK,
		SERVO_IDENTFYING,
		SERVO_IDENTFY_FINISH
		
		
	}ENUM_RUNSTAT;
	/*
	Bit8-bit15
	0:准备 00000000
	1: 辨识启动 00000010
	2：惯量辨识完00000110
	3：摩擦辨识完00001110
	4：发生振动00011110
	5：刚性调整完00111110
	6：模型调整完01111110
	7：调整完成11111110
	*/
	typedef enum
	{
		READY_TO_TUNE=0,
		ACTIVE_INERTIA_IDENTFY=0x02,
		FINISH_INERTIA_IDENTFY=0x04,
		FINISH_FRICTION_IDENTFY=0x08,
		OSCILATION_HAPPEND=0x10,
		FINISH_GAIN_TUNE=0x20,
		FINISH_MODU_TUNE=0x40,
		FINISH_ALL_TUNE=0x80		
		
	}ENUM_TUNSTAT;

	/*
	0: 准备
	1: 惯量辨识+ 摩擦辨识
	2: 最高刚性测试
	3: 模型跟踪测试
	4: 辨识结束
	*/	
	typedef enum
	{
		STEP_READY_TO_IDENTFY =0,
		STEP_INERTIA_IDENTFY,
		STEP_GAINLEVEL_TUNE,
		STEP_MODU_TUNE,
		STEP_IDENTFY_FINISH
		
	}ENUM_IDENTFYSTAG;


	//路径规划结构体
	typedef struct
	{
		int32  MovMaxSpd;     //最大速度	
		int32  MovAccDecSpd;    //运行加减速大小
		int32 MovAccDecTim;  //加减速时间
		int16 MovTurnNum4OneTim;    //运行圈数
		int16 MovWaitTim;    //等待时间
		Uint16 MovMod;        //运行模式
		int16 MovCyclTims;        //运行次数
		int16 MovTim; //运行时间 
		Uint16 MovStag;
		int32 RemainP;
		Uint16 RunMode;
	}STR_TUNE_PATHPLAN;

	//状态字
	typedef struct
	{
		Uint16 RunStat:4;  
						/*
						Bit0-bit3。辨识前状态与辨识中状态
						0: 准备态
						1：rdy
						2：son
						3：err
						4：设置准备OK
						5：son辨识中
						6：son辨识完成
						7：err辨识中
						*/
		Uint16 rsvd:4;
		Uint16 TunStat:8;  
						/*
						Bit8-bit15
						0:准备
						1：惯量辨识完
						2：摩擦辨识完
						3：发生振动
						4：刚性调整完
						5：模型调整完
						6：调整完成
						*/
	} STR_IDENTFYSTATWRD;
typedef union{
     Uint16                  all;
     STR_IDENTFYSTATWRD             bit;
}UNI_IDENTFYSTATWRD;
	
	//辨识状态
	typedef struct
	{	
	    Uint16 IdentfyErr;                  //辨识故障内容
		  UNI_IDENTFYSTATWRD  UNI_STR_IdentfyStatWrd;
	}STR_TUNE_IDENTFYSTAT;

	//控制字
	typedef struct
	{
		Uint16 DefaultGain:1;     //使用确实增益 1:使用，0:不使用
		Uint16 OnlyInertia:1;     //仅惯量辨识 1:是，0: 无效
		Uint16 CmdSelect:1;       //指令来源 1:后台规划,0:客户规划
		Uint16 Rsvd:1;            //保留
		Uint16 TunLevel:2;        //响应等级 0:低，1:中，2:高
		Uint16 TunMod:2;          //调整模式0:普通模式 1:定位模式
	} STR_IDENTFYCONTWRD;
typedef union{
     Uint16                  all;
     STR_IDENTFYCONTWRD             bit;
}UNI_IDENTFYCTRLWRD;

	//辨识控制
	typedef struct
	{
		Uint16 JogSpd;              //JOG速度
		Uint16 JogDir;             //JOG方向1正向，2方向 0停止
		Uint16 PosLimtEn;          //设定极限位置1设定生效，0无效
		//Uint16 ServoOnOrOff;       //内部伺服控制1:on 2:off 0:无效
		//Uint16 IdentfyOnOrOff;     //辨识控制1:启动,2:终止,0无效
		UNI_IDENTFYCTRLWRD   UNI_STR_IdentfyCtrlWrd;
		//Uint16 IdentfyStatRead;	   //上位机读取辨识状态，1:读取一次
		//Uint16 IdentfyStage;       //辨识过程
		                           /*
		                           			0: 准备
		                           			1: 惯量辨识+ 摩擦辨识
		                           			2: 最高刚性测试
		                           			3: 模型跟踪测试
		                           			4: 辨识结束
		                           			*/
		Uint16 IdentRunEn;   //辨识使能有效标志

		Uint8 RunMod;

	}STR_TUNE_IDENTFYCONT;

	//辨识监控
	typedef struct
	{
		STR_TUNE_IDENTFYSTAT   STR_Tun_Stat;
		STR_TUNE_IDENTFYCONT   STR_Tun_Control;
		
		Uint16 OscillateHappen;
		Uint16 OscillatReliev;
		Uint32 OscillateLeveSum;
		Uint16 OscillateLevelAv;
		

		Uint8 PosCmdStop; //位置命令发送结束
		int32  SpdCmdRef;  //位置指令
		int32  SpdFd;
		int32  ToqRef;

		int32 PosRef;
		int32 PosAmperr;
		int16 PosFreq;
		int16 SpdFreq; 
		Uint8 OverHappan; //过冲发生
		int32 OverPuls; //过冲量
		Uint32 PulsOneTurn; //一圈脉冲当量指令单位
		int32 OnePulstoEncodPuls;
		int32 MT_EncoderNum; //编码器线数
		Uint8 GainTunStep;
		int16 WaitTim;
		Uint16 ChangeGainFlag;
		Uint16 GainLevel;
		Uint16 Stage;
		Uint16 TunStatus;
		Uint8 IdentEn;   //0 未启动自调整   1启动自调整   
		
	}STR_TUNE_MONIT;

	//增益表
	typedef struct
	{
		Uint8 GainLevel;
	   	Uint8 UpdataGainloop;
		Uint16 MaxGainLevel;
		Uint16 MinGainLevel;
		
//

		Uint16 AutoTuningModeSel; 
	 	Uint16 Rigidity_Level1st;
		Uint16 InertiaIdyCountModeSel ;
		Uint16 InertiaRatio;	

		Uint16 Pos_Kp;
		Uint16 Spd_Kp;
		Uint16 Spd_Ti;
		Uint16 ToqFiltTime;
		
		Uint16 AdaptFiltMd;
		Uint16 NotchFiltFreqC;
		Uint16 NotchFiltBandWidthC;
		Uint16  NotchFiltAttenuatLvlC ;
		Uint16  NotchFiltFreqD ;
		Uint16 NotchFiltBandWidthD ;
		Uint16 NotchFiltAttenuatLvlD ;
		Uint16 ModuKp;
		Uint16 ModuKff;
		Uint16 ModEn;

	

	}STR_TUNE_GAINTABLE	;

/* Exported_Variables --------------------------------------------------------*/
/* 可供其它文件调用变量的声明 */
extern STR_TUNE_MONIT    STR_TunMonitor;


extern ENUM_RUNSTAT      ENUM_RunStatus; //状态枚举
extern ENUM_TUNSTAT      ENUM_TuneStatus; //调整过程枚举
extern ENUM_IDENTFYSTAG  ENUM_IdentfyStage; //调整阶段枚举

extern STR_TUNE_MONIT    STR_TunMonitor;
extern STR_TUNE_PATHPLAN STR_TunPath;
extern STR_TUNE_GAINTABLE STR_TunGainTable;



extern int32 refCmdLatch;//增益调整函数使用，放到文件内全局，不然第1次操作有问题
extern Uint8 normalCnt; //放到文件全局中，增益调整也需要
extern Uint8 overShootHappen; //此标志应该在停机或者重启时复位为0
extern Uint8 updataFlg;

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */ 

extern void EasyTunMonit(Uint16 ServRunStat); //外部调用的监控模块接口
extern void EasyTunSysCtrl(void); //一键式的流程控制接口
extern void EasyTunModeCtrl(Uint16 ServRunStat,Uint8* pRunmod); //一键式运行模式的切换处理
extern int32 EasyTunJogCmdGenerator(void); //速度位置JOG
extern void EasyTunGetInterfacePara(void);
extern void EasyTunGainParaUpdata(void);


#ifdef __cplusplus
}
#endif  /* extern "C" */
#endif


