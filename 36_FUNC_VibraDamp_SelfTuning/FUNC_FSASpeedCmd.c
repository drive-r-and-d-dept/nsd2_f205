
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "FUNC_GlobalVariable.h"
#include "FUNC_InterfaceProcess.h"
#include "FUNC_FunCode.h"
#include "FUNC_AuxFunCode.h"
#include "FUNC_ErrorCode.h"
#include "FUNC_COMMInterface.h"  // 频谱分析用
#include "FUNC_FSASpeedCmd.h"
#include "PUB_Table.h"
#include "MTR_SpdRegulator.h"   
#include "MTR_GlobalVariable.h"  
#include "MTR_InterfaceProcess.h"   

#include "PUB_Library_Function.h"
#include "FUNC_GlobalVariable.h"
#include "FUNC_FunCode.h"
#include "FUNC_ErrorCode.h" 
#include "FUNC_Filter.h"
#include "FUNC_Monitor_DCBusVolt.h"
#include "FUNC_GPIODriver.h"
#include "FUNC_AdcDriver_ST.h"
#include "FUNC_Monitor_PhaseLack.h"
#include "FUNC_InterfaceProcess.h"



#define Freq0_step1   0      //低频段起始频率
#define Freq1_step1   110    //低频段终止频率        要超过高低频连接频率97.65Hz
#define Freq0_step2   40     //高频段起始频率
#define Freq1_step2   5000   //高频段终止频率         要超过最高频率4000Hz
#define LowFreqCoef  16      //低频段采样周期倍数
#define STEP0   0            //高频段结束等待下一次扫频,或扫频未开始状态
#define STEP1   1            //处于低频段
#define STEP2   2            //低频段完成 等待上位机对完缓冲区
#define STEP3   3            //处于高频段
#define STEP4   4            //高频段完成等待上位机读完 
#define STEP5   5            //高频段上位机读完 
#define FSA_NRDY 1   		 //还未开始扫频
#define FSA_BUSY  2  		 //正在扫频
#define FSA_RDY   3   		 //上位机可以读数据
  

/* Private_TypesDefinitions --------------------------------------------------*/
/* 结构体变量定义 枚举变量定义 */

/* Private_Variables ---------------------------------------------------------*/
/* 文件内变量定义 */
static Uint16 SpdCmdCnt = 0;
//static Uint16 PRBCmdCnt = 0;
static Uint16 SpdCmdDelayCnt = 0;
static Uint16 bit123 = 0;

static Uint16 FSA_step=0;  // 1 :低频段   2 :高频段
static int32 FSAIqCmd = 0;
static Uint16 LowFreqCnt=0;  //低频段的计数器 最大值16K*4
static int16 TempCnt =0;       //SpdCmdCnt据此累加
static int32 Ref = 0;       
//static int32 FSASpeedCmd = 0;

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */
void GenerateFSASpdCmd(void);       // 生成频谱分析速度指令
void FSASendCtrl(void);             // 控制发送开环频率特性分析数据给上位机

/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部调用的函数的声明 */
Static_Inline int16 LowFreqChirp(Uint16 CNT);
Static_Inline int16 HighFreqChirp(Uint16 CNT);


/*******************************************************************************
  函数名: int32 GenerateFSASpdCmd()
  输  入:
  输  出:
  子函数:
  描  述: 生成频谱分析指令
********************************************************************************/
void GenerateFSASpdCmd()
{
    int16 ChirpPhase;	
    static int32 tmr=0;
    // FS_AnalyzeStart表示启动频谱分析速度指令模式, 由上位机控制,
    // 启动频谱分析必须在伺服ON之后, 伺服运行状态由上位机检测
   
    if (AuxFunCodeUnion.code.FS_EnableSend == 0)
    {
        //AuxFunCodeUnion.code.H2F_FSAState = 0;
        FSA_step=STEP0;    
    }
    if (AuxFunCodeUnion.code.FS_AnalyzeStart==1)
    {     
		switch(FSA_step)
		{
            case STEP0://延迟一小段时间再启动			
                if(SpdCmdDelayCnt < (3 * STR_FUNC_Gvar.System.SpdFreq))
                {
                    SpdCmdDelayCnt++;
                    Ref = 0;
                    AuxFunCodeUnion.code.FS_TorqSat = 0;			
                    bit123 = FSA_NRDY;
                    FSA_step=STEP1;	
                    AuxFunCodeUnion.code.H2F_FSAState = 0;
                }
                break;
            case STEP1: //处于低频段
                if( SpdCmdCnt <4096)       //发送中
                {
                    ChirpPhase=LowFreqChirp(LowFreqCnt);	//低频段指令生成的计数器和采样计数器不同
                    if(AuxFunCodeUnion.code.FS_SpdOrTrq==0) //速度激励
                    {
                        Ref = (int32)SinTable[ChirpPhase] *10000>>15;
                        Ref=Ref*AuxFunCodeUnion.code.FS_SpdCmdAmpltd;  
                        FSAIqCmd=0;
                    }
                    else if(AuxFunCodeUnion.code.FS_SpdOrTrq==1) //转矩激励
                    {
                        Ref = 0;
                        FSAIqCmd=(int32)SinTable[ChirpPhase] *4096>>15;
                        FSAIqCmd=FSAIqCmd*AuxFunCodeUnion.code.FS_IqTurbAmpltd/100;
                    }
                    TempCnt++;
                    if(TempCnt>=LowFreqCoef)
                	{
                        TempCnt=0;                  
                        SpdCmdCnt++;
                        if(AuxFunCodeUnion.code.FS_SpdOrTrq==0)       //速度激励
                        {
                            UNI_OsciBuffer.all_16Bits[SpdCmdCnt-1] = UNI_FUNC_MTRToFUNC_FastList_16kHz.List.SpdFdb / 10000;					
                            if(AuxFunCodeUnion.code.FS_Mode == 1)  //闭环
                                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = Ref / 10000;
                            else if(AuxFunCodeUnion.code.FS_Mode == 2)   //开环
                                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = Spd_PdffCtrl.SpdErr / 10000;
                        }
                        else if(AuxFunCodeUnion.code.FS_SpdOrTrq==1)  //转矩激励
                        {
                            UNI_OsciBuffer.all_16Bits[SpdCmdCnt-1] = -STR_MTR_Gvar.SpeedRegulator.FSA_SpdReguOut;	        
                            if(AuxFunCodeUnion.code.FS_Mode == 1)  //闭环
                                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = FSAIqCmd;
                            else if(AuxFunCodeUnion.code.FS_Mode == 2)   //开环
                                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = STR_MTR_Gvar.SpeedRegulator.FSA_SpdReguOutPlusChirp;
                        }
                	}
                    LowFreqCnt++;
                    bit123 = FSA_BUSY;
                }
                else
                    FSA_step=STEP2;
                break;
            case STEP2:  //低频段结束 等待上位机读完
                SpdCmdCnt = 0;
                Ref = 0;
                //bit123 = FSA_RDY;
                tmr++;
                if(tmr<1600)
                bit123 = FSA_RDY;
                else
                { 
                	bit123 = FSA_BUSY;
                    if(AuxFunCodeUnion.code.H2F_FSAState==2)
                	//if(tmr>160000)
                		FSA_step=STEP3;
                    AuxFunCodeUnion.code.DP_Rsvd63 = AuxFunCodeUnion.code.H2F_FSAState;
                }            
                break;
            case STEP3: //处于高频段
                if(SpdCmdCnt<4096) //发送中
                {
                    ChirpPhase=HighFreqChirp(SpdCmdCnt);  //高频段指令生成和电流采样的计数器相同
                    if(AuxFunCodeUnion.code.FS_SpdOrTrq==0) //速度激励
                    {
                        Ref = (int32)SinTable[ChirpPhase] *10000>>15;
                        Ref=Ref*AuxFunCodeUnion.code.FS_SpdCmdAmpltd;  
                        FSAIqCmd=0;
                    }
                    else if(AuxFunCodeUnion.code.FS_SpdOrTrq==1) //转矩激励
                    {
                        Ref = 0;
                        FSAIqCmd=(int32)SinTable[ChirpPhase] *4096>>15;
                        FSAIqCmd=FSAIqCmd*AuxFunCodeUnion.code.FS_IqTurbAmpltd/100;
                    }
                    SpdCmdCnt++;
                    bit123 = FSA_BUSY;
                 }			
                else
                {
                	FSA_step=STEP4;               
                }
			  break;
            case STEP4: //高频段结束等待上位机读完
                SpdCmdCnt = 0;
                Ref = 0;
                FSAIqCmd=0;
                bit123 = FSA_RDY;
                if(AuxFunCodeUnion.code.H2F_FSAState==2)
                {   
                    FSA_step=STEP5;	
                }
                break;
	   
			default:
				break;
        }
    } 
    else
    {
        Ref = 0;
        SpdCmdCnt = 0;
        SpdCmdDelayCnt = 0;
        LowFreqCnt=0;
        TempCnt=0;
        FSA_step=STEP0;
        tmr=0;   
    }

    //零速停机 急停转矩停机  抱闸处理
  
    if((STR_FUNC_Gvar.MonitorFlag.bit.ZeroSpdStop == 1)
       || (STR_FUNC_Gvar.MonitorFlag.bit.ToqStop == 1)
       || ((STR_FUNC_Gvar.MonitorFlag.bit.BrakeUnLock == 1)&&(STR_FUNC_Gvar.MonitorFlag.bit.RunMod == SPDMOD))
       || (STR_FUNC_Gvar.MonitorFlag.bit.PwmStatus == DISPWM))
    {
        STR_FUNC_Gvar.SpdCtrl.SpdRef = 0;
    }
    else if(STR_FUNC_Gvar.MonitorFlag.bit.OTClamp == 1) //超程处理
    {
        STR_FUNC_Gvar.SpdCtrl.SpdRef = STR_FUNC_Gvar.PosCtrl.PosReguOut;
    }
    else
    {
        // 给出速度指令
        STR_FUNC_Gvar.SpdCtrl.SpdRef = Ref;
        STR_FUNC_Gvar.FsaCmd.FSAIqCmd = FSAIqCmd;
    }
}


//add chirp signal to trq

//计算低频chirp
int16 LowFreqChirp(Uint16 CNT )
{
    int64 temp;
    int16 phase;
    //rad=pi*(Freq1_step1-Freq0_step1)*(CNT/16000)*(CNT/16000)*1000/4096
    //256/Period=rad*256/2/pi   //正弦表长度256
    temp=(Freq1_step1-Freq0_step1)*CNT*CNT;
    temp=temp/8192000;
    phase=(int16)(temp-((temp>>8)<<8));
    return(phase);
}

//计算高频chirp
int16 HighFreqChirp(Uint16 CNT )
{
    int64 temp;
    int16 phase;
    //rad=pi*(Freq1_step1-Freq0_step1)*(CNT/16000)*(CNT/16000)*1000/256
    //256/Period=rad*256/2/pi   //正弦表长度256
    temp=(Freq1_step2-Freq0_step2)*CNT*CNT;
    temp=temp/512000;
    phase=(int16)(temp-((temp>>8)<<8));
    return(phase);
}


void FSASendCtrl()
{
    // 频谱分析采样正在进行时如果伺服不处于运行状态则告知数据无效
    if(STR_FUNC_Gvar.MonitorFlag.bit.ServoRunStatus != 2)
    {
        // 将FS_EnableSend的第三位置1, 低两位保持不变;
        AuxFunCodeUnion.code.FS_EnableSend |= 0x4;
    }
    
    if ((SpdCmdCnt <= 4096) && (SpdCmdCnt > 0)&&(FSA_step==STEP3)&&(bit123==FSA_BUSY))
    {
        if(AuxFunCodeUnion.code.FS_SpdOrTrq==0)       //速度激励
        {
            UNI_OsciBuffer.all_16Bits[SpdCmdCnt-1] = UNI_FUNC_MTRToFUNC_FastList_16kHz.List.SpdFdb / 10000;					
            if(AuxFunCodeUnion.code.FS_Mode == 1)  
                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = Ref / 10000;
            else if(AuxFunCodeUnion.code.FS_Mode == 2)   //开环
                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = Spd_PdffCtrl.SpdErr / 10000;
        }
        else if(AuxFunCodeUnion.code.FS_SpdOrTrq==1)  //转矩激励
        {
            UNI_OsciBuffer.all_16Bits[SpdCmdCnt-1] = -STR_MTR_Gvar.SpeedRegulator.FSA_SpdReguOut;					
            if(AuxFunCodeUnion.code.FS_Mode == 1)  
                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = FSAIqCmd;
            else if(AuxFunCodeUnion.code.FS_Mode == 2)   //开环
                UNI_OsciBuffer.all_16Bits[SpdCmdCnt+4095] = STR_MTR_Gvar.SpeedRegulator.FSA_SpdReguOutPlusChirp;          
        }
    }
	
    if (UNI_FUNC_MTRToFUNC_FastList_16kHz.List.StatusFlag.bit.DOvarReg_Clt == 1)
    {
        AuxFunCodeUnion.code.FS_TorqSat |= 0x1;
    }
    if (UNI_FUNC_MTRToFUNC_FastList_16kHz.List.StatusFlag.bit.VoltVectorLmt == 1)
    {
        AuxFunCodeUnion.code.FS_TorqSat |= 0x2;
    }
    if (UNI_FUNC_MTRToFUNC_FastList_16kHz.List.StatusFlag.bit.CurSmpMode == 1)
    {
        AuxFunCodeUnion.code.FS_TorqSat |= 0x4;
    }
    
	// 第三位保持不变, 更新低两位的最新值
    AuxFunCodeUnion.code.FS_EnableSend &= 0x4;
    AuxFunCodeUnion.code.FS_EnableSend |= (0x3 & bit123);

}

/********************************* END OF FILE *********************************/

