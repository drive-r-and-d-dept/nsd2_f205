
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "FUNC_GlobalVariable.h"
#include "FUNC_RigidityLevelTable.h"
#include "FUNC_InterfaceProcess.h"
#include "FUNC_FunCode.h"
#include "FUNC_ErrorCode.h"
#include "FUNC_EasyTune.h"
#include "FUNC_OperEeprom.h"
#include "MTR_GlobalVariable.h"

#define PlusOne 1      //+1级
#define MinusOne 2   //-1级
#define MinusTwo 3   //-2级
#define MinusThree 4   //-3级

extern ENUM_RUNSTAT      ENUM_RunStatus; //状态枚举
extern ENUM_TUNSTAT      ENUM_TuneStatus; //调整过程枚举
extern ENUM_IDENTFYSTAG  ENUM_IdentfyStage; //调整阶段枚举

extern STR_TUNE_MONIT    STR_TunMonitor;
extern STR_TUNE_PATHPLAN STR_TunPath;
extern STR_TUNE_GAINTABLE STR_TunGainTable;


extern int32 refCmdLatch;//增益调整函数使用，放到文件内全局，不然第1次操作有问题
extern Uint8 normalCnt; //放到文件全局中，增益调整也需要
extern Uint8 overShootHappen; //此标志应该在停机或者重启时复位为0
extern Uint8 updataFlg;


void ModCtrlParaStpSet(void);
void SaveNewServoPara(void);
void Return2InitServoPara(void);
void Return2DfltServoPara(void);
void UpdataMaxGainLevel(void);	


void EasyTunProcessCtrl(void);
void GainIdentfy(void);
void ModuGainIdentfy(void);
void FrictionIdentfy(void);





/***********************************************
void EasyTunSysCtrl(void)
描述: 
负责辨识过程控制
辨识启动标志IdentfyEnValidFlg和设置的相关模式进行辨识。
辨识过程由IdentfyStage控制
************************************************/
void EasyTunProcessCtrl(void)
{
	static Uint16 dlyTim=0; //等待时间，主要考虑模式切换的时间
//	static Uint8 upGainFlg=1;
//	Uint16 tmpOnlyJidnty;
	
	//监控处理
	//EasyTunMonit();
	
//STR_FUNC_Gvar.OscTarget.CH4_Test = STR_TunMonitor.GainTunStep;
//STR_FUNC_Gvar.OscTarget.CH3_Test = FunCodeUnion.code.AT_AdaptiveFilterMode;//STR_TunMonitor.OverHappan;
//STR_FUNC_Gvar.OscTarget.CH2_Test = STR_TunGainTable.GainLevel ;
	//如果启动无效退出
/*
	
	if(STR_TunMonitor.STR_Tun_Control.IdentRunEn == INVALID)
	{ 
		if(ENUM_TuneStatus == ACTIVE_INERTIA_IDENTFY )			
		{
			RecovyGainPara();
		}			
		
		ENUM_IdentfyStage = STEP_READY_TO_IDENTFY;
		//ENUM_TuneStatus = READY_TO_TUNE;    //这句话会导致无法结束整个调整流程
		
		dlyTim =0;
		upGainFlg=1;
		//ENUM_RunStatus = VALID;
		//如果前次惯量已经辨识完了，参照三菱可以不需要重新辨识
		
		return;
	}

*/
	//STR_FUNC_Gvar.OscTarget.CH3_Test = AuxFunCodeUnion.code.TUN_IdentfyEn; 

	//过程控制	
	switch(ENUM_IdentfyStage)
	{
		case STEP_READY_TO_IDENTFY:
			//此处可以进行其他复位操作
			//等待伺服使能,等待5ms
			if((ENUM_RunStatus == SERVO_PARA_OK)&&dlyTim++ > 10)
			{
				ENUM_IdentfyStage = STEP_INERTIA_IDENTFY;
				dlyTim=0;
			}

			//   Set2DefaulorInitialtGain(); //设置到默认增益
			//保存增益切换、0900 0901
			ENUM_TuneStatus |= ACTIVE_INERTIA_IDENTFY;
			
			
			break;
		case STEP_INERTIA_IDENTFY : //启动惯量辨识，此处只需要设置启动标志即可
			FunCodeUnion.code.InertiaIdyCountModeSel = 1;    //开启在线惯量辨识
			//STR_MTR_Gvar.GlobalFlag.bit.InertiaIdyEn == 1 ;

			ENUM_RunStatus = SERVO_IDENTFYING;

			FrictionIdentfy();

			//InertiaIdentfyEndJudg();

			/*
			if((STR_TunMonitor.OscillateHappen==1)&&(upGainFlg==1))
			{
				if((FunCodeUnion.code.AutoTuningModeSel == 1)
					&&(FunCodeUnion.code.Rigidity_Level1st > 5))
				{
					
					FunCodeUnion.code.Rigidity_Level1st -= 1;
				}
				else if((FunCodeUnion.code.AutoTuningModeSel == 0)
					&&(FunCodeUnion.code.GN_Pos_Kp > 30))
				{
					FunCodeUnion.code.GN_Pos_Kp -= 10;
				}	

				upGainFlg = 0;
			}
			else if(STR_TunMonitor.OscillateHappen==1 )
			{
				if(dlyTim++ > 100)
					upGainFlg = 1;
			}*/
			
			if((ENUM_TuneStatus&FINISH_INERTIA_IDENTFY) == FINISH_INERTIA_IDENTFY)
			{

				if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.DefaultGain == 0)  //如果不使用默认参数，在这里才恢复默认
				{
					Return2DfltServoPara();
				}	

				UpdataMaxGainLevel();	

			   	if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.OnlyInertia == 0)
				{
					ENUM_IdentfyStage = STEP_GAINLEVEL_TUNE;
				//	FunCodeUnion.code.AutoTuningModeSel = 3;

					STR_TunMonitor.GainTunStep =1;
					
				}
				else 
				{
					ENUM_IdentfyStage = STEP_IDENTFY_FINISH;
					
				}

			
			}


				
			break;
		case STEP_GAINLEVEL_TUNE:
			
			GainIdentfy();

			if((ENUM_TuneStatus&FINISH_GAIN_TUNE) == FINISH_GAIN_TUNE)
			{
				if(1==STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.TunMod)
				
				{
					ENUM_IdentfyStage = STEP_MODU_TUNE;
 	
		          // FunCodeUnion.code.AT_ModuEn = 1;
				   	FunCodeUnion.code.AutoTuningModeSel = 4;
					FunCodeUnion.code.AT_ModuEn= 1;
					FunCodeUnion.code.AT_ModuKp = FunCodeUnion.code.GN_Pos_Kp;
					STR_TunGainTable.ModuKp = FunCodeUnion.code.AT_ModuKp;
					FunCodeUnion.code.AT_ModuKffValu = 900;

					ModCtrlParaStpSet();


				}
				else
				{
					ENUM_IdentfyStage = STEP_IDENTFY_FINISH;
					
				}

				

			}
			
			
			break;
		case STEP_MODU_TUNE:
			ModuGainIdentfy();

			if((ENUM_TuneStatus&FINISH_MODU_TUNE) == FINISH_MODU_TUNE)
			{
				
				ENUM_IdentfyStage = STEP_IDENTFY_FINISH;
				

			}
			
			break;
			
		case STEP_IDENTFY_FINISH:
			
			ENUM_RunStatus = SERVO_IDENTFY_FINISH;
			ENUM_TuneStatus |= FINISH_ALL_TUNE;   //正常结束置位

			//完成后需要设置的功能码
			FunCodeUnion.code.AT_AdaptiveFilterMode = 0;		
			FunCodeUnion.code.AutoTuningModeSel = 1;	
			FunCodeUnion.code.InertiaIdyCountModeSel =0;  //关闭在线惯量辨识
			

			if(AuxFunCodeUnion.code.TUN_IdentfyEn == 0x03)
			{
				SaveNewServoPara();
				ENUM_IdentfyStage = STEP_READY_TO_IDENTFY;
			}
			//如果选否，就不保存参数，恢复原参数

			if(AuxFunCodeUnion.code.TUN_IdentfyEn == 0x00)
			{
				Return2InitServoPara();
				ENUM_IdentfyStage = STEP_READY_TO_IDENTFY;
			}
			break;
	}

}




/***********************************************
void GainIdentfy(Uint8 runMod)
描述: 
增益刚性等级辨识
根据是否振动判断速度带宽，是否过冲位置带宽
************************************************/
void GainIdentfy(void)
{
	Uint8 gainLevel;
	Uint8 updataGainLoop;
	Uint8 IncOrReDucGainFlg; // 1:提高增益，2: 减少增益0,保持不变 3速度最高刚性 4位置最高刚性
	Uint8 maxgainLevelTemp;

	
	gainLevel = STR_TunGainTable.GainLevel;
	updataGainLoop = STR_TunGainTable.UpdataGainloop;
	FunCodeUnion.code.AT_Rsvd40 = 2;
	IncOrReDucGainFlg =0;
	
	//更新位置判断，增益,指令发送完判断更新
	if(STR_TunMonitor.STR_Tun_Control.RunMod == POSMOD)
	{

			if((STR_TunMonitor.PosRef != 0) && (refCmdLatch ==0))
			{
				updataFlg = 1;
				if(STR_TunMonitor.GainTunStep >= 4)
				{
					normalCnt++;
				}
				else
				{
					normalCnt =0;
				}
			}
			refCmdLatch = STR_TunMonitor.PosRef;
	}
	else //spdmod
	{
			if((STR_TunMonitor.SpdCmdRef != 0) && (refCmdLatch ==0))
			{
				updataFlg = 1;

				if(STR_TunMonitor.GainTunStep == 4)
				{
					normalCnt++;
				}
			}
			refCmdLatch = STR_TunMonitor.SpdCmdRef;

	}


	//=============
	
	//开启自适应陷泊器同时把增益调整设置为自调整
	switch(STR_TunMonitor.GainTunStep)
	{
	case 1:
	
		FunCodeUnion.code.AT_AdaptiveFilterMode = 1; //开启一个自适应陷波器

		//FunCodeUnion.code.AutoTuningModeSel = 3; //后台下载参数时设置
		
		STR_TunMonitor.GainTunStep = 2;
		STR_TunMonitor.WaitTim =0;

		break;
	
	case 2: //无振动	
		updataGainLoop = 2;
		//振动判断
		if(STR_TunMonitor.OscillateHappen == 1)
		{
		
			if((FunCodeUnion.code.AT_AdaptiveFilterMode == 0) //等待自适应把频率抑制
			||(STR_TunMonitor.WaitTim++ >= 1000))//如果200ms中没有成功抑制，则直接降增益等级
			{
				IncOrReDucGainFlg = MinusTwo; //reduce gain level
				STR_TunMonitor.GainTunStep = 3; //happen one times oscillation

				STR_TunMonitor.WaitTim =0;

				if(STR_TunGainTable.MaxGainLevel > (gainLevel + 4))
				STR_TunGainTable.MaxGainLevel = gainLevel + 4; //最大四级
			}

			        //最大增益限制
			if((FunCodeUnion.code.AT_NotchFiltFreqC<3000)||(FunCodeUnion.code.AT_NotchFiltFreqD<3000))  
				maxgainLevelTemp=31;
			if((FunCodeUnion.code.AT_NotchFiltFreqC<2000)||(FunCodeUnion.code.AT_NotchFiltFreqD<2000))  
				maxgainLevelTemp=26;
	 	       if((FunCodeUnion.code.AT_NotchFiltFreqC<1200)||(FunCodeUnion.code.AT_NotchFiltFreqD<1200))  
			   	maxgainLevelTemp=24;
	    	       if((FunCodeUnion.code.AT_NotchFiltFreqC<800)||(FunCodeUnion.code.AT_NotchFiltFreqD<800))  
				   	maxgainLevelTemp=21;
		 	if((FunCodeUnion.code.AT_NotchFiltFreqC<400)||(FunCodeUnion.code.AT_NotchFiltFreqD<400))  
				maxgainLevelTemp=19;
		       if((FunCodeUnion.code.AT_NotchFiltFreqC<200)||(FunCodeUnion.code.AT_NotchFiltFreqD<200))  
			   	maxgainLevelTemp=15;
			if((FunCodeUnion.code.AT_NotchFiltFreqC<100)||(FunCodeUnion.code.AT_NotchFiltFreqD<100))  
				maxgainLevelTemp=12;

			
			if(STR_TunGainTable.MaxGainLevel>maxgainLevelTemp)	
				STR_TunGainTable.MaxGainLevel=maxgainLevelTemp;



			//默认刚性振动，则位置环也需要跟着降低
			if(gainLevel <= 12)
			{
				updataGainLoop = 3;
			 	IncOrReDucGainFlg = MinusTwo;
				STR_TunMonitor.GainTunStep = 7;
				
			}
		}
		else
		{
			IncOrReDucGainFlg = PlusOne; //increase gain level
			STR_TunMonitor.WaitTim = 0;

			if(gainLevel >= STR_TunGainTable.MaxGainLevel)
			{
				//速度最高刚性达到
				IncOrReDucGainFlg = MinusTwo; //spd gain max
				STR_TunMonitor.GainTunStep = 4; //位置最高刚性判断
		
			}
		}

		
		break;
	
	case 3: //已经发生过振动
	    updataGainLoop = 2;
		//振动判断
		if(STR_TunMonitor.OscillateHappen == 1)
		{
			//如果已经陷波了,则开启两个陷波器
			if(FunCodeUnion.code.AT_AdaptiveFilterMode == 0)
			{

				if((FunCodeUnion.code.AT_NotchFiltFreqC != 4000) // 两个陷波不关用
				&&(FunCodeUnion.code.AT_NotchFiltFreqD != 4000))
				{
					//这一步可以再做文章，把3?4移到1?2陷波器，这样可自适应4个
					//速度最高刚性达到
					IncOrReDucGainFlg = MinusThree; //spd gain max

					STR_TunMonitor.GainTunStep = 4; //位置最高刚性判断
				if((FunCodeUnion.code.AT_NotchFiltFreqC<3000)||(FunCodeUnion.code.AT_NotchFiltFreqD<3000))  
					maxgainLevelTemp=31;
				if((FunCodeUnion.code.AT_NotchFiltFreqC<2000)||(FunCodeUnion.code.AT_NotchFiltFreqD<2000))  
					maxgainLevelTemp=26;
		 	       if((FunCodeUnion.code.AT_NotchFiltFreqC<1200)||(FunCodeUnion.code.AT_NotchFiltFreqD<1200))  
				   	maxgainLevelTemp=24;
		    		if((FunCodeUnion.code.AT_NotchFiltFreqC<800)||(FunCodeUnion.code.AT_NotchFiltFreqD<800))  
					maxgainLevelTemp=21;
			 	if((FunCodeUnion.code.AT_NotchFiltFreqC<400)||(FunCodeUnion.code.AT_NotchFiltFreqD<400))  
					maxgainLevelTemp=19;
			       if((FunCodeUnion.code.AT_NotchFiltFreqC<200)||(FunCodeUnion.code.AT_NotchFiltFreqD<200))  
				   	maxgainLevelTemp=15;
				if((FunCodeUnion.code.AT_NotchFiltFreqC<100)||(FunCodeUnion.code.AT_NotchFiltFreqD<100))  
					maxgainLevelTemp=12;
				if(STR_TunGainTable.MaxGainLevel>maxgainLevelTemp)	
					STR_TunGainTable.MaxGainLevel=maxgainLevelTemp;
						
					
				}
				else //一个陷波器
				{
					FunCodeUnion.code.AT_AdaptiveFilterMode = 2; //开启两个

					if(STR_TunGainTable.MaxGainLevel > (gainLevel + 4))
					STR_TunGainTable.MaxGainLevel = gainLevel + 4; //最大四级
					
					IncOrReDucGainFlg = 3;
				}
			}

			//自适应无法抑制
			
			if(STR_TunMonitor.WaitTim++ >= 1000)
			{
				//速度最高刚性达到
					IncOrReDucGainFlg = MinusThree; //spd gain max
					STR_TunMonitor.GainTunStep = 4; //位置最高刚性判断
					STR_TunMonitor.WaitTim = 0;

			}


			
		}
		else 
		{
			IncOrReDucGainFlg = PlusOne; //increase gain level
			STR_TunMonitor.WaitTim = 0;

			if(gainLevel >= STR_TunGainTable.MaxGainLevel)
			{
					//速度最高刚性达到
					IncOrReDucGainFlg = MinusTwo; //spd gain max
					STR_TunMonitor.GainTunStep = 4; //位置最高刚性判断
					
			}

			//如果两个陷波器辨识出的频率很接近则设置最高刚性达到
			if((ABS(FunCodeUnion.code.AT_NotchFiltFreqC - FunCodeUnion.code.AT_NotchFiltFreqD)< (FunCodeUnion.code.AT_NotchFiltFreqC/5))
				&&(FunCodeUnion.code.AT_NotchFiltFreqD < 4000))
			{
				//速度最高刚性达到
				IncOrReDucGainFlg = MinusTwo; //spd gain max

				STR_TunMonitor.GainTunStep = 4; //位置最高刚性判断
			}
		}

		
		break;
		case 4://最高刚性判断，稳定运行判断
			//运行10次没问题则进行位置刚性判断
			if((normalCnt >= 10)&&(STR_TunMonitor.OscillateHappen == 0 ))
			{
				normalCnt =0;
				
				if(STR_TunMonitor.STR_Tun_Control.RunMod == SPDMOD)
				{
					STR_TunMonitor.GainTunStep = 6; //辨识结束
				}
				else //pos mod
				{
					STR_TunMonitor.GainTunStep = 5; //位置最高刚性判断
					updataGainLoop = 1;
				}
				FunCodeUnion.code.AT_AdaptiveFilterMode = 0;
			}
			else if(STR_TunMonitor.OscillateHappen == 1)
			{
				
				//STR_TunMonitor.GainTunStep = 3;
				IncOrReDucGainFlg = MinusOne;
			}
			
			break;

		case 5: //位置最高刚性判断，根据过冲量判断
			updataGainLoop = 1;
			if(STR_TunMonitor.OverHappan == 1 )
			{
				IncOrReDucGainFlg = MinusOne;
				normalCnt =0;
				

			}
			else if(normalCnt >=10)
			{
				IncOrReDucGainFlg = MinusOne;
				STR_TunMonitor.GainTunStep = 6;
				ENUM_TuneStatus |= FINISH_GAIN_TUNE;
				
				normalCnt =0;
			}
			
			break;
		case 6:			
			normalCnt =0;
			IncOrReDucGainFlg =0;
			updataGainLoop = 0;
			break;
		case 7:
			if(STR_TunMonitor.OscillateHappen == 1)
			{
				if((FunCodeUnion.code.AT_AdaptiveFilterMode == 0) //等待自适应把频率抑制
				||(STR_TunMonitor.WaitTim++ >= 300))//如果200ms中没有成功抑制，则直接降增益等级
				{
					IncOrReDucGainFlg = MinusTwo; //reduce gain level
					STR_TunMonitor.GainTunStep = 3; //happen one times oscillation

					STR_TunMonitor.WaitTim =0;
					updataGainLoop = 3;
				}
			}
			else
			{
				STR_TunMonitor.GainTunStep = 3; 
			}
			break;
		default:
			break;
			
	}

     
	//增益更新
	if((updataFlg==1)||(IncOrReDucGainFlg >= 4))
	{
		switch(IncOrReDucGainFlg )
		{
			case PlusOne:			
			   gainLevel += 1;
			   break;
			
			case MinusOne:			
			   gainLevel -= 1;
				break;
			
			case MinusTwo:			
				gainLevel -= 2;
				break;
			
			case MinusThree:
				gainLevel -=3;
				break;
			default :
				  break;
		}

		if(STR_TunMonitor.OscillatReliev == 1)
		STR_TunMonitor.OscillateHappen =0;
	//最低以及最高刚性判断
		if(gainLevel < STR_TunGainTable.MinGainLevel)
		{
			//故障
			
			STR_TunMonitor.STR_Tun_Stat.IdentfyErr = 0x0982;
			gainLevel = STR_TunGainTable.MinGainLevel;
			STR_TunMonitor.GainTunStep = 7;
			
			
		}
		updataFlg =0;
		STR_TunMonitor.OverHappan = 0;

		STR_TunGainTable.GainLevel = gainLevel;
		FunCodeUnion.code.Rigidity_Level1st = gainLevel;
		
	    STR_TunGainTable.UpdataGainloop = updataGainLoop;
		 
		   
	}

//	UpdataGain(gainLevel,updataGainLoop);

}

/***********************************************
void ModuGainIdentfy(Uint8 runMod)
描述: 
模型跟踪环增益辨识
根据过冲量进行调整 1ms或者2ms周期调度
************************************************/
void ModuGainIdentfy(void)
{

	if(STR_TunMonitor.PosRef == 0)
	{
		FunCodeUnion.code.AT_ModuEn = 1;
	}

	
	
	//指令发送完成判断,不能再指令01，01这种跳动情况下工作
	if((STR_TunMonitor.PosRef != 0) && (refCmdLatch ==0))
	{
		normalCnt++;
		updataFlg =1;
		
	}
	refCmdLatch = STR_TunMonitor.PosRef;

	
	if(((updataFlg == 1))&&(STR_TunMonitor.OverHappan == 1))//发生过冲,降低增益
	{
			//可以根据不同段设置不同调整量,后续再优化

		STR_TunGainTable.ModuKp -= 300;   //如果发生过冲，降一点
	     /*	
		if(STR_TunGainTable.ModuKp > 300) //最低5hz
            {
			
                STR_TunGainTable.ModuKp -= 150;
              
                //调整失败，正常不应该出现，出现基本都是此调整规则不合理
                STR_TunMonitor.STR_Tun_Stat.IdentfyErr = 0x0986;
             
                
            }
            */
          ENUM_TuneStatus |= FINISH_MODU_TUNE;       
			overShootHappen = 1;

			normalCnt=0;

			STR_TunMonitor.OverHappan = 0;
			updataFlg =0;
		
	}
	else if(updataFlg == 1)//没有过冲发生则增加增益
	{
		updataFlg = 0;
	/*	if(overShootHappen == 1)     //这里有问题，先注释掉
		{

			if(normalCnt == 1)
			
			STR_TunGainTable.ModuKp += 60;	

			if(normalCnt >= 12)
			{
				ENUM_TuneStatus |= FINISH_MODU_TUNE;
			}
	}
       */

		if(STR_TunGainTable.ModuKp < 8000) //最高800hz
		{
		  //可以根据不同段设置不同调整量,后续再优化
		
			STR_TunGainTable.ModuKp += 100;
		}
		else
		{
			ENUM_TuneStatus |= FINISH_MODU_TUNE;
		}
	}

	


}


/***********************************************
void FrictionIdentfy(void)
描述: 
速度指令大于2，而且速度反馈大于1.5时采集两次
重力补偿为停止态的电流大小
上电只辨识一次，不重复辨识
************************************************/
void FrictionIdentfy(void)
{
	static int32 PasFricCnt=0;
	static int32 NegFricCnt=0;
	static int32 ToqrefLatch=0;

	int32 temp=0;

	if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.OnlyInertia == 1)
		return;
	
	//有指令速度大于1rpm
	if((ABS(STR_TunMonitor.SpdCmdRef) <= 1000)
		&&(ABS(STR_TunMonitor.SpdFd)<=1000))
	{
		//FunCodeUnion.code.AT_ConstToqComp = STR_TunMonitor.ToqRef; //单位需要转换,需要滤波几次
	}


	if(ABS(STR_TunMonitor.SpdCmdRef) <= 20000)
	{
		ToqrefLatch = STR_TunMonitor.ToqRef;
		
		return;
	}

	if((STR_TunMonitor.SpdFd > 10000)//大于1.5rpm
		&&(PasFricCnt < 2))
	{
		temp = ((STR_TunMonitor.ToqRef - ToqrefLatch)*1000)>>12;
		
		PasFricCnt++;

		FunCodeUnion.code.AT_ToqPlusComp += (temp>>1);
	}
	else if((STR_TunMonitor.SpdFd < -10000)
		&&(NegFricCnt < 2))
	{
		temp = ((STR_TunMonitor.ToqRef - ToqrefLatch)*1000)>>12;
		
		NegFricCnt++;
		FunCodeUnion.code.AT_ToqMinusComp += (temp>>1);
	}	
	
}




