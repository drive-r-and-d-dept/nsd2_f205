
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "FUNC_GlobalVariable.h"
#include "FUNC_RigidityLevelTable.h"
#include "FUNC_InterfaceProcess.h"
#include "FUNC_FunCode.h"
#include "FUNC_ErrorCode.h"
#include "FUNC_EasyTune.h"
#include "FUNC_OperEeprom.h"
#include "MTR_GlobalVariable.h"


extern ENUM_RUNSTAT      ENUM_RunStatus; //状态枚举
extern ENUM_TUNSTAT      ENUM_TuneStatus; //调整过程枚举
extern ENUM_IDENTFYSTAG  ENUM_IdentfyStage; //调整阶段枚举

extern STR_TUNE_MONIT    STR_TunMonitor;
extern STR_TUNE_PATHPLAN STR_TunPath;
extern STR_TUNE_GAINTABLE STR_TunGainTable;

extern int32 tempTimCnt; //JOG指令发生器函数使用。这个变量在不用时应该恢复0 放大文件内全局
extern int16 cyclTimsCnt; //JOG指令发生器函数使用。这个变量在不用时应该恢复0
extern int32 refCmdLatch;//增益调整函数使用
extern Uint8 normalCnt; //增益调整也需要

/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */
void PathUpdata(Uint8 runMod);
void PosCoinTimandOverPulsCal(void);
int32 EasyTunJogCmdGenerator(void);



/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部调用的函数的声明 */
int32 PosOrSpdCmdGenerator(void);
int32 JogSpdGnerator(void);





/***********************************************
void PathUpdata(Uint8 runMod)
描述: 
速度JOG曲线设置，需要判断距离和最大速度加减速是否匹配，
其他最小判断由上位机完成
************************************************/
void PathUpdata(Uint8 runMod)
{
	int32 temp;
	//首先获取曲线设置
	static int32 tempTime;
	static int32 tempMaxspd;

	tempMaxspd = (int32)AuxFunCodeUnion.code.TUN_Spd ; //运行最大速度rpm
	tempTime =(int32)AuxFunCodeUnion.code.TUN_Spd * AuxFunCodeUnion.code.TUN_AccDecTim/1000; //加速到最大转速的时间

	if(AuxFunCodeUnion.code.TUN_JogEn == 0)
	{
		STR_TunPath.MovTurnNum4OneTim = (int16)AuxFunCodeUnion.code.TUN_TurnNumb4OneTim;//运行距离圈数0.1turn
		STR_TunPath.MovWaitTim = (int16)AuxFunCodeUnion.code.TUN_WaitTim;  //等待时间1ms

		temp= ((STR_TunPath.MovTurnNum4OneTim*10)-((int32)AuxFunCodeUnion.code.TUN_Spd*tempTime/600)); //0.01t
		if(temp <= 0)
		{
			STR_TunPath.MovTim= 0;
			//降低最大运行速度
			tempMaxspd = (int32)STR_TunPath.MovTurnNum4OneTim*6000/tempTime;
		}
		else
		{
			STR_TunPath.MovTim = (temp*600)/(int16)AuxFunCodeUnion.code.TUN_Spd; //0.01t --->ms
		}

		STR_TunPath.MovMod    = AuxFunCodeUnion.code.TUN_MovMode;  //运行模式

		if(STR_TunPath.MovMod < 2)
		{
			STR_TunPath.MovCyclTims = (int16)AuxFunCodeUnion.code.TUN_CyclTims<<1; //循环次数
		}
		else
		{
			STR_TunPath.MovCyclTims = (int16)AuxFunCodeUnion.code.TUN_CyclTims; //循环次数
		}
	}
	//根据速度环和位置环调度周期调整时间和加减速值等
	if(runMod == POSMOD)
	{
		temp = STR_TunMonitor.PosFreq/1000 ;
	}
	else //spd mode
	{
		temp = STR_TunMonitor.SpdFreq/1000 ;
	}
	
	//STR_TunPath.MovAccDecTim *= temp ;
	tempTime *= temp ;
	STR_TunPath.MovAccDecTim = tempTime;
	STR_TunPath.MovWaitTim *= temp ;
	STR_TunPath.MovTim *= temp ;
	
	if(runMod == SPDMOD)
	{
	//注意下面的速度值单位已经不是0.0001rpm/ms，而是0.0001rpm/指令周期
		STR_TunPath.MovAccDecSpd = (int32)AuxFunCodeUnion.code.TUN_Spd*10000/STR_TunPath.MovAccDecTim; //计算加速度
		STR_TunPath.MovMaxSpd =tempMaxspd* 10000; 
	}
	//如果位置模式还需要把0.0001rpm/调节周期转换成脉冲单位
	//脉冲指令单位
	else if(runMod == POSMOD)
	{
		STR_TunPath.MovMaxSpd = ((int64)AuxFunCodeUnion.code.TUN_Spd * STR_TunMonitor.PulsOneTurn *1024)/(60*STR_TunMonitor.PosFreq);
		
		STR_TunPath.MovAccDecSpd = STR_TunPath.MovMaxSpd / STR_TunPath.MovAccDecTim;
		
		
		STR_TunPath.MovMaxSpd = STR_TunPath.MovAccDecSpd * STR_TunPath.MovAccDecTim;
	
	}
	
	STR_TunPath.RunMode= runMod;
	
	//判断运行距离和加减速及速度匹配关系，这个应该在上位机进行判断
	//如果V*T>S，则不合理
	
	//如果有位置限位，则根据位置限位规划速度曲线


	//如果没有位置限位则直接按照设置曲线规划

}


/***********************************************
void PosCoinTimandOverPulsCal(void)
描述: 
定位时间计算, 指令为零后，
如果偏差连续3ms窗口期都小于设定值则认为定位完成
************************************************/
void PosCoinTimandOverPulsCal(void)
{
	static int32 posLatch=0;
	static Uint16 timLatch=0; //指令为零切换标志
	static Uint16 msCnt=0; //指令为零后的时间计算
	static int16 coinCnt=0; //定位完成滤波窗口
	static int8 posDir=0;  //停机态应该清零

	if((STR_TunMonitor.STR_Tun_Control.IdentRunEn == INVALID)
		||(STR_TunMonitor.STR_Tun_Control.RunMod == SPDMOD))
	{
		posLatch = 0;
		timLatch = 0;
		msCnt =0;
		coinCnt=0;
		posDir =0;
		return;
	}
	

	//指令发送完成判断
	if((STR_TunMonitor.PosRef == 0)
		&&(posLatch != STR_TunMonitor.PosRef))
	{
		//timLatch = 1;
		msCnt=0;
		STR_TunMonitor.PosCmdStop = 1;
	}
	else if(STR_TunMonitor.PosRef != 0)
	{
		timLatch = 0;
		msCnt=0;
		coinCnt =0;

		STR_TunMonitor.PosCmdStop = 0;

		STR_TunMonitor.OverPuls = 0;
	}

	//指令方向判断
	if(STR_TunMonitor.PosRef > 0)
	{
		posDir = 1;
	}
	else if(STR_TunMonitor.PosRef < 0)
	{
		posDir = -1;
	}

	if((STR_TunMonitor.PosCmdStop == 1))
	{
		msCnt++;

		//定位时间计算	
		if(ABS(STR_TunMonitor.PosAmperr) < (int16)FunCodeUnion.code.PL_PosReachValue)
	 	{
			coinCnt++;

			if(coinCnt == 1)
			timLatch = msCnt;
		}
		else 
		{
			coinCnt =0;
		}

		if(coinCnt == 50)
		{
			AuxFunCodeUnion.code.TUN_CoinTim = timLatch;//msCnt;
			STR_TunMonitor.PosCmdStop = 0;
			//timLatch = 0;
		}
		//如果10S种没有定位完成输出则报错
		if(msCnt>=50000)
	   	{
	   		STR_TunMonitor.STR_Tun_Stat.IdentfyErr = COINERR;
	   	}

		//过冲量计算
		if(((posDir == 1) && (STR_TunMonitor.PosAmperr < 0)) //正向
		&&(STR_TunMonitor.OverPuls > STR_TunMonitor.PosAmperr))
		{
			STR_TunMonitor.OverPuls = STR_TunMonitor.PosAmperr;
		}
		else if(((posDir == -1) && (STR_TunMonitor.PosAmperr > 0)) //负向
		&&(STR_TunMonitor.OverPuls < STR_TunMonitor.PosAmperr))
		{
			STR_TunMonitor.OverPuls = STR_TunMonitor.PosAmperr;
		}

		//过冲量显示,最后转换为指令单位
		AuxFunCodeUnion.code.TUN_OverPuls= ABS(STR_TunMonitor.OverPuls) * STR_TunMonitor.PulsOneTurn
		                                     /STR_TunMonitor.MT_EncoderNum;      //要加个绝对值，不然显示有问题

		//过冲量大于阀值则置位过冲标志
		if(ABS(STR_TunMonitor.OverPuls) > FunCodeUnion.code.PL_PosReachValue) //编码器单位，以后要统一
		{
			STR_TunMonitor.OverHappan = 1;
		}
	
	}

	posLatch = STR_TunMonitor.PosRef;
}


/***********************************************
void EasyTunJogCmdGenerator(void)
描述: 
速度JOG计算，把JOG T型用一个T型代替，发生器输出都是正的，
循环或者其他反向根据模式定
************************************************/
int32 PosOrSpdCmdGenerator(void)
{
	int32 spdTemp;
	int32 temp;

	//T型指令发生器
	switch(STR_TunPath.MovStag)
	{
		case 0: //加速段
			tempTimCnt++;
			spdTemp = STR_TunPath.MovAccDecSpd * tempTimCnt;
			if(tempTimCnt >= STR_TunPath.MovAccDecTim)
			{
				STR_TunPath.MovStag = 1;

				if(STR_TunPath.MovTim == 0)
				STR_TunPath.MovStag = 2;

				tempTimCnt =0;				
			}
			break;
		
		case 1://匀速段

			spdTemp = STR_TunPath.MovMaxSpd;

			tempTimCnt++;
			
			if(tempTimCnt >= STR_TunPath.MovTim)
			{
				tempTimCnt =0;
				STR_TunPath.MovStag = 2;
			}			
			break;
		
		case 2://减速段
			tempTimCnt++;
			
			spdTemp = STR_TunPath.MovMaxSpd - (STR_TunPath.MovAccDecSpd * tempTimCnt);

			if(tempTimCnt >= STR_TunPath.MovAccDecTim)
			{
				STR_TunPath.MovStag = 3;
				tempTimCnt =0;				
			}
			break;
		
		case 3: //等待段
			//置位单段运行结束
			tempTimCnt++;
			if(tempTimCnt >= STR_TunPath.MovWaitTim)
			{
				tempTimCnt =0;
				cyclTimsCnt++;		

				if((cyclTimsCnt < STR_TunPath.MovCyclTims)
					||(STR_TunPath.MovCyclTims == 0))
				{
					STR_TunPath.MovStag =0;
				}
				else
				{
					STR_TunPath.MovStag =4;
				}
			}
			spdTemp = 0;
			break;
			
		default: //
		   tempTimCnt = 0;
		   spdTemp = 0;
		   //结束复位处理
			break;
	}


	if(STR_TunPath.RunMode == POSMOD)
	{
		temp = (spdTemp + STR_TunPath.RemainP)>>10;
		STR_TunPath.RemainP = (spdTemp + STR_TunPath.RemainP) - (temp<<10);
		spdTemp = temp;
	}

		//JOG 方向模式操作
	switch(STR_TunPath.MovMod)
	{
		case 0: //正向
		temp =	cyclTimsCnt%2;
		if(temp == 1)
		{
			spdTemp = 0- spdTemp;
		}
		break;
		
		case 1://反向
		temp =	cyclTimsCnt%2;
		if(temp == 0)
		{
			spdTemp = 0- spdTemp;
		}
		break;
		
		case 2: //正单向
		break;
		
		case 3://反单向
		spdTemp = 0- spdTemp;
		break;
		
		default:
			break;
	}

	return spdTemp;
}

int32 JogSpdGnerator(void)
{
	static int32 spdLatch=0;
	static Uint8 jogDirLatch=0;
	static Uint8 negDir=0;
	
	int32 spdTemp;
	
	//JOG 方向模式操作
	if((AuxFunCodeUnion.code.TUN_JogDir == 1) && (jogDirLatch == 0))
	{
		negDir = 0;
		STR_TunPath.MovStag = 1;
		tempTimCnt = 0;
	}
	else if((AuxFunCodeUnion.code.TUN_JogDir == 2) && (jogDirLatch == 0))
	{
		negDir = 1;
		STR_TunPath.MovStag = 1;
		tempTimCnt = 0;
	}

	jogDirLatch = AuxFunCodeUnion.code.TUN_JogDir;
	
	//T型指令发生器
	switch(STR_TunPath.MovStag)
	{
		case 1: //加速段
			tempTimCnt++;
			spdTemp = STR_TunPath.MovAccDecSpd * tempTimCnt;
			if(spdTemp >= STR_TunPath.MovMaxSpd)
			{
				STR_TunPath.MovStag = 2;
				tempTimCnt =0;				
			}

			if(AuxFunCodeUnion.code.TUN_JogDir == 0)
			{
				STR_TunPath.MovStag = 3;
				tempTimCnt =0;
			}
			break;
		
		case 2://匀速段

			spdTemp = spdLatch;//STR_TunPath.MovMaxSpd;

			tempTimCnt =0;
			if(AuxFunCodeUnion.code.TUN_JogDir == 0)
			{
				STR_TunPath.MovStag = 3;
			}
			break;
		
		case 3://减速段
			
			tempTimCnt++;
			
			spdTemp = spdLatch - (STR_TunPath.MovAccDecSpd);

			if(spdTemp<= 0)
			{
				STR_TunPath.MovStag = 4;
				tempTimCnt =0;				
			}			
			break;
			
		case 4:
		default: //
		   tempTimCnt = 0;
		   spdTemp = 0;
		   //结束复位处理
			break;
	}

	spdLatch = spdTemp;

	if(negDir == 1)
	{
		spdTemp = 0- spdTemp;
	}

	return spdTemp;
}


int32 EasyTunJogCmdGenerator(void)
{
	int32 spdTemp;	

	if(AuxFunCodeUnion.code.TUN_JogEn == 0)
	{
		spdTemp = PosOrSpdCmdGenerator();
	}
	else if(AuxFunCodeUnion.code.TUN_JogEn == 1)
	{
		spdTemp = JogSpdGnerator();
	}
	
	return spdTemp;
}


