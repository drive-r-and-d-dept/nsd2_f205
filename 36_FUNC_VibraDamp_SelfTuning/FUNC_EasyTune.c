
/* Includes ------------------------------------------------------------------*/
/* 引用头文件 */
#include "FUNC_GlobalVariable.h"
#include "FUNC_RigidityLevelTable.h"
#include "FUNC_InterfaceProcess.h"
#include "FUNC_FunCode.h"
#include "FUNC_ErrorCode.h"
#include "FUNC_EasyTune.h"
#include "FUNC_OperEeprom.h"
#include "MTR_GlobalVariable.h"
 

/* Private_Constants ---------------------------------------------------------*/
/* 宏定义 常数类*/

#define INVALID 0
#define VALID 1
#define NULL_ERR 0
#define DLFTINERTIA 300


/*
H2F69  TUN_PageStatus      0：点x关闭界面  
                                      1：停在第一页 
                                      2：停在第二页   
                                      3：停在第三页  
                                      5：自调整结束后自动关闭界面
                                      
H2F40  TUN_JogEn          0:    无效	 
                                   1：使能	
                                   2：断使能
	离开第二页的时候上位机自动写0
	
H2F43  ServoOnorOff       0：无效        
                                   1：使能

H2F45  TUN_IdentyEn	  0:    无效，点x关闭界面时写0	  
                                  1：调整启动   
                                  2：调整终止    
                                  3：结束且写参数 

	调整结束后 选保存参数，先写3再写0，间隔200ms  ；选不保存参数，
	直接写0。无论是否写功能码，都由上位机自动关闭界面。 
	
H2F47  ReadOrWrite       0： 伺服检测到1以后写0
                                  1： 上位机读功能码之前都置1  

*/



/* Private_Macros ------------------------------------------------------------*/
/* 宏定义 函数类 */


/* Private_TypesDefinitions --------------------------------------------------*/ 
/* 结构体变量定义 枚举变量定义 */ 

/* Private_Variables ---------------------------------------------------------*/
ENUM_RUNSTAT      ENUM_RunStatus; //状态枚举
ENUM_TUNSTAT      ENUM_TuneStatus; //调整过程枚举
ENUM_IDENTFYSTAG  ENUM_IdentfyStage; //调整阶段枚举

STR_TUNE_MONIT    STR_TunMonitor;
STR_TUNE_PATHPLAN STR_TunPath;
STR_TUNE_GAINTABLE STR_TunGainTable;

int32 tempTimCnt = 0; //JOG指令发生器函数使用。这个变量在不用时应该恢复0 放大文件内全局
int16 cyclTimsCnt=0; //JOG指令发生器函数使用。这个变量在不用时应该恢复0
int32 refCmdLatch=0;//增益调整函数使用，放到文件内全局，不然第1次操作有问题
Uint8 normalCnt=0; //放到文件全局中，增益调整也需要
Uint8 overShootHappen=0; //此标志应该在停机或者重启时复位为0
Uint8 updataFlg=0;
Uint16 LastPage = 0 ; //上一次后台软件的界面在哪一页
extern const STR_RGDTYTABLE  GainTable[];
/* Exported_Functions --------------------------------------------------------*/
/* 可供其它文件调用的函数的声明 */
void EasyTunMonit(Uint16 ServRunStat); //外部调用的监控模块接口
void EasyTunProcessCtrl(void); //一键式的流程控制接口
void EasyTunModeCtrl(Uint16 ServRunStat,Uint8* pRunmod); //一键式运行模式的切换处理
int32 EasyTunJogCmdGenerator(void); //速度JOG
void EasyTunGetInterfacePara(void);//快速获取其他功能模块 的实时变量
void EasyTunGainParaUpdata(void);

/* Private_Functions ---------------------------------------------------------*/
/* 该文件内部定义的函数的声明 */
void UpdataMaxGainLevel(void);
void GetSysSetPara(void); //慢速获取其他系统设置 的实时变量
void BackSoftUpdate(void);
void UpdataGain(Uint8 GainLevel,Uint8 UpdataWhichloop);
void SaveInitServoPara(void);
void SetInitTunPara(void);
void SaveNewServoPara(void);
void Return2DfltServoPara(void);
void Return2InitServoPara(void);

void ErrMsgHandle(Uint16 ServRunStat); //
void OScillateLeveMonit(void); 

/*引用的外部函数*/
void PosCoinTimandOverPulsCal(void); //定位时间和过冲量计算
void FrictionIdentfy(void);
void PathUpdata(Uint8 runMod);  //曲线设置计算


/***********************************************
void UpdataMaxGainLevel(void)
描述: 
最高刚性等级更新，根据辨识的惯量值更新
************************************************/
void UpdataMaxGainLevel(void)
{
	if(FunCodeUnion.code.GN_InertiaRatio < 500)
	{
		STR_TunGainTable.MinGainLevel = 12;
		STR_TunGainTable.MaxGainLevel = 25;      
	}
	else if(FunCodeUnion.code.GN_InertiaRatio < 1500)
	{
		STR_TunGainTable.MinGainLevel = 10;
		STR_TunGainTable.MaxGainLevel = 24;
	}
	else if(FunCodeUnion.code.GN_InertiaRatio < 2500)
	{
		STR_TunGainTable.MinGainLevel = 7;
		STR_TunGainTable.MaxGainLevel = 20;
	}
	else
	{
		STR_TunGainTable.MinGainLevel = 4;
		STR_TunGainTable.MaxGainLevel = 18;
	}
}


/***********************************************
void SaveSetInitServoPara(void)
描述: 
备份自调整需要修改的参数
************************************************/
void SaveInitServoPara(void)
{
    //保存自调整会调整的参数
	STR_TunGainTable.AutoTuningModeSel      = FunCodeUnion.code.AutoTuningModeSel;
	STR_TunGainTable.Rigidity_Level1st      = FunCodeUnion.code.Rigidity_Level1st;
	STR_TunGainTable.InertiaRatio           = FunCodeUnion.code.GN_InertiaRatio;
	STR_TunGainTable.InertiaIdyCountModeSel = FunCodeUnion.code.InertiaIdyCountModeSel;
	
	STR_TunGainTable.Pos_Kp              	= FunCodeUnion.code.GN_Pos_Kp;
	STR_TunGainTable.Spd_Kp              	= FunCodeUnion.code.GN_Spd_Kp;
	STR_TunGainTable.Spd_Ti                	= FunCodeUnion.code.GN_Spd_Ti;
	STR_TunGainTable.ToqFiltTime            = FunCodeUnion.code.TL_ToqFiltTime;
	
	STR_TunGainTable.AdaptFiltMd            = FunCodeUnion.code.AT_AdaptiveFilterMode;
	STR_TunGainTable.NotchFiltFreqC         = FunCodeUnion.code.AT_NotchFiltFreqC;
	STR_TunGainTable.NotchFiltBandWidthC  	= FunCodeUnion.code.AT_NotchFiltBandWidthC;
	STR_TunGainTable.NotchFiltAttenuatLvlC  = FunCodeUnion.code.AT_NotchFiltAttenuatLvlC;
	STR_TunGainTable.NotchFiltFreqD         = FunCodeUnion.code.AT_NotchFiltFreqD;
	STR_TunGainTable.NotchFiltBandWidthD    = FunCodeUnion.code.AT_NotchFiltBandWidthD;
	STR_TunGainTable.NotchFiltAttenuatLvlD  = FunCodeUnion.code.AT_NotchFiltAttenuatLvlD;

	STR_TunGainTable.ModuKp                 = FunCodeUnion.code.AT_ModuKp;
	STR_TunGainTable.ModuKff                = FunCodeUnion.code.AT_ModuKffValu;
	STR_TunGainTable.ModEn					= FunCodeUnion.code.AT_ModuEn;

	if(FunCodeUnion.code.AutoTuningModeSel > 3)
	{
		STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.TunMod = 1;
	}
}


/***********************************************
void SetInitTunPara(void)
描述: 
设置自调整用到的初始值
************************************************/
void SetInitTunPara(void)
{
    ENUM_TuneStatus = READY_TO_TUNE;
	ENUM_IdentfyStage = STEP_READY_TO_IDENTFY;
	AuxFunCodeUnion.code.TUN_IdentfyEn = 0;
	AuxFunCodeUnion.code.TUN_ServoOnOrOff= 0;
	AuxFunCodeUnion.code.TUN_JogEn =0;
	
	STR_TunGainTable.UpdataGainloop = 3;	
	STR_TunGainTable.GainLevel =12;
	//惯量
	 FunCodeUnion.code.TUN_InertiaRatio =DLFTINERTIA;    //显示的惯量初始值
	FunCodeUnion.code.GN_InertiaRatio = 1.1*DLFTINERTIA;      //实际惯量初始值，特意有些差别
	AuxFunCodeUnion.code.RatioNow = 1.1*DLFTINERTIA;
	STR_MTR_Gvar.InertiaIdy.RatioNow = 1.1*DLFTINERTIA;
	FunCodeUnion.code.TempInertiaNum = 0;          //辨识次数
	//第三页显示的辨识值
	AuxFunCodeUnion.code.TUN_CoinTim = 0;
	AuxFunCodeUnion.code.TUN_OverPuls = 0;
	FunCodeUnion.code.AT_ToqMinusComp = 0;
	FunCodeUnion.code.AT_ToqPlusComp = 0;
	//指令
	STR_TunPath.MovStag = 0;
	STR_TunPath.RemainP =  0;
	STR_TunMonitor.OscillateHappen = 0;
	STR_TunMonitor.OscillatReliev = 1;
}


/***********************************************
void SaveNewServoPara(void)
描述: 
保存调整好的参数
************************************************/
void SaveNewServoPara(void)
{
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AutoTuningModeSel));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.Rigidity_Level1st));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.GN_InertiaRatio));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.InertiaIdyCountModeSel));
	
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.GN_Pos_Kp));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.GN_Spd_Kp));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.GN_Spd_Ti));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.TL_ToqFiltTime));
	
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_AdaptiveFilterMode));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltFreqC));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltBandWidthC));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltAttenuatLvlC));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltFreqD));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltBandWidthD));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_NotchFiltAttenuatLvlD));

	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_ModuKp));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_ModuKffValu));
	SaveToEepromOne(GetCodeIndex(FunCodeUnion.code.AT_ModuEn));
}


/***********************************************
void  Return2DefaultPara(void)
描述: 
设置到默认的刚性，参数，开始自调整必会调用
************************************************/
void Return2DfltServoPara(void)
{
    //  自调整会调整的参数
	FunCodeUnion.code.AutoTuningModeSel = 1 ;           //默认值置1
	FunCodeUnion.code.Rigidity_Level1st = 12;
	//FunCodeUnion.code.GN_InertiaRatio =   DLFTINERTIA;                惯量不用再设置一遍
	
	FunCodeUnion.code.AT_AdaptiveFilterMode = 1;        //置1
	FunCodeUnion.code.AT_NotchFiltFreqC     = 4000;
	FunCodeUnion.code.AT_NotchFiltBandWidthC = 2;
	FunCodeUnion.code.AT_NotchFiltAttenuatLvlC = 0;
	FunCodeUnion.code.AT_NotchFiltFreqD = 4000;
	FunCodeUnion.code.AT_NotchFiltBandWidthD = 2;
	FunCodeUnion.code.AT_NotchFiltAttenuatLvlD = 0;

	FunCodeUnion.code.AT_ModuKp = 400;
	FunCodeUnion.code.AT_ModuKffValu =  900;
	//FunCodeUnion.code.AT_ModuEn  = 0;    //默认关闭模型跟踪

	STR_TunGainTable.GainLevel =12;
}


/***********************************************
void Return2InitPara(void)
描述: 
恢复之前暂存的刚性等级、前馈、调整模式、增益切换等
近惯量辨识的情况下需要恢复，其他情况不需要
************************************************/
void Return2InitServoPara(void)
{
    //恢复     自调整会调整的参数
	FunCodeUnion.code.AutoTuningModeSel = STR_TunGainTable.AutoTuningModeSel ;
	FunCodeUnion.code.Rigidity_Level1st =STR_TunGainTable.Rigidity_Level1st  ;
	FunCodeUnion.code.GN_InertiaRatio =STR_TunGainTable.InertiaRatio ;
	FunCodeUnion.code.InertiaIdyCountModeSel=STR_TunGainTable.InertiaIdyCountModeSel ;

	FunCodeUnion.code.GN_Pos_Kp = STR_TunGainTable.Pos_Kp;
	FunCodeUnion.code.GN_Spd_Kp = STR_TunGainTable.Spd_Kp;
	FunCodeUnion.code.GN_Spd_Ti = STR_TunGainTable.Spd_Ti ;
	FunCodeUnion.code.TL_ToqFiltTime = STR_TunGainTable.ToqFiltTime;
	
	FunCodeUnion.code.AT_AdaptiveFilterMode = STR_TunGainTable.AdaptFiltMd;
	FunCodeUnion.code.AT_NotchFiltFreqC = STR_TunGainTable.NotchFiltFreqC ;
	FunCodeUnion.code.AT_NotchFiltBandWidthC = STR_TunGainTable.NotchFiltBandWidthC;
	FunCodeUnion.code.AT_NotchFiltAttenuatLvlC = STR_TunGainTable.NotchFiltAttenuatLvlC;
	FunCodeUnion.code.AT_NotchFiltFreqD = STR_TunGainTable.NotchFiltFreqD ;
	FunCodeUnion.code.AT_NotchFiltBandWidthD = STR_TunGainTable.NotchFiltBandWidthD;
	FunCodeUnion.code.AT_NotchFiltAttenuatLvlD = STR_TunGainTable.NotchFiltAttenuatLvlD ;

	FunCodeUnion.code.AT_ModuKp = STR_TunGainTable.ModuKp ;
	FunCodeUnion.code.AT_ModuKffValu =STR_TunGainTable.ModuKff;
	FunCodeUnion.code.AT_ModuEn = STR_TunGainTable.ModEn;
}


/***********************************************
void EasyTunGainParaUpdata(void)
描述: 
更新增益
************************************************/
void EasyTunGainParaUpdata(void)
{
	if(STEP_INERTIA_IDENTFY < ENUM_IdentfyStage)
	{
		UpdataGain(STR_TunGainTable.GainLevel, STR_TunGainTable.UpdataGainloop);
	}
}

/***********************************************
void UpdataGain(void)
描述: 
更新增益
************************************************/
void UpdataGain(Uint8 GainLevel,Uint8 UpdataWhichloop)
{
	if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.OnlyInertia == 1)
		return;
	
	//获取刚性表数据
	if((UpdataWhichloop & 0x01)== 1)
	{
		//STR_TunGainTable.UpdataPosloop =0;
		FunCodeUnion.code.GN_Pos_Kp = GainTable[GainLevel].PosKp;
	}

	if((UpdataWhichloop & 0x02) == 2)
	{
		//STR_TunGainTable.UpdataSpdloop = 0;
		FunCodeUnion.code.GN_Spd_Kp = GainTable[GainLevel].SpdKp;
		FunCodeUnion.code.GN_Spd_Ti = GainTable[GainLevel].SpdTi;
		FunCodeUnion.code.TL_ToqFiltTime = GainTable[GainLevel].ToqFiltTime;
	}
}


/***********************************************
void EasyTunGetCmdPara(void)
描述: 
和其他模块的接口变量，读取需要的全局变量转换为内部变量
快速调度
************************************************/
void EasyTunGetInterfacePara(void)
{
	if((STR_TunMonitor.PosRef == 0)
		&&(ABS(STR_FUNC_Gvar.PosCtrl.PosRef) < STR_TunMonitor.OnePulstoEncodPuls))
	{
		STR_TunMonitor.PosRef = 0;
	}
	else 
	{
		STR_TunMonitor.PosRef = STR_FUNC_Gvar.PosCtrl.PosRef;
	}
	
//	STR_TunMonitor.PosRef = STR_FUNC_Gvar.PosCtrl.PosInput;
	STR_TunMonitor.SpdCmdRef = STR_FUNC_Gvar.SpdCtrl.SpdRef;
	STR_TunMonitor.ToqRef= UNI_FUNC_MTRToFUNC_FastList_16kHz.List.IqRef;
	STR_TunMonitor.SpdFd = UNI_FUNC_MTRToFUNC_FastList_16kHz.List.SpdFdb;
	STR_TunMonitor.PosAmperr = STR_FUNC_Gvar.PosCtrl.PosAmplifErr1;
 									 
	if(((STR_TunMonitor.STR_Tun_Control.IdentRunEn == VALID)
		&&(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.CmdSelect == 1))
		||(AuxFunCodeUnion.code.TUN_JogEn == 1))
	{
		STR_FUNC_Gvar.MonitorFlag.bit.EasyTunEn = 1; //指令来自后台规划
	}
	else
	{
		STR_FUNC_Gvar.MonitorFlag.bit.EasyTunEn = 0;
	}

	//定位时间和过冲量计算
	PosCoinTimandOverPulsCal();

}


/***********************************************
void GetSysSetPara(void)
描述: 
和其他模块的接口变量，读取需要的全局变量转换为内部变量
慢速调度
************************************************/
void GetSysSetPara(void)
{
	Uint32 temp;
	STR_TunMonitor.PosFreq = STR_FUNC_Gvar.System.PosFreq;
	STR_TunMonitor.SpdFreq = STR_FUNC_Gvar.System.SpdFreq;

	temp = ((Uint32)FunCodeUnion.code.MT_EncoderPensH << 16) 
                              + FunCodeUnion.code.MT_EncoderPensL;

	if(temp == STR_FUNC_Gvar.PosCtrl.Numerator)
	{
		STR_TunMonitor.PulsOneTurn = STR_FUNC_Gvar.PosCtrl.Denominator;
	}
	else //约分过后的
	{
		STR_TunMonitor.PulsOneTurn = temp * STR_FUNC_Gvar.PosCtrl.Denominator/STR_FUNC_Gvar.PosCtrl.Numerator ;
	}

	//此处如果齿轮比小于1，则出BUG。23bit后暂时不考虑

	STR_TunMonitor.OnePulstoEncodPuls = (STR_FUNC_Gvar.PosCtrl.Numerator<<1) / STR_FUNC_Gvar.PosCtrl.Denominator;

	STR_TunMonitor.MT_EncoderNum = temp ;
}


/***********************************************
void EasyTunMonit(void)
描述: 
负责监控辨识状态、和上位机交互、启动过程
更新全局的辨识启动标志IdentfyEnValidFlg。
其他模块根据此标志调用。global monitor结构体中需要增加此标志
速度和位置指令调用以及模式切换中都需要此标志
************************************************/
void EasyTunMonit(Uint16 ServRunStat)
{
	//自整定过程中不能进行其他操作，如回零、全闭环操作等等
	//在程序中需要加入互斥，一旦互斥同时发生则报错
	OScillateLeveMonit();

	//定位时间和过冲量计算
	//PosCoinTimandOverPulsCal();
	
	//获取功能模块的其他参数
	GetSysSetPara();
	
	//状态更新
	BackSoftUpdate();

	//故障处理
	ErrMsgHandle(ServRunStat);	

	//故障或停机时调整相关参数复位
	//ParaResetInStopOrErr();
}

/***********************************************
void EasyTunModCtrl(Uint16 ServRunStat,Uint8* pRunmod)
描述: 
ServRunStat 当前运行状态
pRunmod 运行模式指针
启动时为OFF，则根据设置进入速度或者位置模式
启动时如果是ON状态，则速度模式只能惯量辨识
需要根据功能区分后台模式还是功能吗模式
************************************************/
void EasyTunModeCtrl(Uint16 ServRunStat,Uint8* pRunmod)
{
	//Uint16 temp;
	static Uint8 runModBackup = 0xff;
	//static Uint8 JogEnFlag =0;
	//static Uint8 IdentEnFlag=0;

	//判断自调整是后台还是功能吗方式
	//temp = FunCodeUnion.code.AT_RespnLevel & 0x00ff;

	ENUM_RunStatus = (ENUM_RUNSTAT)ServRunStat;

	//第二页的jogen
	if(AuxFunCodeUnion.code.TUN_JogEn == 1)
	{
		if(runModBackup==0xff)
		{
		     runModBackup = *pRunmod ;
		     *pRunmod = SPDMOD;
		}
		 AuxFunCodeUnion.code.TUN_ServoOnOrOff = 1;
		 PathUpdata(*pRunmod);		 
		 return;
	}
	else if((AuxFunCodeUnion.code.TUN_JogEn == 0)
		&&(runModBackup!=0xff))
	{
		*pRunmod = runModBackup;
		AuxFunCodeUnion.code.TUN_ServoOnOrOff = 0;
		STR_TunPath.MovStag =0;
		runModBackup=0xff;
		return;
	}

	//第3页的identEn	
	/*
		if(STR_TunMonitor.IdentEn == 0)
		{		
			if(AuxFunCodeUnion.code.TUN_IdentfyEn == 0x01) //后台自调整
			{
				STR_TunMonitor.IdentEn = 1;
				PathUpdata(*pRunmod);
			}

		}
		if((STR_TunMonitor.IdentEn == 1)&& (AuxFunCodeUnion.code.TUN_IdentfyEn != 1))
		{   
			STR_TunMonitor.IdentEn = 0;
			STR_TunMonitor.STR_Tun_Control.IdentRunEn = INVALID;
		}
	*/
			
	if(AuxFunCodeUnion.code.TUN_IdentfyEn == 0x01) //后台自调整
	{
	  if(STR_TunMonitor.IdentEn==0)
		   PathUpdata(*pRunmod);
	  STR_TunMonitor.IdentEn = 1;
	}
	else 
	{
		if(runModBackup!=0xff)
		{
			*pRunmod = runModBackup;
			runModBackup =0xff;
		}	  
		STR_TunMonitor.IdentEn = 0;
		STR_TunMonitor.STR_Tun_Control.IdentRunEn = INVALID;
	}

	if((STR_TunMonitor.STR_Tun_Control.IdentRunEn == INVALID)
		&&(STR_TunMonitor.IdentEn != 0))
	{
		if(ServRunStat == RUN)
		{
			STR_TunMonitor.STR_Tun_Control.IdentRunEn = VALID;
			ENUM_RunStatus = SERVO_PARA_OK;
		}	
		else if(ServRunStat == RDY)
		{
			if(runModBackup==0xff)
		    {
			    runModBackup = *pRunmod;
				
				//惯量辨识时其实可以用速度模式，完成后再切换到位置模式
			    if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.OnlyInertia == VALID)
				{
				    *pRunmod = SPDMOD;	
				}
				else
				{
				    *pRunmod = POSMOD;
					if(runModBackup==TOQMOD)
						STR_TunMonitor.STR_Tun_Stat.IdentfyErr = WRONGTUNMODE;	
				}
			}
			
			STR_TunMonitor.STR_Tun_Control.IdentRunEn = VALID;				
		}
	}
	else if((ServRunStat == RUN)&&(ENUM_RunStatus < SERVO_PARA_OK ))
	{
		ENUM_RunStatus = SERVO_PARA_OK;	
	}

	//故障、后台中止、功能吗中止时退出自调整
	if((STR_TunMonitor.STR_Tun_Stat.IdentfyErr != NULL_ERR)||(AuxFunCodeUnion.code.TUN_PageStatus==ActiveClose))
	{	
		if(runModBackup != 0xff)
		{
		     *pRunmod = runModBackup;
	         	
		     runModBackup=0xff;
		}
	}
	STR_TunMonitor.STR_Tun_Control.RunMod = *pRunmod ;
	
}


/***********************************************
void BackSoftUpdate(void)
描述: 
和后台软件的互动尽量都放在这里
根据上位机读取信息，更新相关状态字，
并监控是否掉线，超过3S没有读取则认为掉线
监控是否关闭了界面
************************************************/
void BackSoftUpdate(void)
{
	static Uint16 tim=0;
	static Uint8 svflag=0;   //是否保存了初始参数
	
	STR_TunMonitor.STR_Tun_Stat.UNI_STR_IdentfyStatWrd.bit.RunStat = (Uint8)ENUM_RunStatus;
	STR_TunMonitor.STR_Tun_Stat.UNI_STR_IdentfyStatWrd.bit.TunStat = (Uint8)ENUM_TuneStatus;
	AuxFunCodeUnion.code.TUN_IdentfyStat = STR_TunMonitor.STR_Tun_Stat.UNI_STR_IdentfyStatWrd.all;
	AuxFunCodeUnion.code.TUN_IdentfyErr = STR_TunMonitor.STR_Tun_Stat.IdentfyErr;

	//在自调整中如果3S没有读取则通信中断处理
	if(STR_TunMonitor.IdentEn == 1)
	{
		tim++;
	}
	
	if((tim > 3000)&&(AuxFunCodeUnion.code.TUN_PageStatus==PageThree))//调整结束后后台不再写2f47
	{
		//报错通信中断
		STR_TunMonitor.STR_Tun_Stat.IdentfyErr = COMMBREAKOFF;
	}

	if((AuxFunCodeUnion.code.TUN_PageStatus ==PageTwo)&&(LastPage==PageOne))
	{
		svflag=1;   //进入了第二页的标志位
		SaveInitServoPara();
		SetInitTunPara();
		//如果选择了默认增益开始，第二页就换成默认值
		if(STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.bit.DefaultGain == 1)
		{
			Return2DfltServoPara();
		}	
		STR_TunMonitor.STR_Tun_Control.UNI_STR_IdentfyCtrlWrd.all = AuxFunCodeUnion.code.TUN_IdentfyCtrlWd;
	}

	// 中途关闭自调整界面、报错时都清除变量恢复初始值     点调整终止不处理
	if(((AuxFunCodeUnion.code.TUN_PageStatus==ActiveClose)||(ENUM_RunStatus == SERVO_ERR) )&&(svflag==1))
		//&&((LastPage ==PageTwo)||(LastPage==PageThree)||((i==PageOne)&&(LastPage==PageOne)))
	{
        SetInitTunPara();
	    Return2InitServoPara();		
	}
	
	//如果停留在第一 第二页，状态都清零	
	if((AuxFunCodeUnion.code.TUN_PageStatus ==PageOne)||(AuxFunCodeUnion.code.TUN_PageStatus ==PageTwo))
	{
		ENUM_TuneStatus = ACTIVE_INERTIA_IDENTFY;
		ENUM_IdentfyStage = STEP_READY_TO_IDENTFY;
	}

	if(AuxFunCodeUnion.code.TUN_PageStatus==ActiveClose)
		svflag=0;

	if(AuxFunCodeUnion.code.TUN_ReadIdentfyStat == 1)
	{
		AuxFunCodeUnion.code.TUN_ReadIdentfyStat =0;
		tim = 0;
	}

	LastPage = AuxFunCodeUnion.code.TUN_PageStatus;

}


/***********************************************
void ErrMsgHandle(Uint16 ServRunStat)
描述: 
根据当前运行状态处理错误信息，如果其他模块故障停机，
如果本模块故障发生本模块故障码?
************************************************/
void ErrMsgHandle(Uint16 ServRunStat)
{
	if((ServRunStat == ERR)&&(STR_TunMonitor.STR_Tun_Stat.IdentfyErr == 0))
	{
		//STR_TunMonitor.STR_Tun_Stat.IdentfyErr = 0X0900;
	}

	if(STR_TunMonitor.STR_Tun_Stat.IdentfyErr != 0)
	{
		AuxFunCodeUnion.code.TUN_ServoOnOrOff = 0;
		AuxFunCodeUnion.code.TUN_IdentfyEn = 0;
		//显示故障信息
		PostErrMsg(STR_TunMonitor.STR_Tun_Stat.IdentfyErr);
	}
}


/*******************************************************************************
  函数名: OScillateLeveMonit()
  输  入:          
  输  出:   
  子函数:                                       
  描  述: 计算振动等级
********************************************************************************/
void OScillateLeveMonit(void)
{
    static Uint16 SumCnt = 0;			//计算振动累加和的次数
	static Uint16 OldFftCalCnt = 0;	   //保存上周期FFt计算成功的次数
	
	//只要计算结果发生改变，就将结果进行累加
	if(OldFftCalCnt != STR_FUNC_Gvar.Fft.FftCalCnt)
	{
		SumCnt++;	    
		STR_TunMonitor.OscillateLeveSum += STR_FUNC_Gvar.Fft.OscillateValue;

		if(((STR_TunMonitor.PosRef == 0)	&&(STR_TunMonitor.STR_Tun_Control.RunMod == POSMOD))  //脉冲停止时计算平均震动等级
		||((STR_TunMonitor.SpdCmdRef== 0)	&&(STR_TunMonitor.STR_Tun_Control.RunMod == SPDMOD)))
		{
		    STR_TunMonitor.OscillateLevelAv = STR_TunMonitor.OscillateLeveSum / SumCnt;
			STR_TunMonitor.OscillateLevelAv = ((Uint32)STR_TunMonitor.OscillateLevelAv*1000)>>12;	 //转化为百分比		    
			STR_TunMonitor.OscillateLeveSum = 0;
			SumCnt= 0;
		}
	}

	if(STR_FUNC_Gvar.Fft.OscillateLevel < STR_FUNC_Gvar.Fft.OscillateValue)
	{
		STR_TunMonitor.OscillateHappen = 1;
		STR_TunMonitor.OscillatReliev = 0;
	}
	else
	{
		//STR_TunMonitor.OscillateHappen = 0;
		STR_TunMonitor.OscillatReliev = 1;
	}
	
	OldFftCalCnt = STR_FUNC_Gvar.Fft.FftCalCnt;
}

